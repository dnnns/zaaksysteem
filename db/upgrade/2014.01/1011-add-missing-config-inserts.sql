BEGIN;

INSERT INTO config (parameter, value, advanced) SELECT 'document_intake_user', 'intake', 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'document_intake_user'
    )
;
INSERT INTO config (parameter, value, advanced) SELECT 'users_can_change_password', 'off', 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'users_can_change_password'
    )
;

COMMIT;
