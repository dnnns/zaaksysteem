-- the delay type is influenced by relatie_type. since this can yield a conflicting situation
-- i'm removing 'delay_type'. delay_type was set by commit_session, when saving a new zaaktype
-- version.

ALTER TABLE zaaktype_relatie DROP delay_type;

