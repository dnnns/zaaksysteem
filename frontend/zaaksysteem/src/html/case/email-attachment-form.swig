<div data-zs-modal data-ng-init="title='%%E-mail versturen%%'">
	<div data-zs-spinner="isLoading()" class="zs-spinner-medium"></div>

	<form
		name="emailForm"
		class="grid-table modal-medium doc-email-form"
		data-ng-controller="nl.mintlab.docs.EmailAttachmentsController"
		data-ng-init="init()"
		novalidate
	>
		<div class="row row-config" data-ng-show="showTemplateSelection()">
			<div class="column large-4 td-label">%%E-mailsjabloon%%</div>
			<div class="column large-8 td-field">
				<select
					data-ng-model="template"
					data-ng-options="template as template.bibliotheek_notificaties_id.label for template in templates"
					data-ng-change="handleTemplateChange()"
				>
					<option value="">Geen</option>
				</select>
			</div>
		</div>

		<div
			class="row row-config"
			data-ng-class="{'row-type-ontvanger':(typeRecipient !== 'betrokkene')}"
		>
			<div class="column large-4 td-label">%%Selecteer type ontvanger%%</div>
			<div class="column large-8 td-field">
				<select
					data-ng-model="typeRecipient"
					data-ng-options="recipient.value as recipient.label for recipient in getRecipients()"
					data-ng-change="handleTypeRecipientChange()"
				>
				</select>
			</div>
			<div class="row-options" data-ng-show="(typeRecipient !== 'betrokkene') && showCcOptions()">
				<div class="row-options-wrapper">
					<button type="button" data-ng-click="showCc()" data-ng-show="!isCcVisible()">
						Cc
					</button>
					<button type="button" data-ng-click="showBcc()" data-ng-show="!isBccVisible()">
						Bcc
					</button>
				</div>
			</div>
		</div>

		<div
			data-ng-show="typeRecipient === 'betrokkene'"
			class="row row-config row-type-ontvanger"
			data-ng-class="{'row-type-ontvanger':(typeRecipient === 'betrokkene')}"
		>
			<div class="column large-4 td-label">%%Selecteer type betrokkene%%</div>
			<div class="column large-8 td-field">
				<select
					data-ng-model="roleRecipient"
					data-ng-options="role.value as role.label for role in roles"
				>
				</select>
			</div>
			<div class="row-options" data-ng-show="(typeRecipient === 'betrokkene') && showCcOptions()">
				<div class="row-options-wrapper">
					<button type="button" data-ng-click="showCc()" data-ng-show="!isCcVisible()">
						Cc
					</button>
					<button type="button" data-ng-click="showBcc()" data-ng-show="!isBccVisible()">
						Bcc
					</button>
				</div>
			</div>
		</div>

		<div class="row row-email-content">
			<div class="column large-8 td-field" data-ng-show="typeRecipient==='coworker'">
				<input type="text" data-ng-model="recipient" data-zs-placeholder="'%%Zoek uw collega%%'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'contact/medewerker'" data-zs-spot-enlighter-label="naam" data-zs-object-required="<[typeRecipient=='coworker']>"/>
			</div>
			<div class="column large-8 td-field" data-ng-show="typeRecipient!=='coworker'">
				<input type="text" name="recipientAddress" data-ng-model="recipientAddress" data-ng-required="typeRecipient==='other'" class="large" data-zs-placeholder="'%%E-mailadres%%'" data-ng-disabled="typeRecipient!=='other'" />
			</div>
		</div>

		<div class="row row-email-content" data-ng-show="isCcVisible()">
			<div class="column large-4 td-label">%%Cc%%</div>
			<div class="column large-8 td-field">
				<input type="text" data-ng-model="ccAddress" class="large" data-zs-placeholder="'%%Cc (voorbeeld@zaaksysteem.nl;voorbeeld@mintlab.nl)%%'" />
			</div>
		</div>

		<div class="row row-email-content" data-ng-show="isBccVisible()">
			<div class="column large-4 td-label">%%Bcc%%</div>
			<div class="column large-8 td-field">
				<input type="text" data-ng-model="bccAddress" class="large" data-zs-placeholder="'%%Bcc (voorbeeld@zaaksysteem.nl;voorbeeld@mintlab.nl)%%'" />
			</div>
		</div>
		
		<div class="row row-email-content">
			<div class="column large-4 td-label">%%Onderwerp%%</div>
			<div class="column large-8 td-field">
				<input type="text" data-ng-model="emailSubject" class="large" data-zs-placeholder="'%%Onderwerp%%'" />
			</div>
		</div>
		
		<div class="row row-email-content">
			<div class="column large-4 td-label">%%Bericht%%</div>
			<div class="column large-8 td-field">
				<textarea data-ng-model="emailContent" data-ng-required="!template" data-zs-placeholder="'%%Inhoud%%'" ></textarea>
			</div>
		</div>
		
		<div class="row row-email-content row-email-attachment" data-ng-show="attachments.length">
			<div class="column large-4 td-label">%%Bijlagen%%</div>
			<div class="column large-8 td-field">
				<i class="mdi mdi-attachment"></i>  
				<label data-ng-repeat="attachment in attachments">
					<input type="checkbox" data-ng-checked="isAttached(attachment)" data-ng-click="detach(attachment)"/>
					<[getName(attachment)]>
				</label>
				
				<input type="text" data-ng-show="false" data-ng-model="newAttachment" data-zs-placeholder="'%%Voeg een zaakdocument toe%%'" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'case_type_document'" data-zs-spot-enlighter-label="naam"/>
				
			</div>
		</div>
		
		<div class="form-actions clearfix">
			<button data-ng-disabled="isLoading()||emailForm.$invalid" data-ng-click="!emailForm.$invalid&&sendEmail()" class="button button-primary btn btn-flat">
				%%Versturen%%
			</button>
			
			<button data-ng-show="context=='actions'" data-ng-disabled="isLoading()||isSaveDisabled()" data-ng-click="saveTemplate()" class="button button-secondary btn btn-flat">
				%%Opslaan%%
			</button>
		</div>
	</form>
</div>
