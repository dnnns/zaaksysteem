/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.object')
		.factory('objectMutationService', [ '$q', '$http', 'translationService', function ( $q, $http, translationService ) {
			
			var objectMutationService = {},
				mutationOptions = [
					{
						value: 'create',
						label: translationService.get('Nieuw object aanmaken')
					},
					{
						value: 'relate',
						label: translationService.get('Bestaand object relateren')
					},
					{
						value: 'update',
						label: translationService.get('Bestaand object wijzigen')
					},
					{
						value: 'delete',
						label: translationService.get('Bestaand object verwijderen')
					}
				];
			
			objectMutationService.getMutationOptions = function ( ) {
				return mutationOptions;
			};
			
			objectMutationService.addMutation = function ( caseId, objectTypeName, mutation ) {
				
				var deferred = $q.defer();
				
				$http({
					method: 'POST',
					url: '/api/case/' + caseId + '/mutations/add',
					data: {
						object_type: objectTypeName,
						mutation: angular.copy(mutation)
					}
				})
					.success(function ( response ) {
						deferred.resolve(response.result[0]);
					})
					.error(function ( response ) {
						deferred.reject(response.result[0]);
					});
					
				return deferred.promise;
			};
			
			objectMutationService.removeMutation = function ( caseId, mutation ) {
				var deferred = $q.defer();
				
				$http({
					method: 'POST',
					url: '/api/case/' + caseId + '/mutations/delete',
					data: {
						id: mutation.id
					}
				})
					.success(function ( response ) {
						deferred.resolve(response.result[0]);
					})
					.error(function ( response ) {
						deferred.reject(response.result[0]);
					});
					
				return deferred.promise;
			};
			
			objectMutationService.updateMutation = function ( caseId, mutation ) {
				var deferred = $q.defer();
				
				$http({
					method: 'POST',
					url: '/api/case/' + caseId + '/mutations/update',
					data: {
						mutation: angular.copy(mutation)
					}
				})
					.success(function ( response ) {
						deferred.resolve(response.result[0]);
					})
					.error(function ( response ) {
						deferred.reject(response.result[0]);
					});
					
				return deferred.promise;
				
			};
			
			return objectMutationService;
			
		}]);
	
})();
