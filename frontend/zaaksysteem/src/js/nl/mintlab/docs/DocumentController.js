
/*global angular,fetch,_*/
(function ( ) {

	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentController', [
			'$scope', 'smartHttp', '$filter', 'fileUploader', '$timeout', '$q', 'caseDocumentService', 'snackbarService',
			function ( $scope, smartHttp, $filter, fileUploader, $timeout, $q, caseDocumentService, snackbarService ) {

			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
				Folder = window.zsFetch('nl.mintlab.docs.Folder'),
				File = window.zsFetch('nl.mintlab.docs.File');

			$scope.trash = new Folder( { name: 'Prullenbak' });
			$scope.list = new Folder( { name: 'Map' });
			$scope.intake = new Folder( { name: 'Intake' });
			$scope.caseDocs = [];
			$scope.docCats = [];
			$scope.initialized = false;
			$scope.loading = false;
			$scope.loadState = {};

			$scope.filter = {
				query: ''
			};

			if ($scope.readOnly == undefined) {
				$scope.readOnly = false;
			}

			$scope.view = 'list';

			$scope.reloadData = function ( ) {
				var promises = [],
					treeData,
					openFolders = [];

				if($scope.list) {
					getFlatFolderList()
						.forEach(function ( folder ) {
							if(!folder.getCollapsedInScope()) {
								openFolders.push(String(folder.id));
							}
						});
				}

				$scope.loading = true;

				if(!$scope.pip) {
					promises.push(
						smartHttp.connect( {
							url: '/api/case/' + $scope.caseId + '/directory/tree',
							method: 'GET'
						})
							.success(function ( data ) {
								treeData = data.result[0];
							})
					);
				}

				if(!$scope.pip) {
					promises.push(
						caseDocumentService.getCaseDocumentsByCaseId($scope.caseId)
							.then(processCaseDocData)
					);
				}

				if(!$scope.pip) {
					promises.push(
						smartHttp.connect( {
							url: 'file/document_categories',
							method: 'GET'
						})
							.success(processDocCatData)
					);
				}

				$q.all(promises).then(function ( ) {

					var files = [],
						caseDocsById = {},
						folders;

					$scope.trash.empty();
					$scope.list.empty();
					$scope.intake.empty();

					processTreeData(treeData);

					folders = _.mapKeys(getFlatFolderList(), function ( folder ) {
						return String(folder.id);
					});

					_.each(openFolders, function ( id ) {

						var folder = folders[id];

						if(folder) {
							folder.setCollapsed(false);
						}

					});

					_.each($scope.caseDocs, function ( caseDoc ) {
						caseDocsById[caseDoc.id] = caseDoc;
					});

					files = getFlatFileList();

					_.each(files, function ( file ) {
						file.case_documents = _.map(file.case_documents, function ( caseDocId ) {
							return caseDocsById[caseDocId];
						});
					});

					$scope.loading = false;

					$scope.$broadcast('reload');
				});
			};

			$scope.setView = function ( view ) {
				$scope.view = view;
			};

			$scope.addFolder = function ( folder, folderName ) {
				var child = new Folder( { name: folderName });
				folder.add(child);
			};

			$scope.uploadFile = function ( files, params ) {
				var needReload = false,
					uploadPromises = [],
					url = $scope.pip ? 'pip/file/create' : 'file/create';
				if(!params) {
					params = [];
				}

				// Can't use lodash _.map on a fileList object for some reason
				uploadPromises = Array.prototype.slice.call(files).map(function ( value, index) {
					var fileParams = {},
						deferred;

					if(params[index]) {
						fileParams = params[index];
					}

					if(!fileParams.case_id) {
						fileParams.case_id = $scope.caseId;
					}

					if(
						_.filter(getFlatFileList(), function ( file ) {
							return file.name + file.extension === fileParams.filename;
						}).length > 0) 	{
						deferred = $q.defer();
						deferred.reject({
							id: 'upload/filename_exists',
							message: 'Cannot upload file. A file with the same filename already exists'
						});
						return deferred.promise;
					}

					return fileUploader.upload(value, smartHttp.getUrl(url), fileParams).promise.then(function ( upload ) {

						var file = new File(),
							response = upload.getData(),
							result = response.result,
							caseChanges = response.additional_changes,
							data = result ? result[0] : null;

						file.updating = true;

						file.case_documents = data.case_documents;
						file.updateWith(data);

						$timeout(function ( ) {
							file.updating = false;
						});

						// for certain filetypes, the server will unwrap and
						// but the file contents in a folder. for these
						// cases, reload the document screen after
						// all files have been uploaded
						// in the case of reload, we don't need to bother rendering -
						// we'll do that next time.
						if (_.includes(caseChanges, 'case/documents')) {
							needReload = true;
						} else {
							if(!file.accepted) {
								$scope.intake.add(file);
							} else {
								$scope.list.add(file);
							}
							countIntake();
						}

					}, function ( /*upload*/ ) {

					});

				});

				return $q.all(uploadPromises).then(function () {
					if (needReload) {
						$scope.reloadData();
					}
				});
			};

			$scope.replaceFile = function ( file, replace, params ) {
				var replacement = replace[0];
				file.updating = true;
				if(replacement) {
					if(!params) {
						params = {
							'file_id': file.id
						};
					}
					return fileUploader.upload(replacement, smartHttp.getUrl('file/update_file'), params).promise.then(function ( upload ) {
						var result = upload.getData().result,
							data = result ? result[0] : null;
						file.updating = false;
						file.updateWith(data);
					}, function ( /*upload*/ ) {
						file.updating = false;
					});
				}
			};

			function processFileData ( files ) {
				var parsedFiles = [];

				angular.forEach(files, function ( file ) {
					parsedFiles.push(parseFileData(file));
				});

				countIntake();

				$scope.initialized = true;
			}

            function processTreeData ( treeData ) {
                if (treeData._root !== true) {
                    return;
                }

                processFolderData(treeData.children);
                processFileData(treeData.files);
            }

			function processFolderData ( folders ) {
				angular.forEach(folders, function ( folder ) {
					parseFolderData(folder);
				});
			}

			function processCaseDocData ( data ) {
				$scope.caseDocs = data || [];
			}

			function processDocCatData ( data ) {
				var docCats = data.result || [],
					result = [];

				for(var i = 0, l = docCats.length; i < l; ++i) {
					result.push({
						index: i,
						label: docCats[i],
						value: docCats[i]
					});
				}

				$scope.docCats = result;
			}

			function parseFileData ( fileData, parent ) {
				var file = new File();

				file.updateWith(fileData);
				file.case_documents = fileData.case_documents;

				if(file.accepted && !file.date_deleted) {
                    if(!parent && fileData.directory_id) {

                        parent = $scope.list.getEntityByPath([ 'folder_' + fileData.directory_id.id ]);

                    }
					if(!parent) {
						parent = $scope.list;
					}
				} else if(!file.accepted) {
					parent = $scope.intake;
				} else {
					parent = $scope.trash;
				}

				parent.add(file);
			}

			function parseFolderData ( folderData, parent ) {
				var folder = $scope.list.getEntityByPath( [ 'folder_' + folderData.id ]);

				if(!folder){
					folder = new Folder({
                        id: folderData.id,
                        uuid: folderData.id
                    });

                    if (parent) {
                        parent.add(folder);
                    } else {
                        $scope.list.add(folder);
                    }

                    /* Recursively unroll directory tree, if one exists */
                    _.each(folderData.children, function ( child ) {
                        parseFolderData(child, folder);
                    });

                    _.each(folderData.files, function ( file ) {
                        parseFileData(file, folder);
                    });
				}

				folder.setCollapsed(true);

				folderData.uuid = folderData.id;

				folder.updateWith(folderData);

				return folder;
			}

			function countIntake ( ) {
				$scope.setNotificationCount('documents', $scope.intake.getFiles().length);
			}

			function getFlatFileList ( ) {
				var files = [];

				function getChildrenOf ( parent ) {
					var children = parent.getFolders(),
						i,
						l;

					files = files.concat(parent.getFiles());

					for(i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}

				getChildrenOf($scope.intake);
				getChildrenOf($scope.list);

				return files;
			}

			function getFlatFolderList ( ) {

				var folders = [];

				function getChildrenOf ( parent ) {
					var children = parent.getFolders(),
						i,
						l;

					folders = folders.concat(children);

					for(i = 0, l = children.length; i < l; ++i) {
						getChildrenOf(children[i]);
					}
				}

				getChildrenOf($scope.list);

				return folders;
			}

			$scope.getCaseDocLabel = function ( caseDoc ) {
				var label = '';

				if(caseDoc) {
					label = caseDoc.label || (caseDoc.bibliotheek_kenmerken_id ? caseDoc.bibliotheek_kenmerken_id.naam : '');
				}
				return label;
			};

			$scope.isSameCaseDoc = function ( caseDocA, caseDocB ) {
				if(caseDocA === caseDocB) {
					return true;
				}

				if(!caseDocA || !caseDocB) {
					return false;
				}

				return caseDocA.id === caseDocB.id;
			};

			$scope.isNotReferentialCaseDoc = function ( caseDoc ) {
				return !caseDoc || !caseDoc.referential;
			};

			$scope.init = function ( ) {
				$scope.reloadData();
			};

			$scope.isLoading = function ( ) {
				return $scope.loading;
			};

			$scope.$on('drop', function ( event, data, mimeType ) {
				if(mimeType === 'Files') {
					$scope.uploadFile(data);
				}
			});

			$scope.$on('file.accept', function ( event, file ) {
				safeApply($scope, function ( ) {
					countIntake();
				});
			});

			$scope.$on('file.reject', function ( event, file ) {
				safeApply($scope, function ( ) {
					countIntake();
				});
			});

		}]);

})();
