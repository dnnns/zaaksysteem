/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.objecttype')
		.directive('zsObjectTypeAttributeFormField', [ 'systemMessageService', 'translationService', function ( systemMessageService, translationService ) {
			
			return {
				require: [ 'zsObjectTypeAttributeFormField', 'ngModel' ],
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this,
						ngModel;
					
					ctrl.setNgModel = function ( model ) {
						ngModel = model;
					};
					
					ctrl.transform = function ( $object ) {
						
						var obj = {
								"attribute_id": $object.id,
								"name": $object.object.column_name,
								"attribute_type": $object.object.value_type,
								"attribute_label": $object.label,
								"data": {
								},
								"required": false,
								"index": false
							},
							attrType = obj.attribute_type;
						
						if(	_.indexOf(['googlemaps'], attrType) !== -1 ||
							attrType.match(/^valuta.+$/) || 
							$object.is_multiple && (attrType.indexOf('text') === 0 || attrType === 'numeric')
						) {
							systemMessageService.emitError('Dit kenmerk kan niet toegevoegd worden. Kenmerken van dit type worden momenteel nog niet ondersteund.');
							return null;
						}
						
						if($object.object.values) {
							obj.data.values = $object.object.values;
						}
						
						return obj;
					};
					
					ctrl.handleIndexChange = function ( object ) {
						var count;
						
						if(!object.index) {
							return;
						}
						
						count = _.reduce($scope.getList(), function ( sum, obj ) {
							// FIXME: /api/object/ currently converts boolean values
							// to "0" and "1"
							return obj.index && obj.index !== "0" ? sum + 1 : sum;
						}, 0);
						
						if(count > 5) {
							object.index = false;
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('U kunt maximaal vijf kenmerken selecteren die gebruikt worden voor het zoeken')
							});
						}
					};
					
					$scope.$on('zs.sort.update', function ( event, moved, before ) {
						var list = $scope.getList(),
							from = _.indexOf(list, moved),
							to = _.indexOf(list, before);
							
						if(to === -1) {
							to = list.length-1;
						} else {
							to--;
						}
							
						if(to !== from) {
							$scope.$apply(function ( ) {
								
								list = list.concat();
								
								_.pull(list, moved);
								
								to = _.indexOf(list, before);
								
								if(to === -1) {
									to = list.length;
								}
								
								list.splice(to, 0, moved);
								
								ngModel.$setViewValue(list);
								
							});
						}
						
					});
					
					return ctrl;
					
				}],
				controllerAs: 'objectTypeAttributeFormField',
				link: function ( scope, element, attrs, controllers ) {
					
					var zsObjectTypeAttributeFormField = controllers[0],
						ngModel = controllers[1];
						
					zsObjectTypeAttributeFormField.setNgModel(ngModel);
					
				}
			};
			
		}]);
	
})();
