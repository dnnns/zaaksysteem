/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.objecttype')
		.directive('zsObjectTypeValidation', [ '$http', 'safeApply', 'systemMessageService', function ( $http, safeApply, systemMessageService ) {
			
			return {
				require: [ 'zsObjectTypeValidation', '^zsObjectTypeEdit' ],
				restrict: 'E',
				replace: true,
				template: '<div>' + 
					'<div class="zs-spinner-medium" data-zs-spinner="objectTypeValidation.validityState===\'pending\'"></div>' + 
					'<div data-ng-show="objectTypeValidation.validityState===\'valid\'">' + 
						'Alle objecten voldoen aan deze specificatie.' + 
					'</div>' + 
					'<div data-ng-show="objectTypeValidation.validityState===\'invalid\'" class="object-validation-results">' +
						'<div class="object-validation-results-label"><i class="icon-font-awesome icon-warning-sign"></i> De volgende objecten voldoen niet aan de nieuwe specificatie en zullen niet meer beschikbaar zijn:</div>' + 
						'<div class="object-validation-results-list"><ul>' +
							'<li data-ng-repeat="object in objectTypeValidation.invalidObjects">' + 
								'<a href="/object/<[object.id]>" target="_blank"><[object.label]></a>' + 
							'</li>' +
						'</ul></div>' + 
					'</div>' + 
				'</div>',
				scope: {
					active: '&',
					valid: '&'
				},
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this,
						zsObjectTypeEdit,
						invalidate;
					
					ctrl.invalidObjects = [];
					ctrl.validityState = 'pending';
										
					invalidate = _.throttle(function ( ) {
						
						safeApply($scope, ctrl.validate);
						
					}, 100);
						
					ctrl.link = function ( controllers ) {
						zsObjectTypeEdit = controllers[0];	
						
						$scope.$watch(zsObjectTypeEdit.getObjectValues, function ( ) {
							invalidate();
						}, true);
					};
					
					ctrl.validate = function ( ) {
						var values = zsObjectTypeEdit.getObjectValues(),
							id = values.id;
							
						values = _(values)
							.omit('id')
							.pick(_.identity)
							.value();
							
						if(!$scope.active()) {
							ctrl.invalidObjects = [];
							ctrl.validityState = 'invalid';
							return;
						}

						if(id === undefined) {
							ctrl.validityState = 'valid';
							ctrl.invalidObjects = [];
							return;
						}
						
						ctrl.validityState = 'pending';
						
						$http({
							method: 'POST',
							data: {
								object: values,
							},
							url: '/api/object/' + id + '/action/validate'
						})
							.success(function ( response ) {
								ctrl.invalidObjects = response.result;
								ctrl.validityState = ctrl.invalidObjects.length ? 'invalid' : 'valid';
							})
							.error(function ( /*response*/ ) {
								systemMessageService.emitError('Er ging iets fout bij het valideren van het object. Probeer het later opnieuw.');
								ctrl.invalidObjects = [];
								ctrl.validityState = 'invalid';
							});
					};
					
					$scope.$watch($scope.active, function ( ) {
						invalidate();
					});
					
					
				}],
				controllerAs: 'objectTypeValidation',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
