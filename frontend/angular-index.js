import './node_modules/angular';
import './node_modules/angular-route';
import './node_modules/angular-animate';
import './node_modules/angular-i18n/angular-locale_nl-nl';
import './node_modules/angular-cookies';
import './node_modules/angular-local-storage';
import './node_modules/angular-sanitize';

export default window.angular;
