#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;### Test header end

BEGIN { use_ok('Zaaksysteem::Cache'); }

### Load cache object
{
    my $storage = {};

    throws_ok(
        sub {
            Zaaksysteem::Cache->new();
        },
        qr/Missing required storage object/,
        'Missing storage object'
    );

    my $cache = Zaaksysteem::Cache->new(storage => $storage);

    ok($cache, 'Caching object created');
}

### Generate hash of given parameters
{
    my $storage     = {};
    my $parameters  = {
        'jan' => 'frits',
        'amstel'    => 'bier',
    };
    my $create_hash = '4h1gCmnj7QPxhbCTntSsHQ';

    my $cache       = Zaaksysteem::Cache->new(storage => $storage);

    is($cache->_create_hash($parameters), $create_hash, 'MD5 Correct');
}

### Set and get cache for given KEY
{
    my $storage     = {};

    my $parameters  = {
        'jan' => 'frits',
        'amstel'    => 'bier',
    };
    my $cache_content = 'hoi';

    my $cache       = Zaaksysteem::Cache->new(storage => $storage);

    is(
        $cache->set({
            key         => 'say_hoi',
            value       => $cache_content,
            params      => $parameters
        }),
        $cache_content,
        'Set cache for "say_hoi"'
    );

    is(
        $cache->get({
            key     => 'say_hoi',
            params  => $parameters
        }),
        $cache_content,
        'Got cached content for "say_hoi" with parameters'
    );

    is(
        $cache->get({
            key     => 'say_hoi',
        }),
        $cache_content,
        'Got cached content for "say_hoi" without parameters'
    );

    ok(
        !$cache->get({
            key     => 'say_hoi',
            params  => { %{ $parameters }, 'hertog' => 'jan' }
        }),
        'Dit not find cached content because of different parameters'
    );
}

### Set and get cache for given KEY with callback
{
    my $storage     = {};

    my $valid       = 1;

    my $parameters  = {
        'jan' => 'frits',
        'amstel'    => 'bier',
    };
    my $cache_content = 'hoi';

    my $cache       = Zaaksysteem::Cache->new(storage => $storage);

    is(
        $cache->set({
            key         => 'say_hoi',
            value       => $cache_content,
            callback    => sub {
                my $valid = pop;


                return $valid;
            }
        }),
        $cache_content,
        'Set cache for "say_hoi" with callback'
    );

    is(
        $cache->get({
            key             => 'say_hoi',
            callback_args   => [$valid]
        }),
        $cache_content,
        'Got cached content for "say_hoi" with callback'
    );

    ok(
        !$cache->get({
            key             => 'say_hoi',
            callback_args   => [0]
        }),
        'Missed cached content for "say_hoi" with callback'
    );
}

### Clear cache
{
    my $storage     = {};

    my $valid       = 1;

    my $parameters  = {
        'jan' => 'frits',
        'amstel'    => 'bier',
    };
    my $cache_content = 'hoi';

    my $cache       = Zaaksysteem::Cache->new(storage => $storage);

    $cache->set({
        key         => 'say_hoi1',
        value       => $cache_content,
    });

    $cache->set({
        key         => 'say_hoi2',
        value       => $cache_content,
    });

    ok(
        $cache->clear('say_hoi1'),
        'Clear cache for "say_hoi1"'
    );

    ok(
        !exists($cache->storage->{'say_hoi1'}),
        '"say_hoi1" correctly removed'
    );

    ok(
        exists($cache->storage->{'say_hoi2'}),
        '"say_hoi2" still available'
    );

    $cache->clear;

    ok(
        !scalar(keys %{ $cache->storage }),
        'All keys correctly removed'
    );
}


zs_done_testing();
