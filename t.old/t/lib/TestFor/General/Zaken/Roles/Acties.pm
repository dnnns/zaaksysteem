package TestFor::General::Zaken::Roles::Acties;

# ./zs_prove -v t/lib/TestFor/General/Zaken/Roles/Acties.pm
use base qw(ZSTest);

use TestSetup;

sub zs_zaken_roles_actie_wijzig_status : Tests {
    $zs->txn_ok(
        sub {

            my $case = $zs->create_case_ok();

            is($case->status, 'new', "Case is new");
            is($case->afhandeldatum, undef, "Has no afhandeldatum");
            is($case->vernietigingsdatum, undef, "Has no destruction date");


            $case->wijzig_status({status => 'resolved'});
            is($case->status, 'resolved', "Case is resolved");
            isnt($case->afhandeldatum, undef, "Has afhandeldatum");
            isnt($case->vernietigingsdatum, undef, "Has destruction date");

            $case->wijzig_status({status => 'resolved'});
            is($case->status, 'resolved', "Case is overdragen");
            isnt($case->afhandeldatum, undef, "Has afhandeldatum");
            isnt($case->vernietigingsdatum, undef, "Has destruction date");

            # Hack to make sure we have a case which has been closed by regular
            # means, and make sure is_afgehandeld works ok (test should be
            # elsewhere tho
            $case->milestone($case->afhandel_fase->status);
            $case->status('open');
            ok($case->is_afgehandeld, "Is afgehandeld");
            $case->status('resolved');

            ### Prepare for opening
            ok(!$case->coordinator, 'No coordinator set');
            $case->set_behandelaar($zs->create_subject_ok(username => 'frits')->betrokkene_identifier);
            $case->discard_changes();

            $case->wijzig_status({status => 'open'});
            is($case->status, 'open', "Case is open");
            is($case->afhandeldatum, undef, "Has no afhandeldatum");
            is($case->vernietigingsdatum, undef, "Has no destruction date");
            isnt($case->streefafhandeldatum, undef, "Has streefafhandeldatum");
            isnt($case->coordinator, undef, "Case has coordinator");

            is($case->is_afgehandeld, undef, "Isnt afgehandeld");

            $case->wijzig_status({status => 'stalled'});
            is($case->status, 'stalled', "Case is stalled");
            is($case->afhandeldatum, undef, "Has no afhandeldatum");
            is($case->vernietigingsdatum, undef, "Has no destruction date");

        }, "wijzig_status"
    );
}

1;

__END__

=head1 NAME

TestFor::General::Zaken::Roles::Acties - A zaak action role tester

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
