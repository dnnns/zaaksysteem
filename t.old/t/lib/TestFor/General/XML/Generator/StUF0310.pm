package TestFor::General::XML::Generator::StUF0310;
use base 'ZSTest';

use Encode qw(encode_utf8 decode_utf8);
use File::Basename;
use File::Spec::Functions qw(catfile catdir);
use TestSetup;
use Zaaksysteem::XML::Generator::StUF0310;
use XML::Compile::Schema;

sub test_setup : Test(startup) {
    my $self = shift;

    $self->{generator} = Zaaksysteem::XML::Generator::StUF0310->new();

    my $base_dir = catdir(
        dirname(__FILE__), # StUF0310.pm
        '..',              # Generator/
        '..',              # XML/
        '..',              # General/
        '..',              # TestFor/
        '..',              # lib/
        '..',              # t/
    );

    $self->{schema} = XML::Compile::Schema->new(
        [
            catfile($base_dir, qw(share wsdl bag gml), "bag-gml.xsd"),
            map {
                catfile($base_dir, qw(share wsdl stuf), @$_)
            } (
                [qw(xlink xlinks.xsd)],
                [qw(xmlmime xmlmime.xsd)],
                [qw(0301 stuf0301mtom.xsd)],
                [qw(0301 stuf0301.xsd)],
                [qw(bg0310 entiteiten bg0310_ent_basis.xsd)],
                [qw(bg0310 entiteiten bg0310_simpleTypes.xsd)],
                [qw(bg0310 entiteiten bg0310_stuf_simpleTypes.xsd)],
                [qw(zkn0310 entiteiten zkn0310_bg0310_ent.xsd)],
                [qw(zkn0310 entiteiten zkn0310_ent_basis.xsd)],
                [qw(zkn0310 entiteiten zkn0310_simpleTypes.xsd)],
                [qw(zkn0310 entiteiten zkn0310_stuf_simpleTypes.xsd)],
                [qw(zkn0310 mutatie zkn0310_ent_mutatie.xsd)],
                [qw(zkn0310 mutatie zkn0310_msg_mutatie.xsd)],
                [qw(zkn0310 mutatie zkn0310_msg_stuf_mutatie.xsd)],
                [qw(zkn0310 vraagAntwoord zkn0310_ent_vraagAntwoord.xsd)],
                [qw(zkn0310 vraagAntwoord zkn0310_msg_stuf_vraagAntwoord.xsd)],
                [qw(zkn0310 vraagAntwoord zkn0310_msg_vraagAntwoord.xsd)],
                [qw(zkn0310 zs-dms zkn0310_msg_zs-dms.xsd)],
                [qw(zkn0310 zs-dms zkn0310_bg0310_simpleTypes_zs-dms.xsd)],
                [qw(zkn0310 zs-dms zkn0310_ent_zs-dms.xsd)],
                [qw(zkn0310 zs-dms zkn0310_msg_stuf_zs-dms.xsd)],
                [qw(zkn0310 zs-dms zkn0310_simpleTypes_zs-dms.xsd)],
            ),
        ]
    );
}

sub _dummy_stuurgegevens {
    return (
        stuurgegevens => {
            zender => { applicatie => encode_utf8("t\xebst") },
            ontvanger => { applicatie => 'test2' },
            referentienummer => 31337,
            tijdstipBericht => '20150101060000000',
            @_,
        },
    );
}

sub test_generate_case_id : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}genereerZaakIdentificatie_Di02'
    );

    my $generated_xml = $self->{generator}->generate_case_id(
        writer => {
            _dummy_stuurgegevens(functie => 'genereerZaakidentificatie'),
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Di02',
                functie => 'genereerZaakidentificatie',
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
            },
        },
        'XML::Compile can parse the generated "generate_case_id" (request) XML correctly'
    );
}

sub test_generate_case_id_return : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}genereerZaakIdentificatie_Du02'
    );

    my $generated_xml = $self->{generator}->generate_case_id_return(
        writer => {
            _dummy_stuurgegevens(functie => 'genereerZaakidentificatie'),
            zaak => {
                identificatie => { _ => '000042' },
            },
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Du02',
                functie => 'genereerZaakidentificatie',
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
            },
            zaak => {
                entiteittype => 'ZAK',
                identificatie => { _ => '000042', exact => 1 },
            },
        },
        'XML::Compile can parse the generated "generate_case_id" (response) XML correctly'
    );
}

sub test_generate_document_id : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}genereerDocumentIdentificatie_Di02'
    );

    my $generated_xml = $self->{generator}->generate_document_id(
        writer => {
            _dummy_stuurgegevens(functie => 'genereerDocumentidentificatie'),
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Di02',
                functie => 'genereerDocumentidentificatie',
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
            },
        },
        'XML::Compile can parse the generated "generate_document_id" (request) XML correctly'
    );
}

sub test_generate_document_id_response : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}genereerDocumentIdentificatie_Du02'
    );

    my $generated_xml = $self->{generator}->generate_document_id_return(
        writer => {
            _dummy_stuurgegevens(functie => 'genereerDocumentidentificatie'),
            document => {
                identificatie => 42
            }
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Du02',
                functie => 'genereerDocumentidentificatie',
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
            },
            document => {
                'entiteittype' => 'EDC',
                'identificatie' => {
                    '_' => '42',
                    'exact' => 1
                }
            },
        },
        'XML::Compile can parse the generated "generate_document_id" (response) XML correctly'
    );
}

sub test_bv03 : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/StUF0301}Bv03Bericht'
    );

    my $generated_xml = $self->{generator}->bv03(
        writer => {
            _dummy_stuurgegevens(
                crossRefnummer => 1337
            ),
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Bv03',
                crossRefnummer => 1337,
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
            },
        },
        'XML::Compile can parse the generated "Bv03Bericht" (response) XML correctly'
    );
}

sub test_fo03 : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/StUF0301}Fo03Bericht'
    );

    my $generated_xml = $self->{generator}->fo03(
        writer => {
            _dummy_stuurgegevens(
                crossRefnummer => 1337
            ),
            body => {
                code => '666',
                plek => 'server',
                omschrijving => 'Something <went> wrong',
                details => 'REALLY wrong',
            },
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Fo03',
                crossRefnummer => 1337,
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
            },
            body => {
                code => '666',
                plek => 'server',
                omschrijving => 'Something <went> wrong',
                details => 'REALLY wrong',
            },
        },
        'XML::Compile can parse the generated "Fo03Bericht" (response) XML correctly'
    );
}

sub test_write_case : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}zakLk01'
    );

    my $xml_input = {
        _dummy_stuurgegevens(entiteittype => 'ZAK'),
        parameters => {
            mutatiesoort      => 'W',
            indicatorOvername => 'V',
        },
        object => [
            { identificatie => '000042' },
            {
                identificatie      => '000042',
                startdatum         => { _ => '20150101' },
                registratiedatum   => { _ => '20130101' },
                zaakniveau         => { _ => 1 },
                deelzakenIndicatie => { _ => 'N' },

                einddatum        => { _ => '20120101' },
                einddatumGepland => { _ => '20110101' },
                resultaat        => {
                    omschrijving => 'klaar',
                    toelichting  => 'Echt waar',
                },
                heeftAlsInitiator => [
                    {
                        gerelateerde =>
                            { medewerker => { identificatie => 'pietje' } }
                    }
                ],
                heeftAlsUitvoerende => [
                    {
                        gerelateerde =>
                            { medewerker => { identificatie => 'keesje' } }
                    }
                ],
                heeftAlsGemachtigde => [
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456789' }
                        }
                    },
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790' }
                        }
                    }
                ],
                heeftAlsVerantwoordelijke => [
                    { gerelateerde => { identificatie => 'admin' } }
                ],
                heeftAlsBelanghebbende => [
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790' }
                        }
                    },
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790' }
                        }
                    }
                ],
                heeftAlsOverigBetrokkene => [
                    {
                        gerelateerde => {
                            vestiging =>
                                { 'vestigingsNummer' => '123123123123' }
                        }
                    },
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790' }
                        }
                    }
                ],
                heeft => [
                    {
                        gerelateerde => {
                            volgnummer   => 1,
                            omschrijving => 'woeoeoe',
                        },
                    },
                    {
                        gerelateerde => {
                            volgnummer   => 2,
                            omschrijving => 'Nog een status',
                        },
                    }
                ],
                kenmerk => [
                    {
                        kenmerk => { _ => 'ken' },
                        bron    => { _ => 'merk' },
                    },
                ],
# 2015-11 XML::Compile doesn't handle "isVan" without throwing an error.
#                isVan => {
#                    gerelateerde => {
#                        code => { _ => 'ABC123' },
#                    },
#                },
            }
        ]
    };

    my $expected = {
        'object' => [
            {
                'identificatie' => {
                    '_'     => '000042',
                    'exact' => 1
                }
            },
            {
                'deelzakenIndicatie' => {
                    '_'     => 'N',
                    'exact' => 1
                },
                'einddatum' => {
                    '_' => bless(
                        {
                            '_e'   => [0],
                            '_es'  => '+',
                            '_m'   => [20120101],
                            'sign' => '+'
                        },
                        'Math::BigFloat'
                    ),
                    'exact'               => 1,
                    'indOnvolledigeDatum' => 'V'
                },
                'einddatumGepland' => {
                    '_' => bless(
                        {
                            '_e'   => [0],
                            '_es'  => '+',
                            '_m'   => [20110101],
                            'sign' => '+'
                        },
                        'Math::BigFloat'
                    ),
                    'exact'               => 1,
                    'indOnvolledigeDatum' => 'V'
                },
                'heeft' => [
                    {
                        'gerelateerde' => {
                            'omschrijving' => {
                                '_'     => 'woeoeoe',
                                'exact' => 1
                            },
                            'volgnummer' => {
                                '_'     => '1',
                                'exact' => 1
                            }
                        }
                    },
                    {
                        'gerelateerde' => {
                            'omschrijving' => {
                                '_'     => 'Nog een status',
                                'exact' => 1
                            },
                            'volgnummer' => {
                                '_'     => '2',
                                'exact' => 1
                            }
                        }
                    }
                ],
                'heeftAlsBelanghebbende' => [
                    {
                        'gerelateerde' => {
                            'natuurlijkPersoon' => {
                                'entiteittype' => 'NPS',
                                'inp.bsn'      => '123456790'
                            }
                        }
                    },
                    {
                        'gerelateerde' => {
                            'natuurlijkPersoon' => {
                                'entiteittype' => 'NPS',
                                'inp.bsn'      => '123456790'
                            }
                        }
                    }
                ],
                'heeftAlsGemachtigde' => [
                    {
                        'gerelateerde' => {
                            'natuurlijkPersoon' => {
                                'entiteittype' => 'NPS',
                                'inp.bsn'      => '123456789'
                            }
                        }
                    },
                    {
                        'gerelateerde' => {
                            'natuurlijkPersoon' => {
                                'entiteittype' => 'NPS',
                                'inp.bsn'      => '123456790'
                            }
                        }
                    }
                ],
                'heeftAlsInitiator' => [
                    {
                        'gerelateerde' => {
                            'medewerker' => {
                                'entiteittype'  => 'MDW',
                                'identificatie' => {
                                    '_'     => 'pietje',
                                    'exact' => 1
                                }
                            }
                        }
                    },
                ],
                'heeftAlsOverigBetrokkene' => [
                    {
                        'gerelateerde' => {
                            'vestiging' => {
                                'entiteittype' => 'VES',
                                'vestigingsNummer' => {
                                    '_'            => '123123123123',
                                    'exact'        => 1
                                }
                            }
                        }
                    },
                    {
                        'gerelateerde' => {
                            'natuurlijkPersoon' => {
                                'entiteittype' => 'NPS',
                                'inp.bsn'      => '123456790'
                            }
                        }
                    }
                ],
                'heeftAlsUitvoerende' => [
                    {
                        'gerelateerde' => {
                            'medewerker' => {
                                'entiteittype'  => 'MDW',
                                'identificatie' => {
                                    '_'     => 'keesje',
                                    'exact' => 1
                                }
                            }
                        }
                    },
                ],
                'heeftAlsVerantwoordelijke' => [
                    {
                        'gerelateerde' => {
                            'identificatie' => {
                                '_'     => 'admin',
                                'exact' => 1
                            }
                        }
                    },
                ],
                'identificatie' => {
                    '_'     => '000042',
                    'exact' => 1
                },
                'kenmerk' => [
                    {
                        'kenmerk' => { _ => 'ken', exact => 1 },
                        'bron'    => { _ => 'merk', exact => 1 },
                    },
                ],

                # 2015-11 XML::Compile doesn't handle "isVan" without throwing an error.
                #                isVan => {
                #                    entiteittype => 'ZAKZKT',
                #                    gerelateerde => {
                #                        entiteittype => 'ZKT',
                #                        code => { _ => 'ABC123' },
                #                    },
                #                },
                'registratiedatum' => {
                    '_' => bless(
                        {
                            '_e'   => [0],
                            '_es'  => '+',
                            '_m'   => [20130101],
                            'sign' => '+'
                        },
                        'Math::BigFloat'
                    ),
                    'exact'               => 1,
                    'indOnvolledigeDatum' => 'V'
                },
                'resultaat' => {
                    'omschrijving' => {
                        '_'     => 'klaar',
                        'exact' => 1
                    },
                    'toelichting' => {
                        '_'     => 'Echt waar',
                        'exact' => 1
                    }
                },
                'startdatum' => {
                    '_' => bless(
                        {
                            '_e'   => [0],
                            '_es'  => '+',
                            '_m'   => [20150101],
                            'sign' => '+'
                        },
                        'Math::BigFloat'
                    ),
                    'exact'               => 1,
                    'indOnvolledigeDatum' => 'V'
                },
                'zaakniveau' => {
                    '_'     => 1,
                    'exact' => 1
                },
            }
        ],
        'parameters' => {
            'indicatorOvername' => 'V',
            'mutatiesoort'      => 'W'
        },
        'stuurgegevens' => {
            'berichtcode'      => 'Lk01',
            'entiteittype'     => 'ZAK',
            'ontvanger'        => { 'applicatie' => 'test2' },
            'referentienummer' => '31337',
            'tijdstipBericht'  => '20150101060000000',
            'zender'           => { 'applicatie' => "t\x{eb}st" }
        }
    };

    {
        my $generated_xml = $self->{generator}->write_case(writer => $xml_input);
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "zakLk01" (request) XML correctly'
        );
    }

    for my $optional (qw(
        heeftAlsInitiator
        heeftAlsUitvoerende
        heeftAlsGemachtigde
        heeftAlsVerantwoordelijke
        heeftAlsBelanghebbende
        heeftAlsOverigBetrokkene
        heeft
        kenmerk
    )) {
        delete $xml_input->{object}[1]{$optional};
        delete $expected->{object}[1]{$optional};
    }

    {
        my $generated_xml = $self->{generator}->write_case(writer => $xml_input);
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "zakLk01" (request; without some optional bits) XML correctly'
        );
    }
}

sub test_case_response : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}zakLa01'
    );

    my $xml_input = {
        _dummy_stuurgegevens(entiteittype => 'ZAK'),
        antwoord => {
            object => {
                identificatie => '000042',
                resultaat     => {
                    omschrijving => 'klaar',
                    toelichting  => 'Echt waar',
                },
                startdatum          => { _ => '20150101' },
                registratiedatum    => { _ => '20140101' },
                einddatumGepland    => { _ => '20130101' },
                uiterlijkeEinddatum => { _ => '20120101' },
                einddatum           => { _ => '20110101' },
                datumVernietigingDossier =>
                    { _ => 'NIL', noValue => 'waardeOnbekend' },
                zaakniveau         => { _ => 1 },
                deelzakenIndicatie => { _ => 'N' },

                heeftAlsInitiator => {
                        gerelateerde =>
                            { medewerker => { identificatie => 'pietje' } }
                },
                heeftAlsUitvoerende => {
                    gerelateerde =>
                        { medewerker => { identificatie => 'keesje' } }
                },
                heeftAlsGemachtigde => [
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456789', authentiek => 'N' }
                        }
                    },
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790', authentiek => 'J' }
                        }
                    }
                ],
                heeftAlsVerantwoordelijke => {
                    gerelateerde =>
                        { medewerker => { identificatie => 'arnetje' } }
                },
                heeftAlsBelanghebbende => [
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790', authentiek => 'J'  }
                        }
                    },
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790', authentiek => 'J'  }
                        }
                    }
                ],
                heeftAlsOverigBetrokkene => [
                    {
                        gerelateerde => {
                            vestiging =>
                                { 'vestigingsNummer' => '123123123123' }
                        }
                    },
                    {
                        gerelateerde => {
                            natuurlijkPersoon => { 'inp.bsn' => '123456790', authentiek => 'J'  }
                        }
                    }
                ],
                heeft => [
                    {
                        gerelateerde => {
                            volgnummer   => 1,
                            omschrijving => 'woeoeoe',
                            omschrijvingGeneriek => 'wieoewieoe',
                        },
                        toelichting => 'Toelichting hier',
                        indicatieLaatsteStatus => 'N',
                    },
                    {
                        gerelateerde => {
                            volgnummer   => 2,
                            omschrijving => 'Nog een status',
                            omschrijvingGeneriek => 'Hier kan een omschrijving, generiek'
                        },
                        toelichting => 'En nog een toelichting ook',
                        indicatieLaatsteStatus => 'J',
                    }
                ],
                heeftRelevant => [
                    {
                        gerelateerde => {
                            identificatie           => { _ => 42 },
                            creatiedatum            => { _ => '20110101' },
                            titel                   => { _ => 'Documenttitel' },
                            formaat                 => { _ => '.odt' },
                            versie                  => { _ => 5 },
                            verzenddatum            => { _ => '20120101' },
                            vertrouwelijkAanduiding => { _ => 'OPENBAAR' },
                            auteur                  => { _ => 'Freek Frikandel' },
                        },
                    },
                ],
                isVan => {
                    gerelateerde => {
                        omschrijving => 'Een zaaktype',
                        code         => 'ABC123',
                    },
                },
            }
        }
    };

    my $expected = {
        'antwoord' => {
            'object' => [
                {
                    'datumVernietigingDossier' => {
                        '_'                   => 'NIL',
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V',
                        'noValue'             => 'waardeOnbekend'
                    },
                    'deelzakenIndicatie' => {
                        '_'     => 'N',
                        'exact' => 1
                    },
                    'einddatum' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20110101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    },
                    'einddatumGepland' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20130101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    },
                    'heeft' => [
                        {
                            'entiteittype' => 'ZAKSTT',
                            'gerelateerde' => {
                                'entiteittype' => 'STT',
                                'omschrijving' => {
                                    '_'     => 'woeoeoe',
                                    'exact' => 1
                                },
                                'omschrijvingGeneriek' => {
                                    '_'     => 'wieoewieoe',
                                    'exact' => 1
                                },
                                'volgnummer' => {
                                    '_'     => '1',
                                    'exact' => 1
                                }
                            },
                            'indicatieLaatsteStatus' => {
                                '_'     => 'N',
                                'exact' => 1
                            },
                            'toelichting' => {
                                '_'     => 'Toelichting hier',
                                'exact' => 1
                            }
                        },
                        {
                            'entiteittype' => 'ZAKSTT',
                            'gerelateerde' => {
                                'entiteittype' => 'STT',
                                'omschrijving' => {
                                    '_'     => 'Nog een status',
                                    'exact' => 1
                                },
                                'omschrijvingGeneriek' => {
                                    '_' =>
                                        'Hier kan een omschrijving, generiek',
                                    'exact' => 1
                                },
                                'volgnummer' => {
                                    '_'     => '2',
                                    'exact' => 1
                                }
                            },
                            'indicatieLaatsteStatus' => {
                                '_'     => 'J',
                                'exact' => 1
                            },
                            'toelichting' => {
                                '_'     => 'En nog een toelichting ook',
                                'exact' => 1
                            }
                        }
                    ],
                    'heeftAlsBelanghebbende' => [
                        {
                            'entiteittype' => 'ZAKBTRBLH',
                            'gerelateerde' => {
                                'natuurlijkPersoon' => {
                                    'authentiek' => {
                                        '_'           => 'J',
                                        'exact'       => 1,
                                        'metagegeven' => 1
                                    },
                                    'entiteittype' => 'NPS',
                                    'inp.bsn'      => '123456790'
                                }
                            }
                        },
                        {
                            'entiteittype' => 'ZAKBTRBLH',
                            'gerelateerde' => {
                                'natuurlijkPersoon' => {
                                    'authentiek' => {
                                        '_'           => 'J',
                                        'exact'       => 1,
                                        'metagegeven' => 1
                                    },
                                    'entiteittype' => 'NPS',
                                    'inp.bsn'      => '123456790'
                                }
                            }
                        }
                    ],
                    'heeftAlsGemachtigde' => [
                        {
                            'entiteittype' => 'ZAKBTRGMC',
                            'gerelateerde' => {
                                'natuurlijkPersoon' => {
                                    'authentiek' => {
                                        '_'           => 'N',
                                        'exact'       => 1,
                                        'metagegeven' => 1
                                    },
                                    'entiteittype' => 'NPS',
                                    'inp.bsn'      => '123456789'
                                }
                            }
                        },
                        {
                            'entiteittype' => 'ZAKBTRGMC',
                            'gerelateerde' => {
                                'natuurlijkPersoon' => {
                                    'authentiek' => {
                                        '_'           => 'J',
                                        'exact'       => 1,
                                        'metagegeven' => 1
                                    },
                                    'entiteittype' => 'NPS',
                                    'inp.bsn'      => '123456790'
                                }
                            }
                        }
                    ],
                    'heeftAlsInitiator' => [
                        {
                            'entiteittype' => 'ZAKBTRINI',
                            'gerelateerde' => {
                                'medewerker' => {
                                    'entiteittype'  => 'MDW',
                                    'identificatie' => {
                                        '_'     => 'pietje',
                                        'exact' => 1
                                    }
                                }
                            }
                        }
                    ],
                    'heeftAlsOverigBetrokkene' => [
                        {
                            'entiteittype' => 'ZAKBTROVR',
                            'gerelateerde' => {
                                'vestiging' => {
                                    'entiteittype'     => 'VES',
                                    'vestigingsNummer' => {
                                        '_'     => '123123123123',
                                        'exact' => 1
                                    }
                                }
                            }
                        },
                        {
                            'entiteittype' => 'ZAKBTROVR',
                            'gerelateerde' => {
                                'natuurlijkPersoon' => {
                                    'authentiek' => {
                                        '_'           => 'J',
                                        'exact'       => 1,
                                        'metagegeven' => 1
                                    },
                                    'entiteittype' => 'NPS',
                                    'inp.bsn'      => '123456790'
                                }
                            }
                        }
                    ],
                    'heeftAlsUitvoerende' => [
                        {
                            'entiteittype' => 'ZAKBTRUTV',
                            'gerelateerde' => {
                                'medewerker' => {
                                    'entiteittype'  => 'MDW',
                                    'identificatie' => {
                                        '_'     => 'keesje',
                                        'exact' => 1
                                    }
                                }
                            }
                        }
                    ],
                    'heeftAlsVerantwoordelijke' => [
                        {
                            'entiteittype' => 'ZAKBTRVRA',
                            'gerelateerde' => {
                                'medewerker' => {
                                    'entiteittype'  => 'MDW',
                                    'identificatie' => {
                                        '_'     => 'arnetje',
                                        'exact' => 1
                                    }
                                }
                            }
                        }
                    ],
                    'heeftRelevant' => [
                        {
                            'entiteittype' => 'ZAKEDC',
                            'gerelateerde' => {
                                'auteur' => {
                                    '_'     => 'Freek Frikandel',
                                    'exact' => 1
                                },
                                'creatiedatum' => {
                                    '_' => bless(
                                        {
                                            '_e'   => [0],
                                            '_es'  => '+',
                                            '_m'   => [20110101],
                                            'sign' => '+'
                                        },
                                        'Math::BigFloat'
                                    ),
                                    'exact'               => 1,
                                    'indOnvolledigeDatum' => 'V'
                                },
                                'formaat' => {
                                    '_'     => '.odt',
                                    'exact' => 1
                                },
                                'identificatie' => {
                                    '_'     => '42',
                                    'exact' => 1
                                },
                                'taal' => {
                                    '_'       => 'NIL',
                                    'exact'   => 1,
                                    'noValue' => 'waardeOnbekend'
                                },
                                'titel' => {
                                    '_'     => 'Documenttitel',
                                    'exact' => 1
                                },
                                'versie' => {
                                    '_'     => '5',
                                    'exact' => 1
                                },
                                'vertrouwelijkAanduiding' => {
                                    '_'     => 'OPENBAAR',
                                    'exact' => 1
                                }
                            }
                        }
                    ],
                    'identificatie' => {
                        '_'     => '000042',
                        'exact' => 1
                    },
                    'isVan' => [
                        {
                            'entiteittype' => 'ZAKZKT',
                            'gerelateerde' => {
                                'code' => {
                                    '_'     => 'ABC123',
                                    'exact' => 1
                                },
                                'omschrijving' => {
                                    '_'     => 'Een zaaktype',
                                    'exact' => 1
                                }
                            }
                        }
                    ],
                    'registratiedatum' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20140101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    },
                    'resultaat' => {
                        'omschrijving' => {
                            '_'     => 'klaar',
                            'exact' => 1
                        },
                        'toelichting' => {
                            '_'     => 'Echt waar',
                            'exact' => 1
                        }
                    },
                    'startdatum' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20150101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    },
                    'uiterlijkeEinddatum' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20120101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    },
                    'zaakniveau' => {
                        '_'     => 1,
                        'exact' => 1
                    }
                }
            ]
        },
        'parameters'    => { 'indicatorVervolgvraag' => 0 },
        'stuurgegevens' => {
            'berichtcode'      => 'La01',
            'entiteittype'     => 'ZAK',
            'ontvanger'        => { 'applicatie' => 'test2' },
            'referentienummer' => '31337',
            'tijdstipBericht'  => '20150101060000000',
            'zender'           => { 'applicatie' => "t\x{eb}st" }
        }
    };

    {
        my $generated_xml = $self->{generator}->case_response(writer => $xml_input);
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "zakLa01" (response) XML correctly'
        );
    }
}

sub test_get_case_details : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}zakLv01'
    );

    my $generated_xml = $self->{generator}->get_case_details(
        writer => {
            _dummy_stuurgegevens(entiteittype => 'ZAK'),
            gelijk => {
                identificatie => '000042'
            },
        }
    );

    my $parsed_xml = $reader->($generated_xml);

    cmp_deeply(
        $parsed_xml,
        {
            stuurgegevens => {
                berichtcode => 'Lv01',
                ontvanger => {
                    applicatie => 'test2',
                },
                zender => {
                    applicatie => "t\xebst"
                },
                referentienummer => 31337,
                tijdstipBericht => '20150101060000000',
                entiteittype => 'ZAK',
            },
            parameters => {
                indicatorVervolgvraag => 0,
                sortering => 1
            },
            gelijk => {
                entiteittype => 'ZAK',
                identificatie => { _ => '000042', exact => 1 },
            },
        },
        'XML::Compile can parse the generated "zakLv01" (request) XML correctly'
    );
}

sub test_document_response : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}edcLa01'
    );

    my $xml_input = {
        _dummy_stuurgegevens(entiteittype => 'EDC'),
        antwoord => {
            object => {
                identificatie => { _ => '000042' },
                creatiedatum  => { _ => '20150101' },
                titel         => { _ => 'Some title' },
                formaat       => { _ => '.odt' },
                versie        => { _ => '42' },
                verzenddatum  => { _ => '20140101' },
                vertrouwelijkAanduiding =>
                    { _ => 'NIL', noValue => 'waardeOnbekend' },
                auteur => "Ghost Writer",
                inhoud => {
                    _            => 'inhoudelijk',
                    bestandsnaam => 'demo.txt',
                    contentType  => 'text/plain',
                },
                isRelevantVoor =>
                    { gerelateerde => { identificatie => '0000666', }, },
            },
        },
    };

    my $expected = {
        'antwoord' => {
            'object' => [
                {
                    'auteur' => {
                        '_'     => 'Ghost Writer',
                        'exact' => 1
                    },
                    'creatiedatum' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20150101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    },
                    'formaat' => {
                        '_'     => '.odt',
                        'exact' => 1
                    },
                    'identificatie' => {
                        '_'     => '000042',
                        'exact' => 1
                    },
                    'inhoud' => {
                        '_'            => 'inhoudelijk',
                        'bestandsnaam' => 'demo.txt',
                        'contentType'  => 'text/plain'
                    },
                    'isRelevantVoor' => [
                        {
                            'gerelateerde' => {
                                'identificatie' => {
                                    '_'     => '0000666',
                                    'exact' => 1
                                }
                            }
                        }
                    ],
                    'taal' => {
                        '_'       => 'NIL',
                        'exact'   => 1,
                        'noValue' => 'waardeOnbekend'
                    },
                    'titel' => {
                        '_'     => 'Some title',
                        'exact' => 1
                    },
                    'versie' => {
                        '_'     => '42',
                        'exact' => 1
                    },
                    'vertrouwelijkAanduiding' => {
                        '_'       => 'NIL',
                        'exact'   => 1,
                        'noValue' => 'waardeOnbekend'
                    },
                    'verzenddatum' => {
                        '_' => bless(
                            {
                                '_e'   => [0],
                                '_es'  => '+',
                                '_m'   => [20140101],
                                'sign' => '+'
                            },
                            'Math::BigFloat'
                        ),
                        'exact'               => 1,
                        'indOnvolledigeDatum' => 'V'
                    }
                }
            ]
        },
        'parameters'    => { 'indicatorVervolgvraag' => 0 },
        'stuurgegevens' => {
            'berichtcode'      => 'La01',
            'entiteittype'     => 'EDC',
            'ontvanger'        => { 'applicatie' => 'test2' },
            'referentienummer' => '31337',
            'tijdstipBericht'  => '20150101060000000',
            'zender'           => { 'applicatie' => "t\x{eb}st" }
        }
    };


    {
        my $generated_xml = $self->{generator}->document_response(
            writer => $xml_input
        );
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "edcLa01" (response) XML correctly'
        );
    }

    {
        $xml_input->{antwoord}{object}{ontvangstdatum} = delete $xml_input->{antwoord}{object}{verzenddatum};
        $xml_input->{antwoord}{object}{vertrouwelijkAanduiding} = { _ => 'OPENBAAR' };
        delete $xml_input->{antwoord}{object}{auteur};

        $expected->{antwoord}{object}[0]{ontvangstdatum} = delete $expected->{antwoord}{object}[0]{verzenddatum};
        $expected->{antwoord}{object}[0]{vertrouwelijkAanduiding} = { _ => 'OPENBAAR', exact => 1 };
        delete $expected->{antwoord}{object}[0]{auteur};

        my $generated_xml = $self->{generator}->document_response(
            writer => $xml_input
        );
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "edcLa01" (response; with optional fields used differently) XML correctly'
        );
    }
}

sub test_write_case_document : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}edcLk01'
    );

    my $xml_input = {
        _dummy_stuurgegevens(entiteittype => 'EDC'),
        parameters => {
            mutatiesoort => 'T',
        },
        object => [
            {
                identificatie => { _ => '000042' },
                titel         => { _ => 'Some title' },
                taal          => { _ => 'NL' },
                formaat       => { _ => '.odt' },
                verzenddatum  => { _ => '20140101' },
                vertrouwelijkAanduiding => 'OPENBAAR',
                auteur => "Ghost Writer",
                inhoud => {
                    _            => 'inhoudelijk',
                    bestandsnaam => 'demo.txt',
                    contentType  => 'text/plain',
                },
                isRelevantVoor =>
                    { gerelateerde => { identificatie => '0000666', }, },
            },
        ],
    };

    my $expected = {
        'object' => [
            {
                'auteur' => {
                    '_'     => 'Ghost Writer',
                    'exact' => 1
                },
                'identificatie' => {
                    '_'     => '000042',
                    'exact' => 1
                },
                'inhoud' => {
                    '_'            => 'inhoudelijk',
                    'bestandsnaam' => 'demo.txt',
                    'contentType'  => 'text/plain'
                },
                'isRelevantVoor' => [
                    {
                        'gerelateerde' => {
                            'identificatie' => {
                                '_'     => '0000666',
                                'exact' => 1
                            }
                        }
                    }
                ],
                'taal' => {
                    '_'     => 'NL',
                    'exact' => 1
                },
                'titel' => {
                    '_'     => 'Some title',
                    'exact' => 1
                },
                'vertrouwelijkAanduiding' => {
                    '_'     => 'OPENBAAR',
                    'exact' => 1
                },
                'verzenddatum' => {
                    '_' => bless(
                        {
                            '_e'   => [0],
                            '_es'  => '+',
                            '_m'   => [20140101],
                            'sign' => '+'
                        },
                        'Math::BigFloat'
                    ),
                    'exact'               => 1,
                    'indOnvolledigeDatum' => 'V'
                }
            }
        ],
        'parameters' => {
            'indicatorOvername' => 'V',
            'mutatiesoort'      => 'T'
        },
        'stuurgegevens' => {
            'berichtcode'      => 'Lk01',
            'entiteittype'     => 'EDC',
            'ontvanger'        => { 'applicatie' => 'test2' },
            'referentienummer' => '31337',
            'tijdstipBericht'  => '20150101060000000',
            'zender'           => { 'applicatie' => "t\x{eb}st" }
        }
    };

    {
        my $generated_xml = $self->{generator}->write_case_document(
            writer => $xml_input
        );
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "edcLk01" (response; with optional fields used differently) XML correctly'
        );
    }
}

sub test_get_case_document : Tests {
    my $self = shift;
    my $schema = $self->{schema};

    my $reader = $schema->compile(READER =>
        '{http://www.egem.nl/StUF/sector/zkn/0310}edcLv01'
    );

    my $xml_input = {
        _dummy_stuurgegevens(entiteittype => 'EDC'),
        gelijk => {
            identificatie => '5',
        },
        scope => {
            object => {
                isRelevantVoor => {
                    gerelateerde => {
                        identificatie => '00001337'
                    },
                },
            },
        },
    };

    my $expected = {
        'gelijk' => {
            'identificatie' => {
                '_'     => '5',
                'exact' => 1
            }
        },
        'parameters' => {
            'indicatorVervolgvraag' => 0,
            'sortering'             => 1
        },
        'scope' => {
            'object' => {
                'isRelevantVoor' => {
                    'gerelateerde' => {
                        'identificatie' => {
                            '_'     => '00001337',
                            'exact' => 1
                        }
                    }
                }
            }
        },
        'stuurgegevens' => {
            'berichtcode'      => 'Lv01',
            'entiteittype'     => 'EDC',
            'ontvanger'        => { 'applicatie' => 'test2' },
            'referentienummer' => '31337',
            'tijdstipBericht'  => '20150101060000000',
            'zender'           => { 'applicatie' => "t\x{eb}st" }
        }
    };

    {
        my $generated_xml = $self->{generator}->get_case_document(
            writer => $xml_input
        );
        my $parsed_xml = $reader->($generated_xml);

        cmp_deeply(
            $parsed_xml,
            $expected,
            'XML::Compile can parse the generated "edcLk01" (response; with optional fields used differently) XML correctly'
        );
    }
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
