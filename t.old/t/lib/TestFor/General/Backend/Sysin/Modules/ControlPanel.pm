package TestFor::General::Backend::Sysin::Modules::ControlPanel;

use base 'ZSTest';

use TestSetup;
use Zaaksysteem::Backend::Sysin::Modules::ControlPanel;

=head1 NAME

TestFor::General::Backend::Sysin::Modules::ControlPanel - A control panel tester

=head1 SYNOPSIS

    TEST_METHOD='zs_control_panel.*' ./zs_prove -v t/testclass

=head1 TEST_METHODS

=head2 zs_control_panel

Unit test the Control Panel interface.

=cut


sub zs_control_panel : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $zs->create_interface_ok(
                module           => 'controlpanel',
                name             => 'Controlpanel',
                interface_config => {
                    medewerker => {
                        object_type => "medewerker",
                        id          => 1,
                        naam        => "A. Admin",
                        username    => "admin"
                    },
                    api_key => "meuk",
                    domainname => 'Hoppa',
                    pip => 1,
                },
            );
        },
        "Control panel interface test"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
