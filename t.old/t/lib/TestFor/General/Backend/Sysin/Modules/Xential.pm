package TestFor::General::Backend::Sysin::Modules::Xential;
use base qw(ZSTest);

use TestSetup;
use File::Temp qw/tempfile  :seekable/;
use JSON::XS;
use Test::MockObject;

=head1 NAME

TestFor::General::Backend::Sysin::Modules::Xential - Zaaksysteem Xential tests

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Backend/Sysin/Modules/Xential.pm

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head2 test_spoof_mode

Test the spoof mode of the interface

=cut

sub test_spoof_mode : Tests {
    $zs->txn_ok(
        sub {
            my $interface = $zs->create_xential_interface_ok();

            my $case = $zs->create_case_ok();

            my %params = (
                case          => $case->id,
                template_uuid => $zs->generate_uuid,
                document_title =>
                    Zaaksysteem::TestUtils::generate_random_string(),
            );

            my $pt = $interface->process_trigger('spoofmode', \%params);
            my $rs = $pt->records({}, { order => '-desc' });
            is($rs->count, 1, "Got one record");

            my $record = $rs->first;
            ok(!$record->is_error, "No error");

            my $output = decode_json($record->output);
            is($output->{transaction_id}, $pt->id, "We have reference to our transaction ID");

        }, "Xential interface tests"
    );
}

sub test_xential_create_document : Tests {
    my $self = shift;
    $zs->txn_ok(
        sub {
            my $interface = $zs->create_xential_interface_ok();

            my $case = $zs->create_case_ok();
            $case->discard_changes;

            my $subject = $zs->create_subject_ok();

            my %params = (
                case          => $case->id,
                template_uuid => $zs->generate_uuid,
                document_title =>
                    Zaaksysteem::TestUtils::generate_random_string(),
                subject => $subject->id,
            );

            my $opts;
            no warnings qw(redefine once);
            local *Zaaksysteem::Xential::create_document_from_template = sub {
                my $self = shift;
                $opts = {@_};
                return { res => "You called Xential" };
            };
            use warnings;

            my $pt = $interface->process_trigger('create_file_from_template', \%params);
            $pt->discard_changes;

            my $rs = $pt->records({}, { order => '-desc' });
            is($rs->count, 1, "Got one record");

            my $record = $rs->first;
            ok(!$record->is_error, "No error");

            is_deeply(
                $opts,
                {
                    case           => $case,
                    template_uuid  => $params{template_uuid},
                    document_title => $params{document_title},
                    transaction    => $pt,
                    username       => $subject->username,
                },
                "Options to 'Zaaksysteem::Xential::create_document_from_template' valid"
            );

            my $fh      = $self->_get_test_fh;
            my $files   = $interface->process_api_trigger(
                'api_post_file',
                {
                    user    => undef,
                    uploads => $fh,
                    method  => 'post',
                    request_params => {
                        case_uuid           => $case->object_data->uuid,
                        transaction_uuid    => $pt->uuid,
                    }
                }
            );
            is($files->count, 1, "One file added");

        }, "Xential interface tests"
    );
}

=head2 xential_usage_api_post_file

Triggers a xential post upload

=cut

sub xential_usage_api_post_file : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $iface   = $self->_create_xential_interface;
        my ($trans, $case, $case_document_id) = $self->_create_xential_transaction($iface);

        my $fh      = $self->_get_test_fh;
        my $files   = $iface->process_api_trigger(
            'api_post_file',
            {
                user    => undef,
                uploads => $fh,
                method  => 'post',
                request_params => {
                    case_uuid           => $case->object_data->uuid,
                    transaction_uuid    => $trans->uuid,
                }
            }
        );

        my $file = $files->first;

        is($file->filestore_id->original_name, 'xentialfile.doc.docx', 'Correct filename set');
        is($file->case_documents->search->first->case_document_id->id, $case_document_id, 'Correct case_document set');
        is($file->filestore_id->size, 27, 'Found 27 character content');
        ok($file->case_id, 'File has an existing case');

        my $message = $schema->resultset('Message')->search({ subject_id => $file->get_column('created_by') })->first;

        if (ok($message, "Got a message from.. " . $file->created_by)) {
            is(
                $message->message,
                'Sjabloon toegevoegd aan zaak: xentialfile.doc',
                'Found correct message: "message"'
            );
            like(
                $message->logging_id->onderwerp,
                qr/Sjabloon toegevoegd aan zaak \d+: xentialfile.doc/,
                'Found correct log entry: "Onderwerp"'
            );
        }



    }, 'Post a file', 1)

}

=head2 xential_usage_edit_file

Requests an edit from xential

=cut

sub xential_usage_edit_file : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $iface   = $self->_create_xential_interface;
        my $editurl = $iface->get_interface_config->{editservice};
        my $curuser = $zs->set_current_user();

        my $case    = $zs->create_case_ok;
        my $file    = $zs->create_file_ok(db_params => { case => $case });

        # USAGE:
        my $rv      = $iface->process_trigger('request_edit_file',
            {
                file_id             => $file->id,
                subject             => $curuser->id,            }
        );


        my $url     = $rv->{redirect_url};
        ok($url, 'Got a redirect URL');

        # Strip params from redirect url
        $editurl =~ s/\?.*$//;
        like($url, qr/^$editurl/, 'Got editservice as prefix in redirect_url');
        like($url, qr/$_=[a-f0-9]/, "Got UUID as value in redirect_url for param $_") for (qw/
            file_uuid
            transaction_uuid
            interface_uuid
        /);

        throws_ok(
            sub {
                $iface->process_trigger('request_edit_file',
                    {
                        file_id             => ($file->id + 1),
                        subject             => $curuser->id,                    }
                );
            },
            qr/file_not_found/,
            'Exception test: File not found'
        );

        ### Exception: no edit service
        {
            $iface->update_interface_config(
                {
                    %{ $iface->get_interface_config },
                    editservice => undef,
                }
            );

            $iface->update;

            throws_ok(
                sub {
                    $iface->process_trigger('request_edit_file',
                        {
                            file_id             => $file->id,
                            subject             => $curuser->id,                        }
                    );
                },
                qr/no_edit_service/,
                'Exception test: No edit service'
            );
        }

    }, 'Request file edit', 1)

}

=head2 xential_usage_get_file

Gets a file for xential

=cut

sub xential_usage_get_file : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $iface   = $self->_create_xential_interface;
        my $editurl = $iface->get_interface_config->{editservice};
        my $curuser = $zs->set_current_user();

        my $case    = $zs->create_case_ok;
        my $file    = $zs->create_file_ok(db_params => { case => $case });

        # USAGE:
        my $rv      = $iface->process_trigger('request_edit_file',
            {
                file_id             => $file->id,
                subject             => $curuser->id,            }
        );

        my $uri     = URI->new($rv->{redirect_url});
        my %params  = $uri->query_form;

        $rv         = $iface->process_api_trigger('api_get_file',
            {
                request_params  => {
                    file_uuid           => $file->filestore_id->uuid,
                    transaction_uuid    => $params{transaction_uuid},
                },
                method          => 'post'
            }
        );

        isa_ok($rv, 'Zaaksysteem::Model::DB::File');
    }, 'Request file edit', 1)

}

=head2 xential_usage_edit_and_post

Requests an edit from xential, gets and posts the file.

=cut

sub xential_usage_edit_and_post : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $iface   = $self->_create_xential_interface;
        my $editurl = $iface->get_interface_config->{editservice};
        my $curuser = $zs->set_current_user();
        my $case    = $zs->create_case_ok;
        my $file    = $zs->create_file_ok(db_params => { case => $case });


        # USAGE, request file:
        my $rv      = $iface->process_trigger('request_edit_file',
            {
                file_id             => $file->id,
                subject             => $curuser->id,
            }
        );

        my $uri     = URI->new($rv->{redirect_url});
        my %params  = $uri->query_form;

        # USAGE, get file:
        my $newfile = $iface->process_api_trigger('api_get_file',
            {
                request_params  => {
                    file_uuid           => $file->filestore_id->uuid,
                    transaction_uuid    => $params{transaction_uuid},
                },
                method          => 'post'
            }
        );

        my $upload = Test::MockObject->new();
        $upload->mock('tempname' => sub { return $newfile->filestore_id->get_path });

        is($newfile->version, 1, 'Got version 1 for file');

        # USAGE, post file:
        my $search = $iface->process_api_trigger('api_post_file',
            {
                request_params  => {
                    file_uuid           => $params{file_uuid},
                    transaction_uuid    => $params{transaction_uuid},
                    case_uuid           => $params{case_uuid},
                },
                uploads         => {
                    file    => $upload,
                },
                method          => 'post'
            }
        );

        throws_ok(
            sub {
                $iface->process_api_trigger('api_post_file',
                    {
                        request_params  => {
                            file_uuid           => $params{file_uuid},
                            transaction_uuid    => $params{transaction_uuid},
                            case_uuid           => $params{case_uuid},
                        },
                        uploads         => {
                            file    => $upload,
                            file2   => $upload,
                        },
                        method          => 'post'
                    }
                );
            },
            qr/cannot_update_multiple_files/,
            'No possibility to update multiple files via one call'
        );

        my $secfile = $search->first;
        is($secfile->version, 2, 'Got version 2 for replaced file');

    }, 'Request file, get and post', 1)
}


=head1 IMPLEMENTATION

=head2 xential_api_post_file_exceptions

=cut

sub xential_api_post_file_exceptions : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(sub {
        my $iface = $self->_create_xential_interface;
        my ($trans, $case, $case_document_id) = $self->_create_xential_transaction($iface);

        my $fh      = $self->_get_test_fh;

        throws_ok(
            sub {
                $iface->process_api_trigger(
                    'api_post_file',
                    {
                        user    => undef,
                        uploads => undef,
                    }
                );
            },
            qr/missing:.*method.*request_params/,
            'Validation correct: tested required params "method" and "request_params"'
        );

        throws_ok(
            sub {
                $iface->process_api_trigger(
                    'api_post_file',
                    {
                        user    => undef,
                        uploads => undef,
                        method  => 'post',
                        request_params => {
                        }
                    }
                );
            },
            qr/missing:.*case_uuid.*transaction_uuid/,
            'Validation correct: valid checking of transaction_uuid and case_uuid'
        );

        throws_ok(
            sub {
                $iface->process_api_trigger(
                    'api_post_file',
                    {
                        user    => undef,
                        uploads => undef,
                        method  => 'post',
                        request_params => {
                            transaction_uuid    => 'f696ce30-508c-11e5-b970-0800200c9a65',
                            case_uuid           => 'f696ce30-508c-11e5-b970-0800200c9a65',
                        }
                    }
                );
            },
            qr/No uploads set/,
            'Validation correct: do not continue when no content is set'
        );

        throws_ok(
            sub {
                $iface->process_api_trigger(
                    'api_post_file',
                    {
                        user    => undef,
                        uploads => $fh,
                        method  => 'post',
                        request_params => {
                            transaction_uuid => 'f696ce30-508c-11e5-b970-0800200c9a65',
                            case_uuid        => 'f696ce30-508c-11e5-b970-0800200c9a66',
                        }
                    }
                );
            },
            qr/No transaction found matching the given transaction_uuid/,
            'Authorization correct: do not continue on unknown transaction_id method'
        );

        throws_ok(
            sub {
                $iface->process_api_trigger(
                    'api_post_file',
                    {
                        user    => undef,
                        uploads => $fh,
                        method  => 'post',
                        request_params => {
                            transaction_uuid   => $trans->uuid,
                            case_uuid          => 'f696ce30-508c-11e5-b970-0800200c9a65',
                        }
                    }
                );
            },
            qr/no match with case_uuid/,
            'Authorization correct: do not continue on invalid case_uuid'
        );


    }, 'Tested different exceptions')
}

=head1 INTERNAL METHODS

=head2 _get_test_fh

Generates a simple file to work with, content: "Some binary 001010101 text?"

=cut

sub _get_test_fh {
    my $file            = File::Temp->new(TMPDIR => 1, UNLINK => 1);
    print $file "Some binary 001010101 text?";
    $file->seek( 0, SEEK_END );

    my $mock = Test::MockObject->new();
    $mock->mock('tempname', sub { return $file; });

    return { upload => $mock };
}



=head2 _create_xential_interface

Creates a xential interface with create_interface_ok

=cut

sub _create_xential_interface {
    my $self    = shift;

    my $iface = $zs->create_xential_interface_ok();
    $iface->discard_changes;
    return $iface;
}

=head1 _create_xential_transaction

Creates a xential transaction, by creating a zaaktype, a subject, a case and the final transaction

=cut

sub _create_xential_transaction {
    my $self  = shift;
    my $iface = shift;

    my $casetype        = $zs->create_zaaktype_predefined_ok;
    my $casetypenode    = $casetype->zaaktype_node_id;

    ### First status
    my $status          = $casetypenode->zaaktype_statussen->search({ status => 1})->first;

    my $kenmerk         = $zs->create_zaaktype_kenmerk_ok(
        status              => $status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'xentialdocument',
            label           => 'Please attach a copy of your document',
            magic_string    => 'xential_document',
            value_type      => 'file',
        )
    );

    my $case    = $zs->create_case_ok(zaaktype => $casetype);
    my $subject = $zs->create_subject_ok;

    my $ta = $zs->create_transaction_ok({
        interface_id     => $iface->id,
        processor_params => {
            case             => $case->id,
            subject          => $subject->id,
            document_title   => 'xentialfile.doc',
            case_document_id => $kenmerk->id,
        }
    });

    $ta->discard_changes();
    $case->discard_changes();

    is($ta->processor_params->{document_title}, 'xentialfile.doc', 'Correctly filled xential transaction: filename');
    ok($ta->processor_params->{$_}, "Correctly filled xential transaction: $_") for qw/case subject case_document_id/;

    return ($ta, $case, $kenmerk->id);
}



1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
