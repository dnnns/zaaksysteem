package TestFor::Catalyst::Controller::API::V1::Map;
use base qw(ZSTest::Catalyst);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::Map - Map API tests

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Map.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/map> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 wms_layers

url: /api/v1/map/wms_layers

=cut

sub cat_api_v1_map_wms_layers : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        $zs->create_map_interface_ok();
        my $mech     = $zs->mech;
        $mech->zs_login;

        my $o_data    = $mech->get_json_ok(
            $mech->zs_url_base . '/api/v1/map/ol_settings',
        );

        # diag(explain($o_data));
        is($o_data->{result}->{type}, 'ol_settings', 'Got OpenLayer settings');
        is(@{ $o_data->{result}->{instance}->{wms_layers} }, 2, 'Got 2 rows');

        for (my $i = 0; $i < @{ $o_data->{result}->{instance}->{wms_layers} }; $i++) {
            my $row = $o_data->{result}->{instance}->{wms_layers}->[$i];

            is($row->{type}, 'ol_layer_wms', 'Got correct type "ol_layer_wms" for row: ' . $i);
            like($row->{instance}->{active}, qr/^(?:true|false)$/, 'Got column "active" for row: ' . $i);
            ok($row->{instance}->{$_}, "Got column '$_' for row: $i") for qw/id label url/;
        }
    }, 'api/v1/map/wms_layers: Get the wms layers');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        $mech->zs_login;

        my $o_data    = $mech->get_json(
            $mech->zs_url_base . '/api/v1/map/ol_settings',
        );

        like($o_data->{result}->{instance}->{message}, qr/No interface is set/, 'Found proper exception when no interface is found');

    }, 'api/v1/map/wms_layers: Get the wms layers without interface');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
