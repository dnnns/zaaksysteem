package Zaaksysteem::Test;
use strict;
use warnings;
use namespace::autoclean ();

use Import::Into;
use Test::Class::Moose ();
use Test::Fatal;
use Zaaksysteem::Test::Mocks ();
use Zaaksysteem::Test::Util ();
use Test::Pod::Coverage;

sub import {
    my @imports = qw(
        Test::Class::Moose
        namespace::autoclean
        Test::Fatal

        Zaaksysteem::Test::Mocks
        Zaaksysteem::Test::Util
        Test::Pod::Coverage
    );

    my $caller_level = 1;
    $_->import::into($caller_level) for @imports;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
