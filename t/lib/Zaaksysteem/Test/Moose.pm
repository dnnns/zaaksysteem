package Zaaksysteem::Test::Moose;
use Moose;
extends 'Test::Class::Moose';

sub test_setup {
    my $test = shift;

    if ($ENV{TEST_METHOD}) {
        my $test_method = $test->test_report->current_method->name;
        if ($test_method !~ /$ENV{TEST_METHOD}/) {
            $test->test_skip(
                "Skipping $test_method: Skipped on environment variable");
        }
    }
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Moose - Implement default Zaaksysteem::Test constructs

=head1 DESCRIPTION

Some nice things we can do with Test::Class::Moose;


=head1 SYNOPSIS

    package Zaaksysteem::Test:Foo;
    use Moose;
    extends 'Zaaksysteem::Test::Moose;

    sub test_setup {
        my $test = shift;
        $test->next::method; # optional to call parent test_setup

        # more code here
    }

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
