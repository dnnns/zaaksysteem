package Zaaksysteem::Test::Types;
use Zaaksysteem::Test;

=head1 NAME

Zaaksysteem::Test::Types - Test the Zaaksysteem Type checks

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Zorginstituut

=cut


use Zaaksysteem::Types qw(KvK);

sub test_kvk {
    # The KVK claims their kvk numbers are elfproef, but they aren't
    # https://www.kvk.nl/download/DataserviceInschrijving-FunctioneleServiceBeschrijving-V2.3_tcm109-396683.pdf
    # IPD0004
    # Het opgegeven KvKnummer voldoet niet aan zijn formaat.
    # - 8 posities lang
    # - Voldoet niet aan 11-proef
    #
    # Lies lies lies
    my $kvk = 30259896; # https://www.kvk.nl/orderstraat/product-kiezen/?kvknummer=302598960000
    ok(KvK->check($kvk), "$kvk is a valid KvK");
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
