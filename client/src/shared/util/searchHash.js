export const searchHash = '#search=';

export const searchHashExpression = new RegExp(`^${searchHash}(.*)\\s*$`);
