const numericExpression = /^-?\d+$/;

/**
 * @param {string} value
 * @return {boolean}
 */
export const isValidNumeric = value =>
	((typeof value === 'string')
	&& numericExpression.test(value));
