import { isValidCurrency } from './isValidCurrency';

describe('The `isValidCurrency` function', () => {
	test('expects a string argument', () => {
		[ undefined, null, true, false, 1, {}, [], () => {} ].forEach(value => {
			expect(isValidCurrency(value)).toBe(false);
		});
	});

	test('does not accept a point as decimal mark', () => {
		expect(isValidCurrency('1.1')).toBe(false);
	});

	test('does not accept a trailing comma', () => {
		expect(isValidCurrency('1,')).toBe(false);
	});

	test('accepts integers', () => {
		expect(isValidCurrency('1')).toBe(true);
	});

	test('accepts one or two digits after the decimal mark', () => {
		expect(isValidCurrency('1,1')).toBe(true);
		expect(isValidCurrency('1,11')).toBe(true);
});

	test('does not accept more than two digits after the decimal mark', () => {
		expect(isValidCurrency('1,115')).toBe(false);
	});
});
