import angular from 'angular';
import get from 'lodash/get';
import seamlessImmutable from 'seamless-immutable';
import resourceModule from './../../api/resource';
import composedReducerModule from './../../api/resource/composedReducer';
import vormInvokeModule from './../../vorm/vormInvoke';
import auxiliaryRouteModule from '../../util/route/auxiliaryRoute';
import angularUiRouterModule from 'angular-ui-router';
import zsStorageModule from './../../util/zsStorage';
import actionsModule from './../../../intern/zsIntern/zsActiveSubject/actions';
import zsNotificationListModule from './../zsTopBar/zsNotificationList';
import first from 'lodash/head';
import shortid from 'shortid';
import links from './links';
import template from './template.html';
import snackbarServiceModule from '../../ui/zsSnackbar/snackbarService';
import './styles.scss';

export default
	angular.module('zsSideMenu', [
		resourceModule,
		composedReducerModule,
		vormInvokeModule,
		auxiliaryRouteModule,
		angularUiRouterModule,
		zsStorageModule,
		actionsModule,
		zsNotificationListModule,
		snackbarServiceModule
	])
		.directive('zsSideMenu', [ '$http', '$window', '$document', '$state', 'resource', 'composedReducer', 'vormInvoke', 'auxiliaryRouteService', 'zsStorage', 'snackbarService', ( $http, $window, $document, $state, resource, composedReducer, vormInvoke, auxiliaryRouteService, zsStorage, snackbarService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					onOpen: '&',
					onClose: '&',
					user: '&',
					company: '&',
					instanceId: '&',
					development: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						open = false,
						showNotifications = false,
						notificationPaging = 20,
						kccModuleResource,
						kccActiveResource,
						notificationResource,
						hasUnreadReducer,
						actionReducer,
						linkReducer,
						lastArchivedNotification;

					let closeNotifications = ( ) => {
						showNotifications = false;

						let unreadIds =
							notificationResource.data()
								.filter(notification => !notification.is_read)
								.map(notification => notification.id);

						if (unreadIds.length > 0) {

							$http({
								method: 'POST',
								url: '/api/message/mark_read/',
								data: { 'messages' : unreadIds }
							})
							.then( ( ) => {
								notificationResource.reload();
							});

						}
					};

					kccModuleResource =
						resource(
							'/api/v1/sysin/interface/get_by_module_name/kcc',
							{ scope: $scope }
						)
							.reduce( ( requestOptions, data ) => first(data));

					kccActiveResource = resource(
						( ) => {
							return kccModuleResource.data() ? '/api/kcc/user/status' : null;
						},
						{ scope: $scope }
					)
						.reduce(( requestOptions, data ) => get(first(data), 'user_status', 0) === 1);

					notificationResource =
						resource(( ) => `/api/message/get_for_user?rows=${notificationPaging}`, {
							scope: $scope,
							cache: {
								every: 5 * 60 * 1000,
								poll: true
							}
						})
							.reduce( ( requestOptions, data ) => {
								return data || seamlessImmutable([])
									.map( message => {
										return seamlessImmutable({
											$id: shortid(),
											id: message.id,
											is_read: !!message.is_read,
											message: message.message
										});
									});
							});

					// dummy dependency to ensure a 500 on notifications
					// doesn't break the action list
					hasUnreadReducer = composedReducer( { scope: $scope, mode: 'subscription', waitUntilResolved: false }, notificationResource, ( ) => null)
						.reduce( ( notifications ) => {

							return (notifications || []).filter(notification => !notification.is_read).length > 0;
						});

					actionReducer = composedReducer( { scope: $scope }, kccModuleResource, kccActiveResource, ( ) => showNotifications, hasUnreadReducer)
						.reduce( ( kccModule, isActive, showNotifs, hasUnread ) => {

							let actions = [];

							if (kccModule) {
								actions = actions.concat({
									name: 'kcc',
									type: 'button',
									label: isActive ?
										'Afmelden'
										: 'Aanmelden',
									classes: {
										active: isActive
									},
									icon: isActive ?
										'phone-in-talk'
										: 'phone',
									click: ( ) => {

										kccActiveResource.mutate(
											isActive ?
												'active_subject/kcc/disable'
												: 'active_subject/kcc/enable'
										);

									}
								});
							}

							actions = actions.concat({
								name: 'notifications',
								type: 'button',
								label: 'Notificaties',
								classes: {
									active: showNotifs,
									'has-unread': hasUnread
								},
								icon: 'bell',
								click: ( ) => {

									if (showNotifs) {
										closeNotifications();
									} else {
										showNotifications = true;
									}

								}
							});

							return seamlessImmutable(actions).map(
								action => action.merge({ id: shortid() })
							);
						});

					linkReducer = composedReducer({ scope: $scope }, ctrl.user, ( ) => auxiliaryRouteService.getCurrentBase())
						.reduce( ( user/*, baseState*/ ) => {

							let groups;

							if (!user) {
								return [];
							}

							groups =
								seamlessImmutable(
									links({ user, $state, auxiliaryRouteService })
										.concat(
											{
												name: 'logout',
												children: [
													{
														name: 'logout',
														label: 'Uitloggen',
														type: 'button',
														icon: 'power',
														click: ( ) => {

															zsStorage.clear();
															
															$window.location.href = '/auth/logout';
														}
													}
												]
											}
										)
								)
									.map(group => {
										return group.merge({
											class: `side-menu-group-${group.name}`,
											children: group.children.filter(
												child => child.when !== false
											)
										});
									})
									.filter(group => group.children.length);

								return groups;
						});

					let unArchiveNotification = ( ) => {

						$http({
							method: 'POST',
							url: '/api/message/unarchive',
							data: { 'messages' : lastArchivedNotification }
						})
						.then( ( ) => {
							notificationResource.reload();
						});
					};

					ctrl.archiveNotification = ( id ) => {
						$http({
							method: 'POST',
							url: '/api/message/archive',
							data: { 'messages' : id }
						})
						.then( ( ) => {
							lastArchivedNotification = id;
							notificationResource.reload();
							snackbarService.info('Notificatie gearchiveerd', { actions: [ { type: 'button', label: 'Ongedaan maken', click: unArchiveNotification }] });
						});
					};

					ctrl.archiveAllNotifications = ( ) => {

						$http({
							method: 'POST',
							url: '/api/message/archive/all'
						})
						.then( ( ) => {
							notificationResource.reload();
						});
					};

					ctrl.closeNotifications = ( ) => closeNotifications();

					ctrl.getUserDisplayName = ( ) => get(ctrl.user(), 'display_name');

					ctrl.getCompanyName = ( ) => get(ctrl.company(), 'instance.company');

					ctrl.getLinkGroups = linkReducer.data;

					ctrl.openMenu = ( ) => {
						open = true;
						ctrl.onOpen();
					};

					ctrl.closeMenu = ( ) => {
						open = false;
						closeNotifications();
						ctrl.onClose();
					};

					ctrl.handleCloseClick = ( ) => {
						ctrl.closeMenu();
					};

					ctrl.getUserLink = ( ) => {
						return `/betrokkene/${get(ctrl.user(), 'id')}?gm=1&type=medewerker`;
					};

					ctrl.handleMenuButtonClick = ( ) => {
						if (open) {
							ctrl.closeMenu();
						} else {
							ctrl.openMenu();
						}
					};

					ctrl.isOpen = ( ) => open;

					ctrl.getUserActions = actionReducer.data;

					ctrl.getNotifications = notificationResource.data;

					ctrl.isNotificationListVisible = ( ) => showNotifications;

					ctrl.hasUnreadNotifications = hasUnreadReducer.data;

					ctrl.isNotificationsLoading = ( ) => notificationResource.state() === 'pending';

					ctrl.handleReadMore = ( ) => {
						notificationPaging += 10;
					};

					hasUnreadReducer.subscribe(( ) => {

						if (ctrl.hasUnreadNotifications()) {
							showNotifications = true;
						}

					});

					$document.bind('keyup', ( event ) => {
						if (ctrl.isOpen() && event.keyCode === 27) {
							$scope.$evalAsync(( ) => {
								ctrl.closeMenu();
							});
						}
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
