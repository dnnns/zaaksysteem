import isRequired from './common';
import assign from 'lodash/assign';

function getRemoteLookupArgs(args = {}) {
  const rv = {
    remote : true,
    date : new Date(),
    emptyValue: '',
    nullValue : null,
    undef : undefined,
    foo : 'bar',
  };

  return assign(rv, args);
}

describe('isRequired', () => {

  test('Remote search is unset: nothing is required', () => {
    expect(isRequired(getRemoteLookupArgs({remote : false}), ['baz', 'bar'])).toBe(false);
  });

  test('Foo is set', () => {
    expect(isRequired(getRemoteLookupArgs(), ['foo', 'bar'])).toBe(false);
  });

  test('Date object is set', () => {
    expect(isRequired(getRemoteLookupArgs(), ['date'])).toBe(false);
  });

  test('Empty-like vals', () => {
    expect(isRequired(getRemoteLookupArgs(), ['nullValue', 'undef', 'emptyValue', 'baz'])).toBe(true);
  });


});
