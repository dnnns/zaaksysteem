import angular from 'angular';
import suggestionTemplate from './suggestion-template.html';
import templateLiteral from './template.html';

const domElement = angular.element(templateLiteral)[0];

angular
	.element(domElement.querySelector('.suggestion'))
	.replaceWith(suggestionTemplate);

export default domElement.outerHTML;
