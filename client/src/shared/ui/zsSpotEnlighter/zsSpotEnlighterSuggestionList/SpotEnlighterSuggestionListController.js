import flatten from 'lodash/flatten';
import get from 'lodash/get';
import groupBy from 'lodash/groupBy';
import map from 'lodash/map';
import partition from 'lodash/partition';
import take from 'lodash/take';
import uniq from 'lodash/uniq';

import SuggestionListController from './../../zsSuggestionList/controller';

export default class SpotEnlighterSuggestionListController extends SuggestionListController {

	static get $inject() {
		return ['$scope', '$window', 'composedReducer'];
	}

	constructor( $scope, $window, composedReducer ) {
		super($scope);
		const ctrl = this;
		const MORE_RESULTS_THRESHOLD = 50;
		let loading = false;

		const resultTypesReducer = composedReducer({ scope: $scope }, ctrl.externalSearchSettings(), ctrl.externalSearchResultsCount)
			.reduce(( externalSearchSettings, externalResultsTotalCount ) => [
				{
					name: 'internal',
					label: 'Gevonden in Zaaksysteem'
				},
				{
					name: 'external',
					label: get(externalSearchSettings, 'active') ?
						`Gevonden in externe bronnen (${externalResultsTotalCount})`
						: get(externalSearchSettings, 'errorMessage')
				}
			]);

		// we don't use ctrl.objectType() as a dependency for the reducer
		// because we want the groups to change only when the data does
		const groupReducer = composedReducer({ scope: $scope }, ctrl.suggestions, resultTypesReducer, ctrl.externalSearchResultsCount)
			.reduce(( suggestions, resultTypes, externalResultsTotalCount ) => {
				let grouped = groupBy(suggestions, 'type');

				let groups = uniq(flatten(map(suggestions, 'type')))
					.map(type => {
						let items = grouped[type];
						let truncated = (
							ctrl.objectType() === 'all'
							&& items.length > 3
							&& type !== 'saved_search'
							&& type !== 'external-document'
						);

						if (truncated) {
							items = take(items, 3);
						}

						return {
							type,
							items,
							truncated,
							click() {
								loading = true;

								groupReducer.subscribe(() => {
									loading = false;
								}, {
									once: true
								});

								ctrl.onObjectTypeChange({
									$type: type
								});
							}
						};

					});

				let groupTypes = partition(
					groups,
					group => (group.type !== 'external-document')
				);

				return resultTypes.map(( resultType, index ) => {
					return {
						name: resultType.name,
						label: groupTypes[1].length ?
							resultType.label
							: '',
						groups: groupTypes[index],
						moreResults: !!(
							resultType.name === 'external'
							&& externalResultsTotalCount > MORE_RESULTS_THRESHOLD
						)
					};
				});

			});

		ctrl.getGroups = groupReducer.data;

		ctrl.isLoading = () => loading;

		ctrl.handleActionClick = ( suggestion, action, event ) => {
			action.click(event);
			event.stopPropagation();
		};

		ctrl.handleDossierClick = ( query, event ) => {
			ctrl.onChangeQuery({ $query: query });
			event.preventDefault();
			event.stopPropagation();
		};

		ctrl.openInNewWindow = ( link, event ) => {
			$window.open(link, '_new');
			event.preventDefault();
			event.stopPropagation();
		};

	}

}
