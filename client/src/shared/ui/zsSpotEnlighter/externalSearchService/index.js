import angular from 'angular';
import first from 'lodash/head';
import get from 'lodash/get';
import resourceModule from '../../../api/resource/index.js';
import getRequestOptionsFactory from './getRequestOptionsFactory';

export default angular
	.module('shared.ui.zsSpotEnlighter.externalSearchService', [
		resourceModule
	])
	.factory('externalSearchService', [
		'$rootScope', '$timeout', 'resource',
		( $rootScope, $timeout, resource ) => {
			const tokenRefresh = 3600 * 1000;

			const apiResource = resource(
				{
					url: '/api/v1/sysin/interface/get_by_module_name/next2know'
				},
				{
					scope: $rootScope,
					cache: {
						every: tokenRefresh
					}
				}
			)
				.reduce(( requestOptions, data ) =>
					get(first(data), 'instance.interface_config')
				);

			apiResource.subscribe(( data ) => {
				$timeout(() => {
					apiResource.reload();
				}, get(data, 'token.expires_in') * 1000);
			});

			const getRequestParameters = getRequestOptionsFactory(apiResource.data);

			const isDataResolved = () => (
				apiResource.state() === 'resolved'
				&& apiResource.data() !== undefined
			);

			// The ZS Backend will return a token for Next2Know even if the
			// currently logged in ZS user is not known in Next2Know.
			// As a result this function will return true even if the currently
			// logged in ZS user can't actually search Next2Know.
			// We deal with this in zsSpotEnlighter/index.js in the error
			// handling of the externalSearchResource.
			// More information: ZS-14874.
			const conditionsMet = () => (
				isDataResolved()
				&& apiResource.data().token !== null
			);

			function delegate( getValue, ...conditions ) {
				if (conditions.every(value => value)) {
					return getValue();
				}

				return null;
			}

			return {
				/**
				 * @returns {Object|null}
				 */
				getApiInfo() {
					return delegate(
						() => apiResource.data(),
						isDataResolved()
					);
				},

				/**
				 * Search by keyword
				 * ZS-TODO:
				 * - why is the first argument optional, and can we safely assign a default value?
				 * @param {string} [searchIndex = '_all']
				 * @param {string} q search query
				 * @returns {*}
				 */
				getRequestOptions( q, searchIndex = '_all' ) {
					return delegate(
						() => getRequestParameters(`/documents/${searchIndex}/search`, {
							params: {
								q,
								size: 50
							}
						}),
						q,
						conditionsMet()
					);
				},

				/**
				 * Get single search result
				 * @param searchIndex
				 * @param documentId
				 * @returns {*}
				 */
				getDocumentRequestOptions( searchIndex, documentId ) {
					return delegate(
						() => getRequestParameters(`/documents/${searchIndex}/${documentId}?format=array`),
						searchIndex,
						documentId,
						conditionsMet()
					);
				},

				/**
				 * Download a file from next2know
				 * @param filename
				 * @returns {*}
				 */
				getDownloadRequestOptions( filename ) {
					return delegate(
						() => getRequestParameters(`/files/${filename}`, {
							responseType: 'arraybuffer',
						}),
						filename,
						conditionsMet()
					);
				},

				/**
				 * Get a thumbnail from next2know
				 * @param searchIndex
				 * @param documentId
				 * @returns {*}
				 */
				getDocumentThumbRequestOptions( searchIndex, documentId ) {
					return delegate(
						() => getRequestParameters(`/thumbnails/${searchIndex}/${documentId}`),
						searchIndex,
						documentId,
						conditionsMet()
					);
				}
			};
		}
	])
	.name;
