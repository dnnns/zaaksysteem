import {
    login,
    logout
} from './../../../functions/intern/loginPage';

describe('when logged in', () => {

    describe('and user logs out', () => {

        beforeEach(() => {
            login();
            logout();
        });

        it('should redirect to the login page', () => {
            expect(browser.getCurrentUrl()).toMatch(/auth\/page/);
        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
