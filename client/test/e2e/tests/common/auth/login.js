import {
    login,
    logout,
    loginForm,
    usernameField,
    passwordField,
    submit
} from './../../../functions/intern/loginPage';

describe('when logging in', () => {

    beforeAll(() => {

        logout();

    });

    it('should have a form', () => {

        expect(loginForm.isPresent()).toBe(true);

    });

    it('should have a username input field', () => {

        expect(usernameField.isPresent()).toBe(true);

    });

    it('should have a password input field', () => {

        expect(passwordField.isPresent()).toBe(true);

    });

    it('should have a submit button', () => {

        expect(submit.isPresent()).toBe(true);

    });

    describe('when logging in with valid credentials', () => {

        beforeAll(() => {

            login();

        });

        it('should redirect to the application', () => {

            expect(browser.getCurrentUrl()).toMatch(/intern/);

        });

    });

    describe('when logging in with invalid credentials', () => {

        beforeAll(() => {

            logout();

            login('admin', 'wrong password');

        });

        it('should stay on the login page', () => {

            expect(browser.getCurrentUrl()).not.toMatch(/intern/);

        });

        it('should give a warning for the wrong login details', () => {

            expect($('.id_wrong').isPresent()).toBe(true);

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
