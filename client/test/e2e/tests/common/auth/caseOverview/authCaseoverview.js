import navigate from './../../../../functions/common/navigate';
import {
    search,
    exitUniversalSearch
} from './../../../../functions/intern/universalSearch';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';

describe('when logging in as user with authorisation to search case 25', () => {

    beforeAll(() => {

        navigate.as('metzaakoverzichtrechten');

    });

    describe('and arriving on the dashboard', () => {

        it('should have case 25 present', () => {

            expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

        });

    });

    describe('and searching for case 25 in general search', () => {

        beforeAll(() => {

            search('zaakoverzichtrechten');

        });

        it('should have case 25 present', () => {

            expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

        });

        afterAll(() => {

            exitUniversalSearch();

        });

    });

    describe('and opening advanced search', () => {

        beforeAll(() => {

            browser.get('/search/all');

        });

        it('should have case 25 present', () => {

            expect($('[href="/intern/zaak/25"]').isPresent()).toBe(true);

        });

    });

    describe('and opening contact overview', () => {

        beforeAll(() => {

            browser.ignoreSynchronization = true;

            browser.get(getContactUrl('citizen', '3'));

        });

        it('should have case 25 present', () => {

            expect($('#zaak25').isPresent()).toBe(true);

        });

        afterAll(() => {

            browser.ignoreSynchronization = false;

            navigate.to();

        });

    });

});

describe('when logging in as user without authorisation to search case 25', () => {

    beforeAll(() => {

        navigate.as('zonderzaakoverzichtrechten');

    });

    describe('and arriving on the dashboard', () => {

        it('should not have case 25 present', () => {

            expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

        });

    });

    describe('and searching for case 25 in general search', () => {

        beforeAll(() => {

            search('zaakoverzichtrechten');

        });

        it('should not have case 25 present', () => {

            expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

        });

        afterAll(() => {

            exitUniversalSearch();

        });

    });

    describe('and opening advanced search', () => {

        beforeAll(() => {

            browser.get('/search/all');

        });

        it('should not have case 25 present', () => {

            expect($('[href="/intern/zaak/25"]').isPresent()).toBe(false);

        });

    });

    describe('and opening contact overview', () => {

        beforeAll(() => {

            browser.ignoreSynchronization = true;

            browser.get(getContactUrl('citizen', '3'));

        });

        it('should not have case 25 present', () => {

            expect($('#zaak25').isPresent()).toBe(false);

        });

        afterAll(() => {

            browser.ignoreSynchronization = false;

            navigate.to();

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
