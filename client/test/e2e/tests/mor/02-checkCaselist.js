import {
    header,
    getHeaderText,
    tabs,
    supportlink,
    countCaselists,
    listCases,
    loadMoreCases,
    openSearch,
    enterSearchTerm,
    closeSearch,
    search,
    getSearchResults,
    selectSearchTerm
} from './../../functions/mor/mor';
import navigate from './../../functions/common/navigate';

describe('when opening the mor app', () => {

    beforeAll(() => {
        navigate.as('mor', '/mor/');
    });

    it('there should be a header', () => {
        expect(header.isPresent()).toBe(true);
    });

    it('the header should contain the configured background image', () => {
        expect(header.$('.mor-nav__bg').getAttribute('style')).toContain('auroraobjects');
    });

    it('the header should contain the configured title', () => {
        expect(getHeaderText()).toEqual('Melding openbare ruimte');
    });

    it('the header should contain two navigation buttons', () => {
        expect(tabs.all(by.css('a')).count()).toEqual(2);
    });

    it('the footer should contain the configured link', () => {
        expect(supportlink.getAttribute('href')).toEqual('https://www.nissan.nl/');
    });

    describe('and when on the open view', () => {
    
        beforeAll(() => {
            navigate.as('mor', '/mor/zaken/open');
        });
    
        it('the view should contain two caseLists', () => {
            expect(countCaselists()).toEqual(2);
        });

        it('the first caseList should contain the right cases', () => {
            const expected = [136, 137, 138, 139];

            expect(listCases(1)).toEqual(expected);
        });

        it('the second caseList should contain the right cases', () => {
            const expected = [132, 133, 134];

            expect(listCases(2)).toEqual(expected);
        });
    
    });

    describe('and when on the closed view', () => {
    
        beforeAll(() => {
            navigate.as('mor', '/mor/zaken/afgehandeld');
        });
    
        it('the view should contain one caseList', () => {
            expect(countCaselists()).toEqual(1);
        });

        it('the caseList should contain the right cases', () => {
            const expected = [150, 149, 148, 147, 146, 145, 144, 143, 142, 141];

            expect(listCases(1)).toEqual(expected);
        });

        it('the view should contain an option to retrieve more cases', () => {
            expect($('.case-group-loadmore button').isPresent()).toBe(true);
        });

        describe('and when loading more cases', () => {

            beforeAll(() => {
                loadMoreCases();
            });

            it('the caseList should contain the right cases', () => {
                expect(listCases(1)).toContain(140);
            });

        });
    
    });

    describe('and when entering a searchTerm', () => {
    
        beforeAll(() => {
            navigate.as('mor', '/mor/zaken/open');
            openSearch();
            enterSearchTerm('aap');
        });
    
        it('there should be search results matching the case results', () => {
            const expected = ['aap', 'aap noot', 'aap mies', 'aap noot mies'];

            expect(getSearchResults()).toEqual(expected);
        });

        afterAll(() => {
            selectSearchTerm();
            closeSearch();
        });
    
    });

    describe('and when searching with one term', () => {
    
        beforeAll(() => {
            navigate.as('mor', '/mor/zaken/open');
            search('aap');
        });

        it('the first caseList should contain the right cases', () => {
            const expected = [136, 137, 139];

            expect(listCases(1)).toEqual(expected);
        });

        it('the second caseList should contain the right cases', () => {
            const expected = [132, 133];

            expect(listCases(2)).toEqual(expected);
        });

        afterAll(() => {
            closeSearch();
        });
    
    });

    describe('and when searching with multiple terms', () => {
    
        beforeAll(() => {
            navigate.as('mor', '/mor/zaken/open');
            search(['aap', 'mies']);
        });

        it('the first caseList should contain the right cases', () => {
            const expected = [137, 139];

            expect(listCases(1)).toEqual(expected);
        });

        it('the second caseList should contain the right cases', () => {
            const expected = [133];

            expect(listCases(2)).toEqual(expected);
        });

        afterAll(() => {
            closeSearch();
        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
