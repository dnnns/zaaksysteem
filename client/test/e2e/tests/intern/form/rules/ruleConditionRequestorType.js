import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import caseAttribute from './../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const requestorType00 = $('[data-name="aanvrager_type_00"]');
const requestorType01 = $('[data-name="aanvrager_type_01"]');
const requestorType10 = $('[data-name="aanvrager_type_10"]');
const requestorType11 = $('[data-name="aanvrager_type_11"]');

describe('when opening a registration form with the citizen as requestor', () => {

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Systeemvoorwaarde aanvrager type',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorType00)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType01)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorType10)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType11)).toEqual('True');

    });

});

describe('when opening a registration form with the company as requestor', () => {

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Systeemvoorwaarde aanvrager type',
            requestorType: 'organisation',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorType00)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType01)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType10)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorType11)).toEqual('True');

    });

});

describe('when opening a registration form with the employee as requestor', () => {

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Systeemvoorwaarde aanvrager type',
            requestorType: 'employee',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Ja"]').click();

    });

    it('the attributes should have the correct values', () => {

        expect(caseAttribute.getClosedValue(requestorType00)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType01)).toEqual('True');
        expect(caseAttribute.getClosedValue(requestorType10)).toEqual('False');
        expect(caseAttribute.getClosedValue(requestorType11)).toEqual('True');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
