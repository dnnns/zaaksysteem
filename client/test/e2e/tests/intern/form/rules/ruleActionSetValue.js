import navigate from './../../../../functions/common/navigate';
import {
    goNext
} from './../../../../functions/common/form';
import startForm from './../../../../functions/common/startForm';
import caseAttribute from './../../../../functions/common/input/caseAttribute';

const setValue1choice = $('[data-name="vul_waarde_in_1_keuze"]');
const setValue2choice = $('[data-name="vul_waarde_in_2_keuze"]');
const setValue3text = $('[data-name="vul_waarde_in_3_tekst"]');
const setValue4choice = $('[data-name="vul_waarde_in_4_keuze"]');
const setValue5text = $('[data-name="vul_waarde_in_5_tekst"]');
const setFormula1choice = $('[data-name="vul_formule_in_1_keuze"]');
const setFormula2text = $('[data-name="vul_formule_in_2_tekst"]');
const setFormula3valuta = $('[data-name="vul_formule_in_3_valuta"]');
const setFormula4text = $('[data-name="vul_formule_in_4_tekst"]');
const setFormula5valuta = $('[data-name="vul_formule_in_5_valuta"]');
const setFormula6text = $('[data-name="vul_formule_in_6_tekst"]');
const setFormula7valuta = $('[data-name="vul_formule_in_7_valuta"]');
const setFormula8choice = $('[data-name="vul_formule_in_8_keuze"]');
const setFormula9text = $('[data-name="vul_formule_in_9_tekst"]');

describe('when opening a registration form with set value scenarios', () => {

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Vul waarde in',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

    });

    describe('and activating the set values', () => {

        beforeAll(() => {

            setValue1choice.$('[value="Ja"]').click();

        });

        it('the value should be what was set and read-only', () => {

            expect(caseAttribute.getClosedValue(setValue2choice)).toEqual('Ja');

        });

        it('the followup value should be what was set and read-only', () => {

            expect(caseAttribute.getClosedValue(setValue3text)).toEqual('Tekst');

        });

        it('the value should be what was set and not read-only', () => {

            expect(caseAttribute.getValue(setValue4choice)).toBe('Ja');

        });

        it('the followup value should be what was set and not read-only', () => {

            expect(caseAttribute.getValue(setValue5text)).toEqual('Tekst');

        });

        describe('and when deactivating the set values', () => {

            beforeAll(() => {

                setValue5text.$('input').sendKeys(' en nog meer tekst');

                setValue4choice.$('[value="Nee"]').click();

                setValue1choice.$('[value="Nee"]').click();

            });

            it('the followup value should be what was set and not read-only', () => {

                expect(caseAttribute.getValue(setValue2choice)).toBe('Ja');

            });

            it('the followup value should be what was set and not read-only', () => {

                expect(caseAttribute.getValue(setValue4choice)).toBe('Nee');

            });

            it('the followup value should be what was set and not read-only', () => {

                expect(caseAttribute.getValue(setValue5text)).toEqual('Tekst en nog meer tekst');

            });

        });

    });

    describe('and activating the formula rules', () => {

        beforeAll(() => {

            goNext();

            setFormula2text.$('input').sendKeys('1.2345');

            setFormula3valuta.$('input').sendKeys('1,23');

            setFormula1choice.$('[value="Ja"]').click();

        });

        it('the result should be 0 and read-only', () => {

            expect(caseAttribute.getClosedValue(setFormula4text)).toEqual('0');

        });

        it('the result should be 0,00 and read-only', () => {

            expect(caseAttribute.getClosedValue(setFormula5valuta)).toEqual('€ 0,00');

        });

        xit('the result should be 1.518435 and read-only', () => {

            expect(caseAttribute.getClosedValue(setFormula6text)).toEqual('1.518435');

        });

        it('the result should be 1.52 and read-only', () => {

            expect(caseAttribute.getClosedValue(setFormula7valuta)).toEqual('€ 1,52');

        });

        it('the result should be 2.467 and read-only', () => {

            expect(caseAttribute.getClosedValue(setFormula8choice)).toEqual('2.46');

        });

        xit('the followup field should become displayed', () => {

            expect(setFormula9text.isPresent()).toBe(true);

        });

        describe('and when deactivating the formula rules', () => {

            beforeAll(() => {

                setFormula3valuta.$('input').clear().sendKeys('2,46');

            });

            xit('the result should be 4.92 and read-only', () => {

                expect(caseAttribute.getClosedValue(setFormula6text)).toEqual('3.03687');

            });

            it('the result should be 4.92 and read-only', () => {

                expect(caseAttribute.getClosedValue(setFormula7valuta)).toEqual('€ 3,04');

            });

            it('the result should be 4.92 and read-only', () => {

                expect(caseAttribute.getClosedValue(setFormula8choice)).toEqual('4.92');

            });

            it('the followup field should become hidden', () => {

                expect(setFormula9text.isPresent()).toBe(false);

            });

        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
