import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext
} from './../../../../functions/common/form';
import {
    getAboutValue
} from './../../../../functions/intern/caseView/caseMenu';
import getFutureDate from './../../../../functions/common/getFutureDate';

const choice = $('[data-name="boolean"]');

describe('when opening a registration form with change term testscenarios', () => {

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Afhandeltermijn wijzigen',
            requestorType: 'citizen',
            requestorId: '1',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        choice.$('[value="Ja"]').click();

        goNext(2);

    });

    xit('the target date should have been changed', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual(getFutureDate(5));

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
