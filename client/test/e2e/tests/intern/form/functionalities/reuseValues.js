import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    reuseValues
} from './../../../../functions/common/form';
import caseAttribute from './../../../../functions/common/input/caseAttribute';

describe('when starting a registration form with the add reuse values functionality and reusing', () => {

    const attr1 = $('[data-name="tekstveld"]');
    const attr2 = $('[data-name="tekstveld_plus"]');
    const attr3 = $('[data-name="groot_tekstveld"]');
    const attr4 = $('[data-name="enkelvoudige_keuze"]');
    const attr5 = $('[data-name="meervoudige_keuze"]');
    const attr6 = $('[data-name="datum"]');
    const attr7 = $('[data-name="adres_dmv_postcode"]');

    beforeAll(() => {

        navigate.as();

        const data = {
            casetype: 'Gegevens hergebruiken',
            requestorType: 'citizen',
            requestorId: '18',
            channelOfContact: 'behandelaar'
        };

        startForm(data);

        caseAttribute.inputAttribute(attr1);

        reuseValues();

    });

    it('the form should contain the values from the previous case 162 and not from deleted case 163', () => {

        expect(caseAttribute.getValue(attr1)).toEqual('Tweede zaak');
        expect(caseAttribute.getValue(attr2)).toEqual([ 'Tweede', 'zaak' ]);
        expect(caseAttribute.getValue(attr3)).toEqual('Tweede\nzaak');
        expect(caseAttribute.getValue(attr4)).toEqual('Optie 2');
        expect(caseAttribute.getValue(attr5)).toEqual([ false, true, true, false, true ]);
        expect(caseAttribute.getValue(attr6)).toEqual('2017-02-02');
        expect(caseAttribute.getValue(attr7)).toEqual('Voerendaal - Florinstraat 2');

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
