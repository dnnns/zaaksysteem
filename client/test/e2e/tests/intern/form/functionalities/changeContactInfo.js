import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    getRequestorEmailAddress,
    getRequestorPhoneNumber,
    getRequestorMobileNumber,
    changeRequestorEmailAddress,
    changeRequestorPhoneNumber,
    changeRequestorMobileNumber
} from './../../../../functions/common/form';
import waitForElement from './../../../../functions/common/waitForElement';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';
import {
    openContactInfo,
    getEmailAddress,
    getPhoneNumber,
    getMobileNumber
} from './../../../../functions/intern/contactView';

const testData = [
    {
        type: 'add',
        requestorType: 'citizen',
        requestorId: '15',
        oldEmail: '',
        oldPhoneNumber: '',
        oldMobileNumber: '',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'chang',
        requestorType: 'citizen',
        requestorId: '16',
        oldEmail: 'unchanged@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'delet',
        requestorType: 'citizen',
        requestorId: '17',
        oldEmail: 'unremoved@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: '',
        newPhoneNumber: '',
        newMobileNumber: ''
    },
    {
        type: 'add',
        requestorType: 'organisation',
        requestorId: '8',
        oldEmail: '',
        oldPhoneNumber: '',
        oldMobileNumber: '',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'chang',
        requestorType: 'organisation',
        requestorId: '9',
        oldEmail: 'unchanged@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'delet',
        requestorType: 'organisation',
        requestorId: '10',
        oldEmail: 'unremoved@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: '',
        newPhoneNumber: '',
        newMobileNumber: ''
    }
];

for ( const index in testData ) {

    describe(`when starting a registration form for ${testData[index].type}ing the contact info of a ${testData[index].requestorType}`, () => {

        beforeAll(() => {

            navigate.as();

            const data = {
                casetype: 'Contactgegevens aanpassen bij registratie',
                requestorType: testData[index].requestorType,
                requestorId: testData[index].requestorId,
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            goNext();

        });

        it('the email address should be correct', () => {

            expect(getRequestorEmailAddress()).toEqual(testData[index].oldEmail);

        });

        it('the phone number should be correct', () => {

            expect(getRequestorPhoneNumber()).toEqual(testData[index].oldPhoneNumber);

        });

        it('the mobile number should be correct', () => {

            expect(getRequestorMobileNumber()).toEqual(testData[index].oldMobileNumber);

        });

        describe(`and when ${testData[index].type}ing the info and registering the case`, () => {
    
            beforeAll(() => {
        
                changeRequestorEmailAddress(testData[index].newEmail);
                changeRequestorPhoneNumber(testData[index].newPhoneNumber);
                changeRequestorMobileNumber(testData[index].newMobileNumber);

                goNext();

                waitForElement('.case-view');

                browser.sleep(5000);

                browser.ignoreSynchronization = true;

                browser.get(getContactUrl(testData[index].requestorType, testData[index].requestorId));

                waitForElement('[id="ui-id-12"]');

                openContactInfo();

                waitForElement('[name="npc-email"]');

            });
        
            it(`the email address should have been ${testData[index].type}ed`, () => {

                expect(getEmailAddress()).toEqual(testData[index].newEmail);

            });

            it(`the phone number should have been ${testData[index].type}ed`, () => {

                expect(getPhoneNumber()).toEqual(testData[index].newPhoneNumber);

            });

            it(`the mobile number should have been ${testData[index].type}ed`, () => {

                expect(getMobileNumber()).toEqual(testData[index].newMobileNumber);

            });

            afterAll(() => {

                browser.get('/intern/');

                browser.ignoreSynchronization = false;

            });
        
        });

    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
