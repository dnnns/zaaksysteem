import navigate from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    assignSelf,
    assignCoworker,
    assignDepartment,
    getAssignmentType,
    getAssignmentDepartment,
    getAssignmentRole
} from './../../../../functions/common/form';

const testData = [
    {
        type: 'both options disabled',
        casetype: 'Toewijzing bij registratie geen opties',
        assignSelf: false,
        assignCoworker: false,
        assignDepartment: false
    },
    {
        type: 'assign to self option enabled',
        casetype: 'Toewijzing bij registratie mijzelf',
        assignSelf: true,
        assignCoworker: false,
        assignDepartment: false,
        assignmentType: 'me'
    },
    {
        type: 'assignment option enabled',
        casetype: 'Toewijzing bij registratie andere',
        assignSelf: true,
        assignCoworker: true,
        assignDepartment: true,
        assignmentType: 'org-unit',
        assignmentDepartment: '-Backoffice',
        assignmentRole: 'Behandelaar'
    },
    {
        type: 'assign to self and assignment options enabled',
        casetype: 'Toewijzing bij registratie alle opties',
        assignSelf: true,
        assignCoworker: true,
        assignDepartment: true,
        assignmentType: 'me'
    }
];

for ( const index in testData ) {

    describe(`when starting a registration form with ${testData[index].type}`, () => {

        beforeAll(() => {

            navigate.as();

            const data = {
                casetype: testData[index].casetype,
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            goNext();

        });

        it('the correct assignment options should be present', () => {

            expect(assignSelf.isPresent()).toBe(testData[index].assignSelf);
            expect(assignCoworker.isPresent()).toBe(testData[index].assignCoworker);
            expect(assignDepartment.isPresent()).toBe(testData[index].assignDepartment);

        });

        if ( testData[index].assignmentType ) {

            it(`the assignment type should be "${testData[index].assignementType}"`, () => {

                expect(getAssignmentType()).toEqual(testData[index].assignmentType);

            });

        }

        if ( testData[index].assignmentType === 'org-unit' ) {

            it(`the department should be "${testData[index].assignDepartment}"`, () => {

                expect(getAssignmentDepartment()).toEqual(testData[index].assignmentDepartment);

            });

            it(`the assignment should be "${testData[index].assignDepartment}"`, () => {

                expect(getAssignmentRole()).toEqual(testData[index].assignmentRole);

            });

        }

    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
