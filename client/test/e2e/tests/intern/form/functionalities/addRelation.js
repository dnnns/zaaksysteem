import navigate from './../../../../functions/common/navigate';
import waitForElement from './../../../../functions/common/waitForElement';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    addSubject
} from './../../../../functions/common/form';
import {
    openTab
} from './../../../../functions/intern/caseView/caseNav';
import {
    getContactRole,
    getContactAuthorisation
} from './../../../../functions/intern/caseView/caseRelations';

const testData = [
    {
        type: 'citizen normal',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '022002202',
        role: 'Advocaat',
        authorisation: false
    },
    {
        type: 'organisation normal',
        name: 'Zaaktypeactie betrokkene toevoegen',
        subjectType: 'organisation',
        subjectToRelate: '02200002',
        role: 'Aannemer',
        authorisation: false
    },
    {
        type: 'citizen authorized',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '022002202',
        role: 'Advocaat',
        authorisation: true
    },
    // Bug: ZS-15729
    // {
    //     type: 'citizen notified',
    //     name: 'Z. Betrokkene toevoegen',
    //     subjectType: 'citizen',
    //     subjectToRelate: '022002202',
    //     role: 'Advocaat',
    //     authorisation: true,
    //     notification: true
    // },
    {
        type: 'citizen special role',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '022002202',
        role: 'Slager',
        authorisation: false
    // Bug: ZS-15659
    // },
    // {
    //     type: 'citizen manual role',
    //     name: 'Z. Betrokkene toevoegen',
    //     subjectType: 'citizen',
    //     subjectToRelate: '022002202',
    //     role: 'anders',
    //     roleOther: 'manual',
    //     authorisation: false
    }
];

for ( const index in testData ) {

    describe(`when starting a registration form and adding a ${testData[index].type} relation`, () => {

        beforeAll(() => {

            navigate.as();

            const data = {
                casetype: 'Zaaktypeactie betrokkene toevoegen',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };

            startForm(data);

            goNext();

            addSubject(testData[index]);

            goNext();

            openTab('relations');

        });

        it('the case should have the subject as a relation', () => {

            expect($('.related_subjects').getText()).toContain(testData[index].name);
            
        });

        it('the subject should have the selected role', () => {

            expect(getContactRole(testData[index].name)).toEqual(testData[index].role);
            
        });

        it(`the subject authorisation should be set to ${testData[index].authorisation}`, () => {

            expect(getContactAuthorisation(testData[index].name)).toBe(testData[index].authorisation);
            
        });

        if ( testData[index].notification ) {

            it('the subject should have been notified', () => {

                browser.ignoreSynchronization = true;

                openTab('timeline');

                waitForElement('[data-event-type="email/send"]');

                expect($('[data-event-type="email/send"]').isPresent()).toBe(testData[index].notification);

                browser.get('/intern/');

                browser.ignoreSynchronization = false;
                
            });

        }
        
    });

}

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
