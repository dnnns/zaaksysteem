import navigate from './../../../functions/common/navigate';
import waitForElement from './../../../functions/common/waitForElement';
import {
    assignTo,
    stall,
    resume,
    changeTerm,
    relateCase,
    duplicate,
    relateObject,
    changeRequestor,
    changeCoordinator,
    changeDepartmentRole,
    changeRegistrationDate,
    changeTargetDate,
    closeEarly,
    changeStatus,
    changeDestructionDate,
    changeAttributes,
    changePhase,
    toggle,
    selectAction,
    changeAuth,
    changeCasetype
} from './../../../functions/intern/actionMenu';
import {
    getAboutValue,
    getSummaryValue
} from './../../../functions/intern/caseView/caseMenu';
import {
    openTab
} from './../../../functions/intern/caseView/caseNav';
import getFutureDate from './../../../functions/common/getFutureDate';
import waitForSave from './../../../functions/intern/caseView/waitForSave';

describe('when assigning the case to a group', () => {

    beforeAll(() => {

        navigate.as('admin', 75);

        let data = {
            department: '-Zaakacties',
            role: 'Behandelaar'
        };

        assignTo('group', data);

    });

    it('the user should be redirected to the dashboard', () => {

        expect($('.gridster-desktop').isPresent()).toBe(true);

    });

    describe('and when opening the case again', () => {

        beforeAll(() => {

            navigate.to(75);

        });

        it('the department of the case should have changed to that of the group', () => {

            expect(getAboutValue('Afdeling')).toEqual('Zaakacties');

        });

    });

});

describe('when assigning the case to a person', () => {

    beforeAll(() => {

        navigate.as('admin', 76);

        let data = {
            name: 'Toewijzing wijzigen',
            sendEmail: false,
            changeDepartment: true
        };

        assignTo('person', data);

    });

    it('the user should be redirected to the dashboard', () => {

        expect($('.gridster-desktop').isPresent()).toBe(true);

    });

    describe('and when opening the case again', () => {

        beforeAll(() => {

            navigate.to(76);

        });

        it('the assignee of the case should have changed to that person', () => {

            expect(getAboutValue('Behandelaar')).toEqual('Toewijzing wijzigen');

        });

        it('the department of the case should have changed to that of the person', () => {

            expect(getAboutValue('Afdeling')).toEqual('Zaakacties');

        });

    });

});

describe('when assigning the case to a person without authentication', () => {

    beforeAll(() => {

        navigate.as('admin', 130);

        let data = {
            name: 'Toewijzing wijzigen',
            sendEmail: false
        };

        assignTo('person', data);

    });

    it('the user should be redirected to the dashboard', () => {

        expect($('.gridster-desktop').isPresent()).toBe(true);

    });

    describe('and when opening the case again', () => {

        beforeAll(() => {

            navigate.to(130);

        });

        it('the assignee of the case should have changed to that person', () => {

            expect(getAboutValue('Behandelaar')).toEqual('Toewijzing wijzigen');

        });

    });

});

describe('when assigning the case to a person with authentication', () => {

    beforeAll(() => {

        navigate.as('admin', 131);

        let data = {
            name: 'Toewijzing wijzigen',
            sendEmail: false
        };

        assignTo('person', data);

    });

    xit('the user should not be redirected to the dashboard', () => {

        expect($('.case-view').isPresent()).toBe(true);

    });

    describe('and when opening the case again', () => {

        beforeAll(() => {

            navigate.to(131);

        });

        it('the assignee of the case should not have changed to that person', () => {

            expect(getAboutValue('Behandelaar')).toEqual('admin');

        });

    });

});

describe('when assigning the case to myself', () => {

    beforeAll(() => {

        navigate.as('admin', 77);

        assignTo('self');

    });

    it('the user should not be redirected to the dashboard', () => {

        expect($('.case-view').isPresent()).toBe(true);

    });

    describe('and when opening the case again', () => {

        beforeAll(() => {

            navigate.to(77);

        });

        it('the department of the case should not have changed to my department', () => {

            expect(getAboutValue('Afdeling')).toEqual('Backoffice');

        });

    });

});

describe('when stalling the case for an undetermined period of time', () => {

    beforeAll(() => {

        navigate.as('admin', 78);

        stall('indeterminate');

    });

    it('the case should have a stalled notifcation', () => {

        expect($('[data-name="stalled"]').isPresent()).toBe(true);

    });

    it('the stalled notifcation should include that it was indeterminate', () => {

        expect($('[data-name="stalled"]').getText()).toEqual('De zaak is opgeschort voor onbepaalde tijd.');

    });

    it('the target date should show to be stalled in the summary', () => {

        expect(getSummaryValue('Streefafhandeldatum')).toEqual('Opgeschort');

    });

    it('the target date should show to be stalled in the more information', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual('Opgeschort');

    });

});

describe('when stalling the case for a determined period of time', () => {

    beforeAll(() => {

        navigate.as('admin', 79);

        stall('determinate', 'Kalenderdagen', '2');

    });

    it('the case should have a stalled notifcation', () => {

        expect($('[data-name="stalled"]').isPresent()).toBe(true);

    });

    it('the stalled notifcation should include the date till when it was stalled', () => {

        let expectedDateText = getFutureDate(2);

        expect($('[data-name="stalled"]').getText()).toEqual(`De zaak is opgeschort tot ${expectedDateText}.`);

    });

    it('the target date should show to be stalled in the summary', () => {

        expect(getSummaryValue('Streefafhandeldatum')).toEqual('Opgeschort');

    });

    it('the target date should show to be stalled in the more information', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual('Opgeschort');

    });

});

describe('when resuming the case with the default inputs', () => {

    beforeAll(() => {

        navigate.as('admin', 80);

        resume();

    });

    it('the case should not have a stalled notifcation', () => {

        expect($('[data-name="stalled"]').isPresent()).toBe(false);

    });

    it('the target date should show to be stalled in the summary', () => {

        expect(getSummaryValue('Streefafhandeldatum')).toEqual(getFutureDate(1));

    });

    it('the target date should show to be stalled in the more information', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual(getFutureDate(1));

    });

});

describe('when resuming the case with changed inputs', () => {

    beforeAll(() => {

        navigate.as('admin', 81);

        let data = {
            start: '13-01-2010',
            end: '15-01-2010'
        };

        resume(data);

    });

    it('the case should not have a stalled notifcation', () => {

        expect($('[data-name="stalled"]').isPresent()).toBe(false);

    });

    it('the target date should show to be stalled in the summary', () => {

        expect(getSummaryValue('Streefafhandeldatum')).toEqual('28-04-2017');

    });

    it('the target date should have been updated', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual('28-04-2017');

    });

});

describe('when closing the case early', () => {

    beforeAll(() => {

        navigate.as('admin', 82);

        closeEarly('aangehouden');

    });

    it('the user should be redirected to the dashboard', () => {

        expect($('.case-view').isPresent()).toBe(true);

    });

    it('the case should have a resolved notification', () => {

        expect($('.case-message-list [data-name="resolved"]').isPresent()).toBe(true);

    });

});

describe('when setting a new target date for the case', () => {

    beforeAll(() => {

        let data = {
            date: '13-01-2010'
        };

        navigate.as('admin', 83);

        changeTerm('fixed', data);

    });

    it('the target date of the case should have changed to the new target date', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual('13-01-2010');

    });

});

describe('when setting a new target date for the case', () => {

    beforeAll(() => {

        navigate.as('admin', 84);

        let data = {
            termAmount: '2',
            termType: 'Kalenderdagen'
        };

        changeTerm('term', data);

    });

    it('the target date of the case should have changed to the new target date', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual('27-04-2017');

    });

});

describe('when relating a case', () => {

    beforeAll(() => {

        navigate.as('admin', 85);

        relateCase('84');

        openTab('relations');

    });

    it('the case should have 76 as related case', () => {

        expect($('.related-cases [href="/intern/zaak/84"]').isPresent()).toBe(true);

    });

});

describe('when duplicating the case', () => {

    beforeAll(() => {

        navigate.as('admin', 86);

        duplicate();

        waitForSave();

        browser.ignoreSynchronization = true;

        openTab('timeline');

        waitForElement('[data-event-type="case/duplicate"]');

    });

    it('the case should have a duplicate message in the timeline', () => {

        let date = new Date(),
            day = date.getDate();

        expect(element.all(by.css('[data-event-type="case/duplicate"] .timeline-item-date')).first().getText()).toContain(day);
        expect(element.all(by.css('[data-event-type="case/duplicate"]')).first().getText()).not.toContain('Zaakgegevens gekopieerd van zaak 86 naar nieuwe zaak 87');

    });

    afterAll(() => {

        browser.ignoreSynchronization = false;

        navigate.to();

    });

});

describe('when relating an object', () => {

    beforeAll(() => {

        navigate.as('admin', 87);

        relateObject('Zaakactie object relateren');

        openTab('relations');

    });

    it('the case should have the related object', () => {

        expect($('.related_objects [href="/object/8c934ef8-c615-477e-8c64-6e12422ee778"]').isPresent()).toBe(true);

    });

});

describe('when changing the requestor of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 88);

        changeRequestor('natuurlijk_persoon', 'zaakacties');

    });

    it('the case should have a different requestor', () => {

        expect(getAboutValue('Aanvrager')).toEqual('z. zaakacties');

    });

});

describe('when changing the coordinator of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 89);

        changeCoordinator('coordinatorwijzigen');

    });

    it('the case should have a different coordinator', () => {

        expect(getAboutValue('Coordinator')).toEqual('Coordinator wijzigen');

    });

});

describe('when changing the department and role of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 90);

        changeDepartmentRole('-Zaakacties', 'Behandelaar');

    });

    it('the case should have a different department and role', () => {

        expect(getAboutValue('Afdeling')).toEqual('Zaakacties');

    });

});

describe('when changing the registration date of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 91);

        changeRegistrationDate('13-01-2010');

    });

    it('the case should have a different registration date', () => {

        expect(getAboutValue('Registratiedatum')).toEqual('13-01-2010');

    });

});

describe('when changing the target date of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 92);

        changeTargetDate('14-01-2010');

    });

    it('the case should have a different target date', () => {

        expect(getAboutValue('Streefafhandeldatum')).toEqual('14-01-2010');

    });

});

describe('when changing the destruction date of a case to a specific date', () => {

    beforeAll(() => {

        navigate.as('admin', 94);

        changeDestructionDate('fixed', '16-01-2010');

    });

    it('the case should have a different destruction date', () => {

        expect(getAboutValue('Uiterste vernietigingsdatum')).toEqual('16-01-2010');

    });

});

describe('when changing the destruction date of a case by letting it recalculate', () => {

    beforeAll(() => {

        navigate.as('admin', 95);

        changeDestructionDate('term', '5 jaar');

    });

    it('the case should have a different destruction date', () => {

        let expectedDateText = getFutureDate(365 * 5);

        expect(getAboutValue('Uiterste vernietigingsdatum')).toEqual(expectedDateText);

    });

});

describe('when changing the attribute of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 96);

        changeAttributes('Terugkoppeling', 'Change attribute');

    });

    it('the case should have a different value in the attribute', () => {

        expect($('[data-name="terugkoppeling"] textarea').getAttribute('value')).toEqual('Change attribute');

    });

});

describe('when changing the phase of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 97);

        $('.phase-advance-button').click();

        changePhase('Behandelen');

    });

    it('the case should have be in a different phase', () => {

        expect($('.phase-list .active [href="/intern/zaak/97/fase/behandelen/"]').isPresent()).toBe(true);

    });

});

describe('when changing the status from closed to open', () => {

    beforeAll(() => {

        navigate.as('admin', 98);

        changeStatus('In behandeling');

        toggle();

    });

    it('the case should have a different status', () => {

        expect($('[data-name="resolved"]').isPresent()).toBe(false);

    });

    describe('when changing the status from open to closed', () => {

        beforeAll(() => {

            changeStatus('Afgehandeld');

        });

        it('the case should have a different status', () => {

            expect($('[data-name="resolved"]').isPresent()).toBe(true);

        });

    });

});

describe('when changing the authentication settings of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 99);

        changeAuth('-Systeemrollen', 'Behandelaar', 'handle');

        selectAction('Rechten wijzigen');

    });

    it('the case should have different authentication settings', () => {

        expect($('zs-case-admin-view [checked="checked"]').isPresent()).toBe(true);

    });

});

describe('when changing the casetype of a case', () => {

    beforeAll(() => {

        navigate.as('admin', 100);

        changeCasetype('Zaakacties zaaktype wijzigen');

    });

    it('the name of the casetype should be of a different casetype', () => {

        expect($('.top-bar-current-location span').getText()).toEqual('Zaak 100: Zaakacties zaaktype wijzigen');

    });

    it('the attributes should be updated to those case should be of a different casetype', () => {

        expect($('[data-name="terugkoppeling"]').isPresent()).toBe(false);
        expect($('[data-name="zaakacties"]').isPresent()).toBe(true);

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
