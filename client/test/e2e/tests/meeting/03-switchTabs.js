import navigate from './../../functions/common/navigate';
import {
    header,
    tabs,
    openTab,
    meetings,
    proposals
} from './../../functions/meeting/meeting';

describe('when opening the meeting app', () => {

    beforeAll(() => {
        navigate.as('burgemeester', '/vergadering/bbv/');
    });

    it('should contain a header', () => {
        expect(header.isPresent()).toBe(true);
    });

    it('should contain tabs', () => {
        expect(tabs.all(by.css('a')).count()).toBe(2);
    });

    describe('when opening the archived tab', () => {

        beforeAll(() => {
            openTab(2);
            meetings.click();
        });

        it('when clicking on a tab it should navigate to that tab', () => {
            expect(proposals.count()).toEqual(2);
        });

        afterAll(() => {
            openTab(1);
        });

    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
