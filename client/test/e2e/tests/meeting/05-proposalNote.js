import navigate from './../../functions/common/navigate';
import {
    meetings,
    noteBar,
    noteTextarea,
    noteSave,
    notes,
    proposals
} from './../../functions/meeting/meeting';

describe('when opening a case and adding a note in the meeting app', () => {

    beforeAll(() => {
        navigate.as('burgemeester', '/vergadering/bbv/');

        meetings.click();

        proposals
            .first()
            .click();

        browser.waitForAngular();

        noteBar.click();

        noteTextarea
            .clear()
            .sendKeys('this is my note');

        noteSave.click();
    });

    it('the note should have the given content', () => {
        expect(notes.first().getText()).toEqual('this is my note');
    });

    it('the ability to add additional notes should still be present', () => {
        const addNote = element.all(by.css('.proposal-detail-view__noteplaceholder')).last();

        expect(addNote.getText()).toEqual('Voeg een nieuwe persoonlijke notitie toe');
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
