import navigate from './../../functions/common/navigate';
import {
    proposals,
    meetings,
    noteBarPlaceholder,
    voteBar,
    proposalDetailViews,
    getProposalViewValue
} from './../../functions/meeting/meeting';
import checkPresenceByText from './../../functions/common/input/checkPresenceByText';

describe('when clicking on a case in the caseList in the meeting app', () => {

    beforeAll(() => {
        navigate.as('burgemeester', '/vergadering/bbv/');
    });

    it('should navigate to the detailView', () => {
        meetings.click();
        proposals
            .last()
            .click();

        expect(browser.getCurrentUrl()).toContain('/!voorstel/');
    });

    it('should contain a detailView', () => {
        expect(proposalDetailViews.count()).toBeGreaterThan(0);
    });

    it('should have a detailView that contains a proposal note bar', () => {
        expect(noteBarPlaceholder.isPresent()).toBe(true);
    });

    it('should have a detailView that contains a proposal vote bar', () => {
        expect(voteBar.isPresent()).toBe(true);
    });

    it('should display the information of the case properly', () => {
        expect(getProposalViewValue('Zaaknummer')).toEqual('41');
        expect(getProposalViewValue('Zaaktype')).toEqual('Voorstel 2');
        expect(getProposalViewValue('Voorstel onderwerp')).toEqual('Openstaand voorstel');
        expect(getProposalViewValue('Vertrouwelijkheid')).toEqual('Openbaar');
        expect(getProposalViewValue('Voorstel agenderingswijze')).toEqual('Paraafstukken');
        expect(checkPresenceByText('Datum voorstel 1', '.proposal-detail-view__label')).toBe(false);
        expect(checkPresenceByText('Datum voorstel 2', '.proposal-detail-view__label')).toBe(true);
        expect(getProposalViewValue('Datum voorstel 2')).toEqual('16 jan. 2017');
        expect(getProposalViewValue('Resultaat')).toEqual('aangehouden');
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
