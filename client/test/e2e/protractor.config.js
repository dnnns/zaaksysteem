require('babel-core/register')({
    presets: [ 'es2015' ],
    plugins: [ 'add-module-exports' ]
});

var path = require('path'), // eslint-disable-line
    baseUrl = 'https://testbase-instance.dev.zaaksysteem.nl',
    reporter = require('./custom_reporter.js'),
    folder = process.env.npm_config_folder ? `${process.env.npm_config_folder}/**/` : '',
    file = process.env.npm_config_file ? process.env.npm_config_file : '*',
    testpath = `**/${folder}${file}.js`,
    specs,
    suites,
    capabilities,
    multiCapabilities,
    maxSessions,
    chromeOptions = {
        args: [
            '--disable-extensions',
            '--disable-web-security',
            '--start-maximized'
        ]
    };

if ( process.env.npm_config_e2etype === 'multi' ) {

    specs = [];

    let testScenarios = [
        'tests/common/**/*.js',
        'tests/form/**/*.js',
        'tests/intern/caseView/**/*.js',
        'tests/intern/dashboard/**/*.js',
        'tests/intern/plusMenu/**/*.js',
        'tests/intern/topbar/**/*.js',
        'tests/meeting/**/*.js',
        'tests/mor/**/*.js'
    ];

    multiCapabilities = testScenarios.map(testScenario => {
        return {
            browserName: 'chrome',
            chromeOptions,
            specs: [
                testScenario
            ]
        };
    });

    maxSessions = process.env.npm_config_e2esessions ? process.env.npm_config_e2esessions : testScenarios.length;

} else {

    specs = [
        `tests/${testpath}`
    ];

    suites = {
        common: `tests/common/${testpath}`,
        form: `tests/form/${testpath}`,
        intern: `tests/intern/${testpath}`,
        apps: [`tests/meeting/${testpath}`, `tests/mor/${testpath}`]
    };

    capabilities = {
        browserName: 'chrome',
        chromeOptions
    };

    multiCapabilities = [];

}

exports.config = {
    baseUrl,
    specs,
    suites,
    capabilities,
    multiCapabilities,
    maxSessions,
    chromeOnly: true,
    framework: 'jasmine2',
    directConnect: true,
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    allScriptsTimeout: 6000000,
    jasmineNodeOpts: {
        defaultTimeoutInterval: 6000000
    },
    onPrepare: () => {

        jasmine.getEnv().clearReporters();

        jasmine.getEnv().addReporter(reporter);

        browser.driver.get(`${baseUrl}/auth/logout`);

        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');

    }
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
