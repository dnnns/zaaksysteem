export default [
    {
        name: 'Basic casetype',
        id: '12',
        uuid: 'b75a33e1-401d-47a6-b41c-4293f7a33e5a'
    },
    {
        name: 'Afhandeltermijn wijzigen',
        id: '72',
        uuid: 'f3e9e3c2-b9b2-40b0-bbed-c29c80c4a462'
    },
    {
        name: 'Pauzeren',
        id: '16',
        uuid: '77a00b26-8a11-4fa5-b4da-a0177d51bafa'
    },
    {
        name: 'Systeemvoorwaarde Algemene instellingen Aan',
        id: '38',
        uuid: 'a74f3300-13dc-48a5-aafb-60d5e09376f2'
    },
    {
        name: 'Systeemvoorwaarde Algemene instellingen Uit',
        id: '39',
        uuid: '33468d51-538d-4501-8569-df8339bb92fe'
    },
    {
        name: 'Fase actie bij registratie',
        id: '71',
        uuid: 'a1186669-90de-4571-972f-54e47eb5e9fd'
    },
    {
        name: 'Vul waarde in',
        id: '45',
        uuid: 'f166be60-a2b4-466c-be1b-ac1dc8c43631'
    },
    {
        name: 'Toon en verberg',
        id: '42',
        uuid: 'dd042425-19d9-4f0c-ae34-77b1c8892ce1'
    },
    {
        name: 'Voorwaarden AND of OR',
        id: '40',
        uuid: 'e681bea9-d030-45a1-879e-078fb410c3f3'
    },
    {
        name: 'Voorwaarden kenmerktypen',
        id: '41',
        uuid: 'd720c45b-7c2b-4183-b654-f7ae2ac656ae'
    },
    {
        name: 'Systeemvoorwaarde contactkanaal',
        id: '36',
        uuid: '57071da5-6b68-4342-bf97-94ddd4f34d5f'
    },
    {
        name: 'Systeemvoorwaarde aanvrager type',
        id: '32',
        uuid: '90060214-2b71-4e1c-9fa4-5cb3092766da'
    },
    {
        name: 'Systeemvoorwaarde aanvrager postcode',
        id: '33',
        uuid: '0e0fb8f6-c661-49ab-aa83-bd38c7ebcf20'
    },
    {
        name: 'Toewijzing bij registratie geen opties',
        id: '73',
        uuid: 'e02140f8-5991-4b24-8d38-e3314d08b1c3'
    },
    {
        name: 'Toewijzing bij registratie mijzelf',
        id: '74',
        uuid: 'db92e509-4c84-480e-a002-18e22fb57308'
    },
    {
        name: 'Toewijzing bij registratie andere',
        id: '75',
        uuid: '155fdf57-01d4-49d5-9d11-095c0dd09ee3'
    },
    {
        name: 'Toewijzing bij registratie alle opties',
        id: '76',
        uuid: '66f17ea8-2af5-4b3d-9bb5-f3f1b4ecfb87'
    },
    {
        name: 'Alle kenmerken registratie',
        id: '30',
        uuid: '6c551b68-d6d8-4d27-a48d-413f62ebe741'
    },
    {
        name: 'Wijzig vertrouwelijkheid bij registratie aan',
        id: '77',
        uuid: '9e6a15ca-23e6-46b3-8c20-e4dcf1399d4e'
    },
    {
        name: 'Wijzig vertrouwelijkheid bij registratie uit',
        id: '78',
        uuid: '8dd0837b-e132-4fe5-95ec-ad878a9ed104'
    },
    {
        name: 'Contactgegevens aanpassen bij registratie',
        id: '79',
        uuid: '2ec9d1d8-75e1-49eb-84ca-7c9b8fec98a1'
    },
    {
        name: 'Zaaktypeactie betrokkene toevoegen',
        id: '80',
        uuid: '337d0a25-c40d-4455-93a3-594e30d3e5f1'
    },
    {
        name: 'Gegevens hergebruiken',
        id: '81',
        uuid: '1e143518-968d-474c-9ac3-ef29fdc5f220'
    }
];

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
