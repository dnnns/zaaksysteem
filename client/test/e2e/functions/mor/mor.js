import waitForElement from './../common/waitForElement';
import {
    scroll
} from './../common/mouse';

export const header = $('mor-nav');
export const tabs = $('.mor-nav__tabs');
export const activeTab = $('mor-app ui-view');
export const buttonBar = $('.mor-nav__button-bar');
export const searchUnit = $('.mor-nav__search');
export const results = element.all(by.css('[data-name="result"] vorm-radio-group label'));
export const supportlink = $('.case-list-view__supportlink a');
export const caseGroup = element.all(by.css('mor-app ui-view .case-group'));
export const caseViewButton = $('case-detail-view .case-detail-view__button-container button');
export const caseCompleteButton = $('case-complete-view .case-detail-view__button-container button');
export const loadMore = $('.case-group-loadmore button');
export const searchResultItems = element.all(by.css('zs-suggestion-list .suggestion'));
export const closeCaseButton = $('.case-detail-view__header .mdi-arrow-left');
export const closeCaseCompleteViewButton = $('.casecomplete-modal .case-detail-view__header .mdi-arrow-left');

export const getHeaderText = () => header.$('.mor-nav__title h1').getText();

export const openSearch = () => {
    buttonBar
        .$('[data-name="search"]')
        .click();
};

export const enterSearchTerm = searchTerm => {
    searchUnit
        .$('input')
        .sendKeys(searchTerm);
};

export const selectSearchTerm = () => {
    searchResultItems
        .first()
        .click();
};

export const getSearchResults = () => new Promise(resolve => {
    const searchResults = [];

    searchResultItems
        .each(searchResultItem =>
            searchResultItem
                .getText()
                .then(searchResult =>
                    searchResults.push(searchResult)

            )

        );

    resolve(searchResults);
});

export const search = searchTerm => {
    const searchTerms = searchTerm.constructor === Array ? searchTerm : [ searchTerm ];

    openSearch();

    for ( const term in searchTerms ) {
        enterSearchTerm(searchTerms[term]);
        selectSearchTerm();
    }
};

export const closeSearch = () => {
    searchUnit
        .$('.mor-nav__clearbutton')
        .click();
};

export const logout = () => {
    buttonBar.
        $('[data-name="logout"]')
        .click();
};

export const openView = view => {
    tabs
        .$(`a[href="/mor/zaken/${view}"]`)
        .click();
};

export const countCaselists = () => activeTab.all(by.css('.case-group')).count();

export const listCases = ( view = 1 ) => new Promise(resolve => {
    const caseItems = caseGroup.get(view - 1).all(by.css('case-list-item-list-item'));
    const cases = [];

    caseItems
        .each(caseItem =>
            caseItem
                .$('a')
                .getAttribute('href')
                .then(href => {
                    const caseNumber = href.substring(href.indexOf('!') + 9, href.length - 1);

                    cases.push(parseInt(caseNumber, 10));
                })
        );

    resolve(cases);
});

export const loadMoreCases = () => {
    scroll(0, 850);
    loadMore.click();
};

export const openCase = caseNumber => {
    $(`case-list-item-list-item a[href*="/!melding/${caseNumber}/"]`).click();
    waitForElement('.case-detail-view__body');
};

export const clickTransitionButton = () => {
    caseViewButton.click();
};

export const clickCompleteButton = () => {
    caseCompleteButton.click();
};

export const getTransitionButtonText = () =>
    caseViewButton
        .getText()
        .then(text => text.toLowerCase());

export const inputAttribute = ( name, value ) => {
    const attribute = $(`vorm-field[data-label="${name}"]`);

    attribute
        .getAttribute('data-type')
        .then(dataType => {

            switch (dataType) {

                case 'option':
                attribute
                    .$(`input[value="${value}"]`)
                    .click();
                break;

                case 'text':
                attribute
                    .$('input')
                    .sendKeys(value);
                break;

                case 'textarea':
                attribute
                    .$('textarea')
                    .sendKeys(value);
                break;

                default:
                break;
            }
    });
};

export const clearAttribute = name => {
    const attribute = $(`vorm-field[data-label="${name}"]`);

    attribute
        .getAttribute('data-type')
        .then(dataType => {

            switch (dataType) {

                case 'option':
                //impossible
                break;

                case 'text':
                attribute
                    .$('input')
                    .clear();
                break;

                case 'textarea':
                attribute
                    .$('textarea')
                    .clear();
                break;

                default:
                break;
            }
    });
};

export const inputAttributes = data => {
    for ( const index in data ) {
        inputAttribute(data[index].name, data[index].value);
    }
};

export const inputResult = result => {
    const resultOptions = element.all(by.css('[data-name="result"] .vorm-radio-option'));

    resultOptions
        .each(resultOption =>
            resultOption
                .$('span')
                .getText()
                .then(text => {
                    if ( text === result ) {
                        resultOption.$('input').click();
                    }
                })
        );
};

export const completeCase = ( result, data ) => {
    clickTransitionButton();
    inputAttributes(data);
    inputResult(result);
    clickCompleteButton();
};

export const checkValue = ( name, expectedValue ) => new Promise(resolve => {
    const attributes = element.all(by.css('.case-detail-view__body > .case-detail-view__list .case-detail-view__item'));

    attributes
        .each(attribute =>
            attribute
                .$('.case-detail-view__label')
                .getText()
                .then(text => {
                    if ( text === name ) {
                        attribute
                            .$('.case-detail-view__value')
                            .getText()
                            .then(value =>
                                resolve( value === expectedValue )
                            );
                    }
                })
        );
});

export const checkValues = data => new Promise(resolve => {
    for ( const index in data ) {
        checkValue(data[index].name, data[index].value)
            .then(value => {
                if ( !value ) {
                    resolve(value);
                } else if ( index + 1 === data.length ) {
                    resolve(value);
                }
            });
    }
});

export const closeCaseView = () => {
    closeCaseButton.click();
};

export const closeCaseCompleteView = () => {
    closeCaseCompleteViewButton.click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
