import {
    selectFirstSuggestion
} from './../common/select';
import {
    mouseOver
} from './../common/mouse';
import repeat from './../common/repeat';

export const resizeWidget = (widgetTitle, corner, x, y) => {
    //corner should be nw, ne, sw, se
    const widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..'));
    const cornerHandle = widget.$(`.handle-${corner}`);

    mouseOver(widget.$('.handle-se'));

    browser.driver
        .actions()
        .dragAndDrop(cornerHandle, { x, y })
        .mouseUp()
        .perform();
};

export const moveWidget = (widgetTitle, x, y) => {
    browser
        .driver
        .actions()
        .dragAndDrop(element(by.cssContainingText('.widget-header-title', widgetTitle)), { x, y })
        .mouseUp()
        .perform();
};

export const deleteWidget = (widgetTitle) => {
    element(by.cssContainingText('.widget-header-title', widgetTitle))
        .element(by.xpath('../../..'))
        .$('.widget-header-remove-button')
        .click();
};

export const acceptCase = (caseNumber) => {
    const caseRow = $(`[data-name="intake"] .table-row[href="/intern/zaak/${caseNumber}"]`);

    caseRow
        .$('.case-intake-action-take-on')
        .click();
};

export const refuseCase = (caseNumber) => {
    const caseRow = $(`[data-name="intake"] .table-row[href="/intern/zaak/${caseNumber}"]`);
    const confirmButton = $('.zs-modal-body .confirm-button');

    caseRow
        .$('.case-intake-action-reject')
        .click();

    confirmButton.click();
};

export const addFavoriteCasetype = (casetype) => {
    selectFirstSuggestion($('.widget-favorite-suggest input'), casetype);
    browser.sleep(1000);
};

export const removeFavoriteCasetype = (casetype) => {
    const itemToRemove = element(by.cssContainingText('.widget-favorite-link', casetype)).element(by.xpath('..'));

    itemToRemove
        .$('.widget-favorite-remove')
        .click();
};

export const startFavoriteCasetype = (casetype) => {
    element(by.cssContainingText('.widget-favorite-link', casetype)).click();
};

export const filterWidget = (widgetTitle, text) => {
    const widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..'));
    const widgetFilterButton = widget.$('.widget-header-filter button');
    const widgetFilter = widget.$('.widget-header-filter input');

    widgetFilterButton.click();
    widgetFilter.sendKeys(text);
};

export const navigateWidgetPages = (widgetTitle, navType, numberOfClicks = 1) => {
    const widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..'));
    const widgetPaginationButton = widget.$(`.pagination-buttons .pagination-button-${navType}`);

    repeat(numberOfClicks, () => widgetPaginationButton.click());
};

export const setWidgetMaxResults = (widgetTitle, numberOfPages) => {
    const widget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..'));
    const widgetPaging = widget.$('.pagination select');

    widgetPaging.sendKeys(numberOfPages);
};

export const getWidgetCases = (widgetTitle) => new Promise(resolve => {
    const casesInWidget = element(by.cssContainingText('.widget-header-title', widgetTitle)).element(by.xpath('../../..')).all(by.css('.zs-table-row'));
    const caseNumbers = [];

    casesInWidget
        .each(caseInWidget =>
            caseInWidget
                .$('[column-id="case.number"]')
                .getText()
                .then((caseNumber) =>
                    caseNumbers
                        .push(caseNumber)
                )
        );

    resolve(caseNumbers);
});

export const getWidgetCasetypes = () => new Promise(resolve => {
    const casetypesInWidget = element.all(by.css('[data-name=""] .widget-favorite-link'));
    const casetypeNames = [];

    casetypesInWidget
        .each(casetypeInWidget =>
            casetypeInWidget
                .getText()
                .then((casetypeName) =>
                    casetypeNames
                        .push(casetypeName)
            )
        );

    resolve(casetypeNames);
});

export const getIntakeIconType = (caseNumber) => $(`.table-row[href="/intern/zaak/${caseNumber}"] zs-case-intake-action-list > zs-icon`).getAttribute('icon-type');

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
