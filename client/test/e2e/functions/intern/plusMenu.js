import {
    selectFirstSuggestion
} from './../common/select';
import getRandomBsn from './../common/getValue/getRandomBsn';
import getRandomKvk from './../common/getValue/getRandomKvk';
import inputFile from './../common/input/inputFile';
import inputDate from './../common/input/inputDate';
import {
    mouseOverCreateButton,
    mouseOut
} from './../common/mouse';
import waitForElement from './../common/waitForElement';

export const openAction = name => {

    mouseOverCreateButton();

    return element(
        by.css(`zs-contextual-action-menu-button [data-name=${name}] button`)
    ).click();

};

export const fillCreateCase = params => {

    const form = $('zs-contextual-action-menu form');
    const casetypeClose = '[data-name="casetype"] .mdi-close';
    const requestorTypeClose = '[data-name="requestor_type"] .mdi-close';
    const recipientClose = '[data-name="recipient"] .mdi-close';

    if ( params.rememberCasetype ) {

        form.$('.zs-switch-label').click();

    }

    if ( params.casetypeClear ) {

        form.$(casetypeClose).click();

    }

    if ( params.casetype ) {

        selectFirstSuggestion(form.$('[data-name="casetype"] .vorm-object-suggest input'), params.casetype);

    }

    if ( params.requestorTypeClear ) {

        form.$(requestorTypeClose).click();

    }

    if ( params.requestorType ) {

        form.$('[data-name="requestor_type"]')
            .$(`input[type=radio][value=${params.requestorType}]`)
            .click();

    }

    if ( params.requestor ) {

        selectFirstSuggestion(form.$('[data-name="requestor"] input'), params.requestor);

    }

    if ( params.requestorType === 'medewerker' ) {

        let recipientType = params.recipientType ? params.recipientType : 'medewerker';

        form.$('[data-name="recipient_type"]')
            .$(`input[type=radio][value=${recipientType}]`)
            .click();

        if ( params.recipientClear ) {

            form.$(recipientClose).click();

            selectFirstSuggestion(form.$('[data-name="recipient"] input'), params.recipient);

        }

    }

    if ( params.source ) {

        form.$('[data-name="source"] select').sendKeys(params.source);

    }

    form.submit();

    waitForElement('zs-case-registration');

};

export const createCase = params => {

    openAction('zaak');

    fillCreateCase(params);

};

export const createContact = newContact => {

    let form = $('zs-contextual-action-form form'),
        fields = {};

    openAction('contact');

    fields.type = {
            type: 'radio',
            fieldname: 'betrokkene_type',
            value: newContact.type === undefined ? undefined : newContact.type
        };
        
    if (newContact.type === 'natuurlijk_persoon') {

        fields.bsn = {
            type: 'text',
            fieldname: 'np-burgerservicenummer',
            value: newContact.bsn === undefined ? getRandomBsn() : newContact.bsn
        };
        fields.firstName = {
            type: 'text',
            fieldname: 'np-voornamen',
            value: newContact.firstName
        };
        fields.surnamePrefix = {
            type: 'text',
            fieldname: 'np-voorvoegsel',
            value: newContact.surnamePrefix
        };
        fields.lastName = {
            type: 'text',
            fieldname: 'np-geslachtsnaam',
            value: newContact.lastName === undefined ? 'lastName' : newContact.lastName
        };
        fields.sex = {
            type: 'radio',
            fieldname: 'np-geslachtsaanduiding',
            value: newContact.sex === undefined ? 'M' : newContact.sex
        };
        fields.country = {
            type: 'list',
            fieldname: 'np-landcode',
            value: newContact.country
        };

        if ( newContact.country === undefined ) {

            fields.correspondenceAddress = {
                type: 'checkbox',
                fieldname: 'briefadres',
                value: newContact.correspondenceAddress
            };
            fields.streetName = {
                type: 'text',
                fieldname: 'np-straatnaam',
                value:    newContact.streetName !== undefined ?
                        newContact.streetName
                        : newContact.correspondenceAddress === undefined ? 'streetName' : undefined
            };
            fields.houseNumber = {
                type: 'text',
                fieldname: 'np-huisnummer',
                value:    newContact.houseNumber !== undefined ?
                        newContact.houseNumber
                        : newContact.correspondenceAddress === undefined ? '1' : undefined
            };
            fields.houseNumberExtra = {
                type: 'text',
                fieldname: 'np-huisnummertoevoeging',
                value: newContact.houseNumberExtra
            };
            fields.zipcode = {
                type: 'text',
                fieldname: 'np-postcode',
                value:    newContact.zipcode !== undefined ?
                        newContact.zipcode
                        : newContact.correspondenceAddress === undefined ? '1111AA' : undefined
            };
            fields.city = {
                type: 'text',
                fieldname: 'np-woonplaats',
                value:    newContact.city !== undefined ?
                        newContact.city
                        : newContact.correspondenceAddress === undefined ? 'city' : undefined
            };
            fields.withinCity = {
                type: 'checkbox',
                fieldname: 'np-in-gemeente',
                value: newContact.withinCity
            };

            if ( newContact.correspondenceAddress !== undefined ) {

                fields.correspondenceStreetName = {
                    type: 'text',
                    fieldname: 'np-correspondentie_straatnaam',
                    value:    newContact.correspondenceAddress === undefined ?
                            undefined
                            : newContact.correspondenceStreetName === undefined ? 'streetName' : newContact.correspondenceStreetName
                };
                fields.correspondenceHouseNumber = {
                    type: 'text',
                    fieldname: 'np-correspondentie_huisnummer',
                    value:    newContact.correspondenceAddress === undefined ?
                            undefined
                            : newContact.correspondenceHouseNumber === undefined ? '1' : newContact.correspondenceHouseNumber
                };
                fields.correspondenceHouseNumberExtra = {
                    type: 'text',
                    fieldname: 'np-correspondentie_huisnummertoevoeging',
                    value:    newContact.correspondenceHouseNumberExtra
                };
                fields.correspondenceZipcode = {
                    type: 'text',
                    fieldname: 'np-correspondentie_postcode',
                    value:    newContact.correspondenceAddress === undefined ?
                            undefined
                            : newContact.correspondenceZipcode === undefined ? '1111AA' : newContact.correspondenceZipcode
                };
                fields.correspondenceCity = {
                    type: 'text',
                    fieldname: 'np-correspondentie_woonplaats',
                    value:    newContact.correspondenceAddress === undefined ?
                            undefined
                            : newContact.correspondenceCity === undefined ? 'city' : newContact.correspondenceCity
                };
            }

        } else {

            fields.foreignAddress1 = {
                type: 'text',
                fieldname: 'np-adres_buitenland1',
                value: newContact.foreignAddress1 === undefined ? 'foreignAddress1' : newContact.foreignAddress1
            };
            fields.foreignAddress2 = {
                type: 'text',
                fieldname: 'np-adres_buitenland2',
                value: newContact.foreignAddress2
            };
            fields.foreignAddress3 = {
                type: 'text',
                fieldname: 'np-adres_buitenland3',
                value: newContact.foreignAddress3
            };

        }

    } else {

        fields.country = {
            type: 'list',
            fieldname: 'vestiging_landcode',
            value: newContact.country
        };
        fields.tradeName = {
                type: 'text',
                fieldname: 'handelsnaam',
                value: newContact.tradeName === undefined ? 'tradeName' : newContact.tradeName
        };

        if ( newContact.country === undefined ) {

            fields.typeOfBusinessEntity = {
                type: 'list',
                fieldname: 'rechtsvorm',
                value: newContact.typeOfBusinessEntity === undefined ? 'Eenmanszaak' : newContact.typeOfBusinessEntity
            };
            fields.coc = {
                type: 'text',
                fieldname: 'dossiernummer',
                value: newContact.coc === undefined ? getRandomKvk() : newContact.coc
            };
            fields.establishmentNumber = {
                type: 'text',
                fieldname: 'vestigingsnummer',
                value: newContact.establishmentNumber
            };
            fields.streetName = {
                type: 'text',
                fieldname: 'vestiging_straatnaam',
                value: newContact.streetName === undefined ? 'streetName' : newContact.streetName
            };
            fields.houseNumber = {
                type: 'text',
                fieldname: 'vestiging_huisnummer',
                value: newContact.houseNumber === undefined ? '1' : newContact.houseNumber
            };
            fields.houseLetter = {
                type: 'text',
                fieldname: 'vestiging_huisletter',
                value: newContact.houseLetter
            };
            fields.houseNumberExtra = {
                type: 'text',
                fieldname: 'vestiging_huisnummertoevoeging',
                value: newContact.houseNumberExtra
            };
            fields.zipcode = {
                type: 'text',
                fieldname: 'vestiging_postcode',
                value: newContact.zipcode === undefined ? '1111AA' : newContact.zipcode
            };
            fields.city = {
                type: 'text',
                fieldname: 'vestiging_woonplaats',
                value: newContact.city === undefined ? 'city' : newContact.city
            };

        } else {

            fields.foreignAddress1 = {
                type: 'text',
                fieldname: 'vestiging_adres_buitenland1',
                value: newContact.foreignAddress1 === undefined ? 'foreignAddress1' : newContact.foreignAddress1
            };
            fields.foreignAddress2 = {
                type: 'text',
                fieldname: 'vestiging_adres_buitenland2',
                value: newContact.foreignAddress2
            };
            fields.foreignAddress3 = {
                type: 'text',
                fieldname: 'vestiging_adres_buitenland3',
                value: newContact.foreignAddress3
            };

        }

    }

    Object.keys(fields).forEach( key => {

        if ( fields[key].value !== undefined ) {

            switch (fields[key].type) {
                case 'radio':
                    form.$(`[data-name="${fields[key].fieldname}"] [value="${fields[key].value}"]`).click();
                    break;
                case 'list':
                    form.$(`[data-name="${fields[key].fieldname}"] select`).sendKeys(fields[key].value);
                    break;
                case 'checkbox':
                    form.$(`[data-name="${fields[key].fieldname}"] input`).click();
                    break;
                default:
                    form.$(`[data-name="${fields[key].fieldname}"] input`).sendKeys(fields[key].value);
                    break;
            }

        }

    });

    form.submit();

    browser.sleep(2000);

    browser.get('/intern');

};

export const createContactMoment = newContactMoment => {

    let form = $('zs-contextual-action-form form');

    openAction('contact-moment');

    form.$(`[data-name="subject_type"] [value="${newContactMoment.type}"]`).click();

    selectFirstSuggestion(form.$('[data-name="subject"] input'), '123456789');

    selectFirstSuggestion(form.$('[data-name="case"] input'), '43');

    form.$('[data-name="message"] textarea').sendKeys(newContactMoment.message);

    form.$('[type="submit"]').click();

};

export const createWidget = ( type, name ) => {

    openAction('create_widget');

    $(`zs-dashboard-widget-create [data-name="${type}"]`).click();

    browser.waitForAngular();

    if ( type === 'Zoekopdracht') {

        let searches = element.all(by.css('zs-dashboard-widget-search-select-list-group li'));

        searches.filter((elm) => {
            return elm.getText().then((text) => {
                return text === `${name}`;
            });
        }).first().click();

    }

};

export const uploadDocument = documentName => {

    let form = $('zs-contextual-action-form');

    openAction('upload');

    inputFile(form, documentName);

    mouseOut();

};

export const useTemplate = (data = {}) => {

    let form = $('zs-case-template-generate form');

    openAction('sjabloon');

    if (data.template) {

        form.$('[data-name="template"] select').sendKeys(data.template);

    }

    if (data.name) {

        form.$('[data-name="name"] input').clear().sendKeys(data.name);

    }

    if (data.filetype) {

        form.$(`[data-name="filetype"] input[value="${data.filetype}"]`).click();

    }

    if (data.caseDocument) {

        form.$('[data-name="case_document"] select').sendKeys(data.caseDocument);

    }

    form.submit();

    mouseOut();

};

export const relateSubject = data => {

    let form = $('zs-case-add-subject form');

    openAction('betrokkene');

    if ( data.subjectType ) {

        let subjectType = data.subjectType === 'organisation' ? 'bedrijf' : 'natuurlijk_persoon';

        form.$(`[data-name="relation_type"] input[value="${subjectType}"]`).click();

    }

    selectFirstSuggestion(form.$('[data-name="related_subject"] input'), data.subjectToRelate);

    if ( data.role ) {

        form.$('[data-name="related_subject_role"] select').sendKeys(data.role);

    }

    if ( data.roleOther ) {

        form.$('[data-name="related_subject_role_freeform"] input').sendKeys(data.roleOther);

    }

    if ( data.authorisation ) {

        form.$('[data-name="pip_authorized"] input').click();

    }

    if ( data.notification ) {

        form.$('[data-name="notify_subject"] input').click();

    }

    form.submit();

    mouseOut();

};

export const sendEmail = data => {

    let form = $('zs-case-send-email form');

    openAction('email');

    if ( data.template ) {

        form.$('[data-name="template"] select').sendKeys(data.template);

    }

    form.$('[data-name="recipient_type"] select').sendKeys(data.recipientType);

    if ( data.recipientEmployee ) {

        selectFirstSuggestion(form.$('[data-name="recipient_medewerker"] input'), data.recipientEmployee);

    }

    if ( data.recipientOther ) {

        form.$('[data-name="recipient_address"] input').sendKeys(data.recipientOther);

    }

    if ( data.cc ) {

        form.$('[data-name="recipient_cc"] input').sendKeys(data.cc);

    }

    if ( data.bcc ) {

        form.$('[data-name="recipient_bcc"] input').sendKeys(data.bcc);

    }

    if ( data.subject ) {

        form.$('[data-name="email_subject"] input').sendKeys(data.subject);

    }

    if (data.content ) {

        form.$('[data-name="email_content"] textarea').sendKeys(data.content);
        
    }

    form.submit();

};

export const planCase = data => {

    let form = $('zs-case-plan form');

    openAction('geplande-zaak');

    selectFirstSuggestion(form.$('[data-name="casetype"] input'), data.casetype);

    if ( data.copyRelations ) {

        form.$('[data-name="copy_relations"] input').click();

    }

    inputDate(form.$('[data-name="from"]'), data.from);

    if ( data.pattern ) {

        form.$('[data-name="has_pattern"] input').click();

        if ( data.count ) {

            form.$('[data-name="count"] input').clear().sendKeys(data.count);

        }

        if ( data.type ) {

            form.$('[data-name="type"] select').sendKeys(data.type);

        }

        if ( data.repeat ) {

            form.$('[data-name="repeat"] input').clear().sendKeys(data.repeat);

        }

    }

    form.submit();
    
    mouseOut();
    
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
