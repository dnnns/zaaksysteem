const { join } = require('path');

const {
  DEV_SERVER_ORIGIN,
  IS_DEV,
  PUBLIC_PATH,
  ROOT,
} = require('../../library/constants');

const publicPath = IS_DEV
  ? `${DEV_SERVER_ORIGIN}${PUBLIC_PATH}`
  : PUBLIC_PATH;

module.exports = {
  filename: '[name].js',
  path: join(ROOT, PUBLIC_PATH),
  publicPath,
};
