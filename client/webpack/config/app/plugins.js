const {
  DefinePlugin,
  DllReferencePlugin,
} = require('webpack');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin');
const HtmlWebpackAlwaysWritePlugin = require('../../plugins/HtmlWebpackAlwaysWritePlugin');

const { join } = require('path');

const cherryPack = require('../../library/cherryPack');
const apps = require('../../library/apps');
const {
  COMMONS_CHUNK,
  DEV_SERVER_ORIGIN,
  IS_DEBUG_BUILD,
  IS_DEV,
  PUBLIC_PATH,
  ROOT,
  VENDOR,
} = require('../../library/constants');

const {
  DISABLE_ANIMATION,
  SUPPORT_PROXY_URL,
} = require('../../library/packageConfig');

const basePlugins = [
  new DllReferencePlugin({
    context: process.cwd(),
    manifest: require(join('..', '..', 'manifest', 'vendors.json')),
  }),
  new DefinePlugin({
    ENV: {
      USE_SERVICE_WORKERS: !(IS_DEV || IS_DEBUG_BUILD),
      IS_DEV,
      DISABLE_ANIMATION: DISABLE_ANIMATION,
      SUPPORT_PROXY_URL: JSON.stringify(SUPPORT_PROXY_URL),
    },
  }),
  new NamedModulesPlugin(),
  new HtmlWebpackAlwaysWritePlugin({
    entries: apps,
    stripPrefix: cherryPack({
      production: '',
      development: DEV_SERVER_ORIGIN,
    }),
    root: ROOT,
  }),
  new CaseSensitivePathsPlugin(),
];

const getHtmlOptions = app => ({
  base: (`/${app}/`),
  chunks: [app, COMMONS_CHUNK],
  filename: join(app, 'index.html'),
  inject: 'body',
  minify: false,
  template: `${join('.', 'src', app, 'index.ejs')}`,
  title: 'Zaaksysteem',
});

module.exports = cherryPack({
  production() {
    const {
      optimize: {
        CommonsChunkPlugin,
        UglifyJsPlugin,
      },
    } = require('webpack');
    const ExtractTextPlugin = require('extract-text-webpack-plugin');
    const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
    const ServiceWorkerCachePlugin = require('../../plugins/ServiceWorkerCachePlugin');

    return [
      ...basePlugins,
      ...apps.map(app =>
        new HtmlWebpackPlugin(
          Object.assign({}, getHtmlOptions(app), {
            chunks: [app, 'shared'],
          }))
      ),
      ...(
        !IS_DEBUG_BUILD ?
          apps.map(app =>
            new ServiceWorkerCachePlugin({
              root: ROOT,
              publicPath: PUBLIC_PATH,
              entries: [app],
              globs: [join(app, 'index.html')],
              options: {
                navigateFallback: `${PUBLIC_PATH}${app}/index.html`,
                navigateFallbackWhitelist: [new RegExp(`^/${app}`)],
              },
              external: [
                VENDOR,
              ],
              commons: [
                COMMONS_CHUNK,
              ]
            }))
          : []
      ),
      new CommonsChunkPlugin({
        filename: '[name].js',
        name: COMMONS_CHUNK,
      }),
      new ExtractTextPlugin({
        filename: '[name].css',
        allChunks: true,
      }),
      ...(
        !IS_DEBUG_BUILD ?
          [new OptimizeCssAssetsPlugin({
            cssProcessorOptions: {
              safe: true,
            }
          })]
          : []
      ),
      new UglifyJsPlugin({
        mangle: !IS_DEBUG_BUILD,
        sourceMap: IS_DEBUG_BUILD,
      }),
    ];
  },

  development() {
    return [
      ...basePlugins,
      ...apps.map(app =>
        new HtmlWebpackPlugin(
          Object.assign({}, getHtmlOptions(app)), {
            chunks: app,
          }
        )
      )
    ];
  },
});
