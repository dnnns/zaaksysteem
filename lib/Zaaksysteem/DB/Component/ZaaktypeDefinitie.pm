package Zaaksysteem::DB::Component::ZaaktypeDefinitie;

use Moose;

BEGIN { extends 'DBIx::Class::Row'; }

sub preset_client_name {
    my $self = shift;

    return undef unless $self->preset_client;

    my $subject = $self->result_source->schema->betrokkene_model->get(
        {},
        $self->preset_client
    );

    return $subject ? $subject->display_name : 'Onbekende gebruiker';
}

sub preset_subject {
    my $self = shift;

    return unless $self->preset_client;

    my (undef, $type, $id) = split m[\-], $self->preset_client;

    my %type_map = (
        natuurlijk_persoon => 'NatuurlijkPersoon',
        bedrijf => 'Bedrijf',
        medewerker => 'Subject'
    );

    unless (defined $type && exists $type_map{ $type }) {
        return;
    }

    my $schema = $self->result_source->schema;

    my $subject = $schema->resultset($type_map{ $type })->find($id);

    return unless defined $subject;

    return $subject->as_object;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 preset_client_name

TODO: Fix the POD

=cut

