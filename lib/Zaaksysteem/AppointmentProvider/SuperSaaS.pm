package Zaaksysteem::AppointmentProvider::SuperSaaS;
use Moose;
use namespace::autoclean;

with qw(
    Zaaksysteem::AppointmentProvider::Roles::TestConnection
    Zaaksysteem::AppointmentProvider
    MooseX::Log::Log4perl
);

=head1 NAME

Zaaksysteem::AppointmentProvider::SuperSaaS - Appointment provider plugin for te SuperSaaS API

=head1 DESCRIPTION

Adds support for using L<SuperSaaS|http://www.supersaas.nl/> for appointments.

=cut

use DateTime::Format::ISO8601;
use JSON::XS;
use LWP::UserAgent;
use MooseX::Types::URI qw(Uri);
use URI::Escape qw(uri_escape_utf8);
use BTTW::Tools;
use Zaaksysteem::Tools::SysinModules qw(:certificates);
use Zaaksysteem::Types qw(Boolean NonEmptyStr);
use Zaaksysteem::ZAPI::Form::Field;

=head1 ATTRIBUTES

=head2 supersaas_endpoint

The URL of the SuperSaaS API endpoint.

=cut

has supersaas_endpoint => (
    is       => 'ro',
    isa      => Uri,
    required => 1,
    coerce   => 1,
);

=head2 supersaas_ca_certificate_use_system

Boolean value, indicating whether the "system" CA store should be used, or the
one configured in the C<supersaas_ca_certificate> field.

=cut

has supersaas_ca_certificate_use_system => (
    is       => 'ro',
    isa      => Boolean,
    required => 1,
);

=head2 supersaas_ca_certificate

CA certificate to use for verification when connecting to SuperSaaS.

=cut

has supersaas_ca_certificate => (
    is       => 'ro',
    required => 1,
);

=head2 supersaas_password

Password of the SuperSaaS superuser.

=cut

has supersaas_password => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head2 supersaas_future_days

Days into the future to list when requesting "available dates".

=cut

has supersaas_future_days => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 supersaas_products

List of products (hashrefs, with 2 keys: calendar_id and calendar_name) used to
create the product list in the catalog.

=cut

has supersaas_products => (
    is      => 'ro',
    isa     => 'ArrayRef[HashRef]',
    default => sub { [] },
);

=head2 ua

L<LWP::UserAgent> instance used to connect to the SuperSaaS API. By default, a
new instance is created, with trace logging hooks for request and response.

=cut

has ua => (
    is       => 'rw',
    isa      => 'LWP::UserAgent',
    default  => sub {
        my $self = shift;
        
        my $ca_file = $self->supersaas_ca_certificate;
        my $ua = LWP::UserAgent->new(
            ssl_opts => {
                verify_hostname => 1,
                $ca_file
                    ? (SSL_ca_file => "$ca_file")
                    : (SSL_ca_path => '/etc/ssl/certs'),
            },
        );

        if ($self->log->is_trace) {
            $ua->add_handler("request_send",  sub { $self->log->trace(shift->dump); return; });
            $ua->add_handler("response_done", sub { $self->log->trace(shift->dump); return; });
        }


        return $ua;
    },
    lazy     => 1,
    required => 0,
);

=head1 METHODS

=head2 shortname

Always returns the string C<supersaas>.

=cut

sub shortname { "supersaas" }

=head2 label

Always returns the string C<SuperSaaS>.

=cut

sub label { "SuperSaaS" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_supersaas_endpoint',
        type        => 'text',
        label       => 'API endpoint',
        default     => 'https://www.supersaas.com/api',
        required    => 1,
        description => '<p>URL waarop de SuperSaaS API te bereiken is.</p>',
        data        => {
            pattern     => '^https:\/\/.+',
            placeholder => 'https://www.supersaas.com/api',
        },
    ),
    ca_certificate(name => 'supersaas_ca_certificate'),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_supersaas_password',
        type        => 'password',
        label       => 'Superuser-wachtwoord',
        required    => 1,
        description => "<p>Wachtwoord van de superuser van het SuperSaaS-account.</p>",
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_supersaas_future_days',
        type        => 'number',
        label       => "Aantal dagen in de toekomst",
        required    => 1,
        default     => 7,
        description => "<p>Maximaal aantal dagen in de toekomst dat een afspraak te maken is.</p>",
        data        => { pattern => '^\d+$' },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_supersaas_products',
        type        => 'multiple',
        label       => "Agenda's",
        required    => 0,
        data => {
            fields => [
                {
                    name        => 'calendar_id',
                    type        => 'text',
                    label       => 'Agenda-id',
                    description => "<p>Het agenda-id is te vinden door op de SuperSaaS-omgeving op de 'Configureren'-knop bij een agenda te klikken. Het nummer aan het einde van de URL is het agenda-ID.</p>",
                    data        => { pattern => '^\d+$' },
                },
                {
                    name        => 'calendar_name',
                    type        => 'text',
                    label       => 'Agenda-naam',
                    description => "<p>Een korte beschrijvende naam voor deze agenda die getoond wordt bij het instellen van het kenmerk in de catalogus.</p>",
                },
            ],
        },
    ),
);

=head2 configuration_items

Returns a list of configuration items (L<Zaaksysteem::ZAPI::Form::Field>
instances) that are necessary to configure the appointment interface to
use the SuperSaaS API.

=cut

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

=head2 get_product_list

Retrieve the product list, as configured in the interface configuration.

Used to configure appointment plugin library attributes.

=cut

sub get_product_list {
    my ($self) = @_;

    return [
        map {
            {
                id => $_->{calendar_id},
                label => $_->{calendar_name},
            }
        } @{ $self->supersaas_products },
    ];
}

=head2 get_location_list

Retrieve a (fake) location list, used to configure appointment plugin library
attributes.

Because SuperSaaS doesn't have a concept of "locations", a static list with one
entry, 'Standaardlocatie', is returned.

=head3 Parameters

The id of the product to get a location list for. Should be an "id" as returned
by C<get_product_list>.

=cut

sub get_location_list {
    my ($self) = @_;

    return [
        { id => 1, label => "Standaardlocatie" },
    ];
}

=head2 get_dates

Retrieve a list of dates on which appointments can be planned.

Uses the "number of days in the future" configuration item.

=cut

sub get_dates {
    my ($self) = @_;

    my $date = DateTime->now();

    my @rv;
    for (0 .. $self->supersaas_future_days) {
        push @rv, $date->ymd;

        $date = $date->add(days => 1);
    }

    return \@rv;
}

=head2 get_timeslots

Retrieve a list of time slots that can be used to plan appointments. Uses the
"number of slots per day" configuration item to determine the number of slots.

=head3 Parameters

This method requires three arguments:

=over

=item * date

Date to request timeslots for

=item * product_id

Product ("calendar") id to get free timeslots for

=item * location_id

Location id to use (ignored, SuperSaaS doesn't use locations)

=back

=cut

sub get_timeslots {
    my ($self, $date, $product_id, $location_id) = @_;

    my $api_date = $date->clone->set_time_zone('Europe/Amsterdam');

    my %slots_data = $self->_supersaas_http(
        'GET',
        sprintf('/free/%d.json', $product_id),
        {
            # Even with 5-minute slots x 12 hour days, this should be more than enough
            maxresults => 200,
            # Only ask for slots after
            from       => $date->strftime('%F %T'),
        }
    );
    my $slots = JSON::XS->new()->decode($slots_data{response}->decoded_content);

    my %seen;

    my @rv;
    for my $slot (@{ $slots->{slots} }) {
        # If there are multiple slots for the same time, just show the first
        # It means there are multiple "resources" (rooms, etc.) that can be
        # booked.
        next if $seen{ $slot->{start} . $slot->{finish} };
        $seen{ $slot->{start} . $slot->{finish} } = 1;

        my $start  = DateTime::Format::ISO8601->parse_datetime($slot->{start})->set_time_zone('Europe/Amsterdam');
        my $finish = DateTime::Format::ISO8601->parse_datetime($slot->{finish})->set_time_zone('Europe/Amsterdam');

        next if $start->ymd ne $api_date->ymd;

        push @rv, {
            start_time  => $start->set_time_zone('UTC')->iso8601 . 'Z',
            end_time    => $finish->set_time_zone('UTC')->iso8601 . 'Z',
            plugin_data => {
                product_id    => $product_id,
                location_id   => $location_id,
                resource_id   => $slot->{id},
                resource_name => $slot->{name},
            },
        };

    }

    return \@rv;
}

=head2 book_appointment

Create a new appointment in the configured SuperSaaS calendar.

=head3 Arguments

One block of C<appointment_data>, in the same format as returned by
C<get_timeslots>, and one L<Zaaksysteem::Object::Types::Subject> instance (for
the person for whom he appointment is being made).

=head3 Returns

A new L<Zaaksysteem::Object::Types::Appointment> object (unsaved).

=cut

sub book_appointment {
    my ($self, $appointment_data, $requestor) = @_;

    for (qw(resource_id resource_name)) {
        throw("appointmentprovider/supersaas/invalid_data", "Invalid appointment: missing field '$_'")
            unless exists $appointment_data->{plugin_data}{$_};
    }

    my $api_start_time = $appointment_data->{start_time}->clone->set_time_zone('Europe/Amsterdam');
    my $api_finish_time = $appointment_data->{end_time}->clone->set_time_zone('Europe/Amsterdam');

    my %book_data = $self->_supersaas_http(
        'POST',
        '/bookings.json',
        {
            'booking[start]'       => $api_start_time->iso8601,
            'booking[finish]'      => $api_finish_time->iso8601,
            'booking[full_name]'   => $requestor->display_name,
            'schedule_id'          => $appointment_data->{plugin_data}{product_id},
            'booking[resource_id]' => $appointment_data->{plugin_data}{resource_id},
        }
    );

    my $location = $book_data{response}->header('Location');
    if (!$location) {
        throw(
            "appointmentprovider/supersaas/internal",
            "'Location' header not found in SuperSaaS response."
        );
    }

    ($appointment_data->{plugin_data}{appointment_id}) = $location =~ m{/([^/]+)\.json$};

    my $appointment = Zaaksysteem::Object::Types::Appointment->new(
        %$appointment_data,
        plugin_type => $self->shortname,
    );

    return $appointment;
}

=head2 delete_appointment

Remove an appointment from the SuperSaaS calendar.

=head3 Arguments

One L<Zaaksysteem::Object::Types::Appointment> instance, containing the
appointment to remove.

=head3 Returns

A hash reference, containing the success state (always true -- if something
goes wrong an exception is thrown).

=cut

sub delete_appointment {
    my ($self, $appointment) = @_;

    my %delete_data = $self->_supersaas_http(
        'DELETE',
        sprintf('/bookings/%s.json', $appointment->plugin_data->{appointment_id}),
        {
            schedule_id => $appointment->plugin_data->{product_id},
        },
    );

    return {
        success => \1,
    };
}

=head2 _test_host_port

Return the configured endpoint's host and port.

Required by L<Zaaksysteem::AppointmentProvider::Roles::TestConnection>.

=cut

sub _test_host_port {
    my $self = shift;
    my $uri = URI->new($self->supersaas_endpoint);
    return ($uri->host, $uri->port);
}

=head2 _test_certificate

Return the configured CA certificate. 

Required by L<Zaaksysteem::AppointmentProvider::Roles::TestConnection>.

=cut

sub _test_certificate {
    my $self = shift;

    return (
        ca => $self->supersaas_ca_certificate,
    );
}

=head2 _supersaas_http

A wrapper to handle calling of the SuperSaaS API.

=cut

sub _supersaas_http {
    my ($self, $method, $url_part, $params) = @_;

    my $url = URI->new($self->supersaas_endpoint . $url_part);
    $params->{password} = $self->supersaas_password;

    my $post_content;
    if ($method eq 'POST') {
        $post_content = _url_escape_hash($params);
    }
    else {
        $url->query_form($params);
    }

    my $request_method = HTTP::Request::Common->can($method);

    my $request = $request_method->(
        $url,
        ($method eq 'POST')
            ? ( Content => $post_content )
            : ()
    );
    my $response = $self->ua->request($request);

    if ($response->is_success) {
        return (
            request => $request,
            response => $response,
        );
    }
    else {
        throw(
            "appointmentprovider/supersaas/http_error",
            "SuperSaaS - HTTP error: " . $response->code,
            {
                request  => $request,
                response => $response,
            }
        );
    }
}

# Somehow, the internal "make a query string" bits in HTTP::Request::Common and
# URI don't cope well with the whole characters vs bytes thing.
# So we do it ourselves.
sub _url_escape_hash {
    my $params = shift;

    my @rv;
    for my $k (keys %$params) {
        my $v = uri_escape_utf8($params->{$k});

        push @rv, "$k=$v";
    }

    return join("&", @rv);
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
