package Zaaksysteem::Geo;
use Moose;

use Zaaksysteem::Geo::Location;
use BTTW::Tools;
use Geo::Coder::Google;

with 'MooseX::Log::Log4perl';

has google_api_version => (
    is      => 'ro',
    isa     => 'Num',
    default => 3,
);

has known_region => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return { nl => qr/Netherlands|Nederland/ };
    }
);

has _object_mapping => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return {
            route                       => 'street',
            locality                    => 'city',
            country                     => 'country',
            postal_code                 => 'zipcode',
            administrative_area_level_1 => 'province',
        };
    }
);

has key => (
    is       => 'rw',
);

has language => (
    is      => 'rw',
    default => sub {
        return 'nl';
    }
);

has region => (
    is      => 'rw',
    default => sub {
        return 'nl';
    }
);

has type => (
    is => 'rw'
);

has query => (
    is => 'rw',
);

has success => (
    is     => 'ro',
    writer => '_set_success',
);

has results => (
    is     => 'ro',
    writer => '_set_results',
);

has geocoder => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;
        my $geocoder    = Geo::Coder::Google->new(
            apiver      => $self->google_api_version,
            key      => $self->key,
            language    => $self->language,
            region      => $self->region,
        );
        # Prevent problems when network connection is not available, 2 seconds should be more
        # than enough: Bumped to 5 seconds
        $geocoder->ua->timeout(5);
        return $geocoder;
    },
);


sub geocode {
    my $self    = shift;

    die('Zaaksysteem::Geo: cannot code unless query is given')
        unless $self->query;

    my @results     = $self->geocoder->geocode(
        location    => $self->query
    );

    my @locations;
    for my $result (@results) {
        my $location = {
            search => $self->query,
        };

        my %address_components;
        for my $key (@{ $result->{address_components} }) {
            for my $type (@{ $key->{types} }) {
                $address_components{ $type} = $key;
            }
        }

        my $mapping = $self->_object_mapping;
        for my $key (keys %{$mapping}) {
            if (my $val = $address_components{ $key }->{long_name}) {
                $location->{ $mapping->{ $key } } = $val;
            }
            else {
                $self->log->warn("Missing $key in address components from Google. Pinpointing on map may cause issues");
            }
        }

        ### Region functionality doesn't seem to work, we can fix it manually
        ### by filtering the given region from it
        if (
            $self->region &&
            $self->known_region->{$self->region} &&
            $location->{country} !~ $self->known_region->{ $self->region }
        ) {
            next;
        }

        $location->{identification} = $location->{address} = $result->{formatted_address};

        unless (
            defined($result->{geometry}->{location}) &&
            UNIVERSAL::isa($result->{geometry}->{location}, 'HASH')
        ) {
            next;
        }

        $location->{coordinates} = $result->{geometry}->{location};

        my $location_object = Zaaksysteem::Geo::Location->new(
            $location
        );

        push(@locations, $location_object);
    }

    if (scalar(@locations)) {
        $self->_set_results(\@locations);
        $self->_set_success(1);
    }

    return $self->success;
}

sub TO_JSON {
    my $self    = shift;

    return [] unless $self->success;

    my @results = @_;
    for my $result (@{ $self->results }) {
        push(@results, $result->TO_JSON);
    }

    return \@results;
}

sub BUILD {
    my $self    = shift;

    $self->geocode if $self->query;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 GOOGLE_API_VER

TODO: Fix the POD

=cut

=head2 KNOWN_REGIONS

TODO: Fix the POD

=cut

=head2 OBJECT_MAPPING

TODO: Fix the POD

=cut

=head2 TO_JSON

TODO: Fix the POD

=cut

=head2 geocode

TODO: Fix the POD

=cut

