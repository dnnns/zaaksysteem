package Zaaksysteem::SBUS::Logging::Object;

use Moose;
use Data::Serializer;

use constant TO_DB_MAPPING => {
    mutatie_type        => 'mutatie_type',
    object_type         => 'object',
    kerngegeven         => 'kerngegeven',
    label               => 'label',
    error               => 'error_message',
    created             => 'created',
};

has [qw/
    mutatie_type
    object_type
    params
    error
    kerngegeven
    label

    parent_object
    traffic_object
    is_flushed
/] => (
    'is'    => 'rw'
);

has 'changes'   => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self = shift;

        return [];
    }
);

has 'created'   => (
    'is'        => 'rw',
    'default'   => sub {
        DateTime->now('time_zone'   => 'Europe/Amsterdam');
    }
);


sub record_has_changed {
    my $self    = shift;

    if (scalar(@{ $self->changes })) {
        return 1;
    }

    if (uc($self->mutatie_type) ne 'W') {
        return 1;
    }

    return;
}

sub change {
    my $self    = shift;
    my $opt     = shift;

    die('Invalid options for change') unless(
        exists($opt->{column}) &&
        exists($opt->{old}) &&
        exists($opt->{new})
    );

    push(
        @{ $self->changes },
        $opt
    );

    return 1;
}

sub success {
    return 1 unless shift->error;
    return;
}

sub flush {
    my ($self, $dbic) = @_;

    return if $self->is_flushed;

    my $TO_DB_MAPPING   = TO_DB_MAPPING;

    my $obj = Data::Serializer->new(
        'serializer'    => 'Storable',
    );

    my $create = {
        modified    => $self->created,
    };

    while (my ($key, $mapping) = each %{ $TO_DB_MAPPING }) {
        $create->{$mapping} = $self->$key;
    }

    if ($self->error) {
        $create->{error} = 1;
    }

    $create->{params}   = $obj->serialize($self->params)
        if $self->params;

    $create->{changes}  = $obj->serialize($self->changes)
        if $self->changes;

    if ($self->parent_object) {
        $create->{pid} = $self->parent_object->id;
    }

    if ($self->traffic_object) {
        $create->{sbus_traffic_id} = $self->traffic_object->id;
    }

    if (my $logobject = $dbic->resultset('SbusLogging')->create($create)) {
        $self->is_flushed(1);
        return $logobject;
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_DB_MAPPING

TODO: Fix the POD

=cut

=head2 change

TODO: Fix the POD

=cut

=head2 flush

TODO: Fix the POD

=cut

=head2 record_has_changed

TODO: Fix the POD

=cut

=head2 success

TODO: Fix the POD

=cut

