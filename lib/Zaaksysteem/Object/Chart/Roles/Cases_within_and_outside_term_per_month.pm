package Zaaksysteem::Object::Chart::Roles::Cases_within_and_outside_term_per_month;

use Moose::Role;
use Data::Dumper;

=head2 search_month

Filter a given resultset by a given month.

So, out of the given resultset, only give me the resultset for which the
afhandeldatum is within month x in year y.

=cut

sub search_month {
    my ($self, $rs, $earliest, $latest, $arguments) = @_;

    my $month = $arguments->{month} or die "need month";
    my $year  = $arguments->{year}  or die "need year";

    return $rs->search({
        "date_part('month', hstore_to_timestamp((me.index_hstore->'case.date_of_registration')))" => $month,
        "date_part('year', hstore_to_timestamp((me.index_hstore->'case.date_of_registration')))" => $year,
        "hstore_to_timestamp((me.index_hstore->'case.date_of_registration'))" => {
            '>=' => $earliest,
            "<=" => $latest,
        }
    });
}


=head2 search_months_with_cases

Get a list of months in which we have cases
this will simply yield 2013-01, 2013-02, 2013-03

=cut

sub search_months_with_cases {
    my ($self, $rs) = @_;

    # Get a list of months in which we have cases
    # this will simply yield 2013-01, 2013-02, 2013-03
    my $resultset = $rs->search(
        {
            -and => [
                \"defined(index_hstore, 'case.date_of_completion')"
            ]
        }, {
        select => [
            {
                date_part => "'month', hstore_to_timestamp((me.index_hstore->'case.date_of_completion'))",
                -as => 'month'
            },
            {
                date_part => "'year', hstore_to_timestamp((me.index_hstore->'case.date_of_completion'))",
                -as => 'year',
            },
        ],
        group_by => [ 'year', 'month'],
        order_by => [ 'year', 'month']
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my @results = $resultset->all();

    # could be combined to one query?
    my $agg_rs = $self->resultset->search(
        {
            -and => [
                \"defined(index_hstore, 'case.date_of_completion')"
            ]
        },
        {
            select => [
                { min => "hstore_to_timestamp((me.index_hstore->'case.date_of_completion'))" },
                { max => "hstore_to_timestamp((me.index_hstore->'case.date_of_completion'))" },
            ],
            "as" => [ 'min', 'max' ],
            order_by => undef,
        }
    );
    $agg_rs->result_class('DBIx::Class::ResultClass::HashRefInflator');
    my $minmax = $agg_rs->first;
    my $earliest = $minmax->{min};
    my $latest   = $minmax->{max};

    my @res = @results[-12..-1];

    # We're only interested the last year (for performance reasons)
    return ($earliest, $latest, grep { defined($_) } @res);
}

=head2 cases_within_and_outside_term_per_month

Generate chart profile

=cut

sub cases_within_and_outside_term_per_month {
    my ($self) = @_;

    my ($earliest, $latest, @results) = $self->search_months_with_cases($self->resultset);

    my @month_names = qw/januari februari maart april mei juni juli augustus september oktober november december/;

    my $categories = [];
    my $within     = [];
    my $outside    = [];

    # Loop through the months
    foreach my $result (@results) {

        my $month = $month_names[$result->{month}-1] . ' '. $result->{year};

        push @$categories, $month;

        my $within_and_outside_rs = $self->search_month($self->resultset, $earliest, $latest, $result);

        my $within_and_outside = $self->within_and_outside_term(
           $within_and_outside_rs
        );

        push @$within, $within_and_outside->{within};
        push @$outside, $within_and_outside->{outside};
    }

    my $series = [{
        name => 'Binnen',
        data => $within,
        color => 'green'
    }, {
        name => 'Buiten',
        data => $outside,
        color => 'red'
    }];

    my $profile = {
        chart => {
            type => 'column'
        },
        title => {
            text => "Zaken binnen/buiten termijn per maand (%)"
        },
        xAxis => {
            categories => $categories
        },
        yAxis => {
            min => '0',
            title => {
                text => 'Percentage zaken'
            }
        },
        tooltip => {
            headerFormat => '<div style="font-size:10px">{point.key}</div><table>',
#            pointFormat => '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' .
#                '<td style="padding:0"><b>{point.y:.f} zaken</b></td></tr>',
            pointFormat => '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.0f}%</b><br/>',
            footerFormat => '</table>',
            shared => 1,
            useHTML => 1
        },
        plotOptions => {
            column => {
                stacking => 'percent'
            }
        },
        series => $series
    };

    return $profile;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

