package Zaaksysteem::Object::Types::Event;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Object::Types::Event

=cut

extends 'Zaaksysteem::Object';

with 'Zaaksysteem::Object::Roles::Security';

use Zaaksysteem::Types qw(NonEmptyStr);

=head1 DESCRIPTION

Here be dragons

=head1 ATTRIBUTES

=head2 event_type

The type of event

=cut

has event_type => (
    is       => 'ro',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Event type',
    required => 1,
);

=head2 event_data

The type of event

=cut

has event_data => (
    is       => 'ro',
    isa      => 'HashRef',
    traits   => [qw(OA)],
    label    => 'Event data',
    required => 1,
);

=head2 event

A reference to the object that was created, modified or deleted by the event

=cut

has event => (
    is     => 'ro',
    type   => 'object',
    traits => [qw(OR)],
    label  => 'Related event objects',
);

=head2 event_id

The legacy event ID.

=cut


has event_id => (
    is    => 'ro',
    isa   => 'Int',
    required => 1,
    traits => [qw(OA)],
    label => 'Legacy event ID',
);

=head2 created_by

Created by L<Zaaksysteem::Object::Types::Subject>

=cut

has created_by => (
    is     => 'ro',
    type   => 'subject',
    traits => [qw(OR)],
    label  => 'Created by',
);

=head2 modified_by

Modified by L<Zaaksysteem::Object::Types::Subject>

=cut

has modified_by => (
    is     => 'ro',
    type   => 'subject',
    traits => [qw(OR)],
    label  => 'Modified by',
);

=head2 created_for

Created for L<Zaaksysteem::Object::Types::Subject>

=cut

has created_for => (
    is     => 'ro',
    type   => 'subject',
    traits => [qw(OR)],
    label  => 'Created for',
);


__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
