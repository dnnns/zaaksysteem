package Zaaksysteem::Object::Types::CountryCode;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(NonEmptyStr);
use BTTW::Tools;

use Zaaksysteem::Object::ConstantTables qw/COUNTRY_TABLE/;

=head1 NAME

Zaaksysteem::Object::Types::CountryCode - Built-in object type implementing
a class for CountryCode objects

=head1 DESCRIPTION

An object class for country codes. This module implements codes for both Dutch and ISO 3166 codes.

=head1 SEE ALSO

L<https://www.iso.org/obp/ui/#search>


=head1 ATTRIBUTES

=head2 dutch_code

The Dutch government uses their own codes, these are known as RGBZ land codes.

=cut

has dutch_code => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'Dutch country code',
    required => 1,
    unique   => 1,
);

=head2 label

The label of the rechtsvorm (legal entity type).

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the country',
    required => 1,
);

=head2 alpha_one

The alpha one code of a country

=cut

has alpha_one => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'ISO 3166 Alpha 1 code',
    required => 0,
);

=head2 alpha_two

The alpha two code of a country

=cut

has alpha_two => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'ISO 3166 Alpha 2 code',
    required => 0,
);

has code => (
    is       => 'rw',
    isa      => 'Int',
    traits   => [qw(OA)],
    label    => 'ISO 3166 numeric code',
    required => 0,
);

=head1 METHODS

=head2 new_from_code

    $country = Zaaksysteem::Object::Types::CountryCode->new_from_code(6030);

Loads object from dutch_code

=cut

sig new_from_code => 'Int';

sub new_from_code {
    my ($class, $code) = @_;

    my ($country) = grep { $_->{dutch_code} eq int($code) } @{ COUNTRY_TABLE() };

    return $class->new(%$country) if $country;

    throw(
        'object/types/countrycode/unknown_code',
        "No country found by code: $code"
    );

    return $class->new(%$country);
}

sig new_from_name => 'Str';

sub new_from_name {
    my ($class, $name) = @_;

    my ($country) = grep { lc($_->{label}) eq lc($name) } @{ COUNTRY_TABLE() };

    return $class->new(%$country) if $country;

    throw(
        'object/types/countrycode/unknown_name',
        "No country found by name: $name"
    );
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
