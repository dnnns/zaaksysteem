package Zaaksysteem::Object::Types::MunicipalityCode;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use Zaaksysteem::Types qw(NonEmptyStr);
use BTTW::Tools;

use Zaaksysteem::Object::ConstantTables qw/MUNICIPALITY_TABLE/;

=head1 NAME

Zaaksysteem::Object::Types::MunicipalityCode - Built-in object type implementing
a class for MunicipalityCode objects

=head1 DESCRIPTION

An object class for municipality codes. This module implements codes for dutch municipalities

=head1 ATTRIBUTES

=head2 dutch_code

The Dutch government uses their own codes, these are known as RGBZ gemeente codes.

=cut

has dutch_code => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'Dutch municipality code',
    required => 1,
    unique   => 1,
);

=head2 label

The name of the municipality

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the municipality',
    required => 1,
);

=head1 METHODS

=head2 new_from_code

    $municipality = Zaaksysteem::Object::Types::MunicipalityCode->new_from_code(3);

Loads object from dutch_code

=cut

sig new_from_code => 'Int';

sub new_from_code {
    my ($class, $code) = @_;

    my ($municipality) = grep { $_->{dutch_code} eq int($code) } @{ MUNICIPALITY_TABLE() };

    return $class->new(%$municipality) if $municipality;

    throw(
        'object/types/municipalitycode/unknown_code',
        "Unknown code no municipality by code '$code'"
    );

}

sig new_from_name => 'Str';

sub new_from_name {
    my ($class, $name) = @_;

    my ($municipality) = grep { lc($_->{label}) eq lc($name) } @{ MUNICIPALITY_TABLE() };

    return $class->new(%$municipality) if $municipality;

    throw(
        'object/types/municipalitycode/unknown_name',
        "Unknown name no municipality by name '$name'"
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
