package Zaaksysteem::Object::Types::Appointment;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

with qw[
    Zaaksysteem::Object::Roles::Security
];

use Zaaksysteem::Types qw(Timestamp);

=head1 NAME

Zaaksysteem::Object::Types::Appointment - Appointment (in a remote system)

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 start_time

Start date and time of the appointment.

=cut

has start_time => (
    is       => 'rw',
    isa      => Timestamp,
    traits   => [qw[OA]],
    label    => 'Appointment start',
    required => 1,
);

=head2 end_time

End date and time of the appointment.

=cut

has end_time => (
    is       => 'rw',
    isa      => Timestamp,
    traits   => [qw[OA]],
    label    => 'Appointment end',
    required => 1,
);

=head2 plugin_type

(Short)name of the L<Zaaksysteem::AppointmentProvider> that was used to create this
appointment.

=cut

has plugin_type => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw[OA]],
    label    => 'Plugin type',
    required => 1,
);

=head2 plugin_data

Plugin-specific data used to identify the specific appointment (so it can be
deleted properly later on, if required).

=cut

has plugin_data => (
    is       => 'rw',
    isa      => 'HashRef',
    traits   => [qw[OA]],
    label    => 'Plugin-specific data',
    required => 1,
);

sub _format_appointment_dates {
    my ($self, $dt) = @_;

    my $fmt = DateTime::Format::Strptime->new(pattern => '%F %R');
    $dt->set_time_zone('Europe/Amsterdam');
    return $fmt->format_datetime($dt);
}

override _as_string => sub {
    my $self = shift;

    return join(" - ",
        $self->_format_appointment_dates($self->start_time),
        $self->_format_appointment_dates($self->end_time)
    );
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
