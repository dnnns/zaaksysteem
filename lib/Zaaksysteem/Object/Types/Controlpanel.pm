package Zaaksysteem::Object::Types::Controlpanel;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation Zaaksysteem::Object::Roles::Security);


use Zaaksysteem::Types qw(FQDN ArrayRefIPs CustomerType Betrokkene NonEmptyStr JSONBoolean JSONNum);
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Object::Types::Controlpanel - Built-in object type implementing
a class for customer configuration entries.

=head1 DESCRIPTION

Controlpanel objects are objects in which generic customer related information is stored.

=head1 ATTRIBUTES

=head2 owner

The betrokkene id of the customer/owner

=cut

has owner => (
    is       => 'ro',
    isa      => Betrokkene,
    traits   => [qw(OA)],
    label    => 'Owner ID',
    required => 1,
    unique   => 1,
);

=head2 shortname

A short name for the customer, will be used for provisioning purposes, eg database name generation, etc.

=cut

has shortname => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Shortname',
    required => 0,
    default  => sub {
        my $self = shift;
        return $self->owner;
    },
    lazy     => 1,
);

=head2 template

Default template

=cut

has template => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Template',
    required => 1,
);

=head2 ipaddresses

A list of IP adresses the customer has on the platform

=cut

has ipaddresses => (
    is       => 'rw',
    isa      => ArrayRefIPs,
    traits   => [qw(OA)],
    label    => 'IP addresses',
    clearer  => '_clear_ipaddresses',
);

=head2 customer_type

A type of customer, currently supported: commercial and government

=cut

has customer_type => (
    is       => 'rw',
    isa      => CustomerType,
    traits   => [qw(OA)],
    label    => 'Type klant',
    required => 1,
);


=head2 domain

The domain where all instances are created

=cut

has domain => (
    is       => 'rw',
    isa      => FQDN,
    traits   => [qw(OA)],
    label    => 'Domain below which we can create hosts for instances',
    required => 1,
);

=head2 read_only

Boolean to determine if write actions are allowed on all control panel objects.

=cut

has read_only => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    label    => 'Read only',
    required => 0,
    coerce   => 1,
);

=head2 allowed_instances

The current number of hosts in use

=cut

has allowed_instances => (
    is       => 'rw',
    isa      => JSONNum,
    traits   => [qw(OA)],
    label    => 'Allowed number of instances',
    default  => 25,
    coerce   => 1,
);

=head2 stat_diskspace

The current amount of diskspace in use in GBs

=cut

has stat_diskspace => (
    is       => 'rw',
    isa      => JSONNum,
    traits   => [qw(OA)],
    label    => 'Current amount of diskspace in use',
    coerce   => 1,
);

=head2 allowed_diskspace

The current amount of diskspace in use in GBs

=cut

has allowed_diskspace => (
    is       => 'rw',
    isa      => JSONNum,
    traits   => [qw(OA)],
    label    => 'Current amount of diskspace in use',
    default  => 100,
    coerce   => 1,
);


=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new(
        owner         => 'betrokkene-bedrijf-1',
        shortname     => 'betrokkene-bedrijf-1',
        customer_type => 'overheid',
    );
}

=head2 relatable_types

Returns a list of types that can be related to a Controlpanel object

=cut

sub relatable_types {
    return qw(instance host naw);
}

=head2 add_customer_d

Shortcut method for adding a relationship with a CustomerD object.

Dies when you want to set a relationship if there is already an existing relationship with another Controlpanel object.

=cut

sub add_customer_d {
    my ($self, $customer_d) = @_;

    my $existing_relation = $customer_d->relations();
    if (@$existing_relation > 0) {
        return 1 if $existing_relation->[-1]->related_object_id eq $self->id;
        throw('/customerconfig/set_parent', 'Existing relationship, unable to set new relationship');
    }

    return $self->relate(
        $customer_d,
        relationship_name_a => 'mother',
        relationship_name_b => 'child',
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
