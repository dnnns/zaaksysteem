package Zaaksysteem::Object::Types::Session::Invitation;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Session::Invitation - Session invitation data type

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use JSON::XS qw[];
use MIME::Base64 qw[encode_base64];
use Moose::Util::TypeConstraints qw[role_type];
use URI;

use Zaaksysteem::Types qw[Timestamp];

=head1 ATTRIBUTES

=head2 subject

=cut

has subject => (
    is => 'rw',
    type => 'subject',
    traits => [qw[OR]],
    label => 'Subject',
    required => 1
);

=head2 date_expires

=cut

has date_expires => (
    is => 'rw',
    isa => Timestamp,
    type => 'datetime',
    label => 'Expiration timestamp',
    coerce => 1,
    traits => [qw[OA]],
    required => 1
);

=head2 object

=cut

has object => (
    is => 'rw',
    # OR-attributes usually use the 'type' field, but we want global object
    # reference support here, so we use a plain 'isa' instead.
    isa => role_type('Zaaksysteem::Object::Reference'),
    label => 'Object',
    traits => [qw[OR]],
    predicate => 'has_object'
);

=head2 action_path

=cut

has action_path => (
    is => 'rw',
    isa => 'Str',
    label => 'API namespace constraint',
    traits => [qw[OA]],
    predicate => 'has_action_path'
);

=head2 token

=cut

has token => (
    is => 'rw',
    isa => 'Str',
    label => 'Authentication token',
    traits => [qw[OA]],
    required => 1
);

=head1 METHODS

=head2 as_uri

=cut

sig as_uri => 'URI, HashRef';

sub as_uri {
    my ($self, $base_uri, $actions) = @_;

    my $json = JSON::XS->new;

    my $data = $json->encode({
        %{ $actions },
        auth_token => $self->token,
        base_uri   => $base_uri->host
    });

    return sprintf('zaaksysteem://%s', encode_base64($data, ''));
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
