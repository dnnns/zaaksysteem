use utf8;
package Zaaksysteem::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2017-08-08 09:35:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:QpH2NoNR5kCk+SNcBJ03nA

__PACKAGE__->storage_type('Zaaksysteem::DB::Storage');

use Module::Find;
use Moose;
use File::Spec::Functions qw(catdir);
use Log::Log4perl;

use Zaaksysteem::Cache;
use Zaaksysteem::Betrokkene;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Schema - Schema definition for Zaaksysteem datamodel

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 CLEAR_PER_REQUEST

Definition list for the L</ATTRIBUTES> that should be cleared each request
cycle.

=cut

use constant CLEAR_PER_REQUEST => [qw/
    users
    current_user
    cache
    betrokkene_pager
    catalyst_config
    customer_instance
    current_user_ou_ancestry
    customer_config
/];

=head1 ATTRIBUTES

=head2 current_user_ou_ancestry

Stores a list of groups the L</current_user> is (implicitly via inheritance)
member of.

=cut

has 'current_user_ou_ancestry'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;

        return [ map {
            {
                ou_id => $_->id,
                name  => $_->name,
            }
        } @{ $self->current_user->inherited_groups } ];
    }
);

=head2 users

=head2 current_user

Stores a weak reference to the currently logged in user.

=cut

has [qw/users current_user/] => (
    'is'        => 'rw',
    'weak_ref'  => 1,
);

=head2 customer_config

Stores a weak reference to the cutomer config for the current request.

=cut

has customer_config => (
    is       => 'rw',
    lazy     => 1,
    isa      => 'Maybe[HashRef]',
    weak_ref => 1,
    default  => sub {
        my $self = shift;
        if ($self->customer_instance) {
            return $self->customer_instance;
        }
        return {};
    },
);

=head2 cache

=cut

has [qw/cache/] => (
    'is'        => 'rw',
);

=head2 log

Stores a weak reference to a L<Catalyst::Log> instance.

=cut

has 'log'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return Catalyst::Log->new }
);

=head2 betrokkene_pager

=head2 catalyst_config

=head2 customer_instance

=cut

has [qw/betrokkene_pager catalyst_config customer_instance/]  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return {} }
);

=head1 METHODS

=head2 betrokkene_model

Factory method that instantiates fresh L<Zaaksysteem::Betrokkene> instances.

=cut

sub betrokkene_model {
    my $self            = shift;

    my $dbic            = $self->clone;

    my $stash           = {};

    if ($self->betrokkene_pager) {
        for my $key (keys %{ $self->betrokkene_pager }) {
            $stash->{ $key } = $self->betrokkene_pager->{ $key };
        }
    }

    $dbic->cache($self->cache);

    return Zaaksysteem::Betrokkene->new(
        dbic            => $dbic,
        stash           => $stash,
        config          => $self->catalyst_config,
        customer        => $self->customer_instance,
    );
}

=head2 set_current_user

Installs the provided subject as current_user

=cut

sub set_current_user {
    my $self = shift;
    my $subject = shift;

    $self->default_resultset_attributes->{ current_user } = $subject;
    $self->current_user($subject);

    return $self;
}

=head2 lock

    $schema->lock($integer);

Set a blocking lock on the database.

This uses C<pg_advisory_lock>.
For more information see L<https://www.postgresql.org/docs/9.1/static/functions-admin.html>

=cut

sub lock {
    my ($self, $id) = @_;
    $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        $dbh->do('SELECT pg_advisory_lock(?)', undef, $id);
    });
}

=head2 unlock

    $schema->unlock($integer);

Unlock a lock on the database

This uses C<pg_advisory_unlock>.
For more information see L<https://www.postgresql.org/docs/9.1/static/functions-admin.html>

=cut

sub unlock {
    my ($self, $id) = @_;
    $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        $dbh->do('SELECT pg_advisory_unlock(?)', undef, $id);
    });
}

=head2 try_lock

    $schema->try_lock($integer);

Try setting a lock on the database, when a lock is aquired returns true otherwise false.

This uses C<pg_try_advisory_lock>.
For more information see L<https://www.postgresql.org/docs/9.1/static/functions-admin.html>

=cut

sub try_lock {
    my ($self, $id) = @_;
    my @lock = $self->storage->dbh_do(sub {
        my ($storage, $dbh) = @_;
        $dbh->selectrow_array('SELECT pg_try_advisory_lock(?)', undef, $id);
    });
    return $lock[0];
}

=head2 format_datetime_object

    $schema->format_datetime_object(DateTime->now());

Formats the DateTime object as recommended by
L<DBIx::Class::Manual::Cookbook/Formatting DateTime objects in queries>

=cut

sub format_datetime_object {
    my $self = shift;
    my $dt   = shift;
    return $self->storage->datetime_parser->format_datetime($dt);
}

=head1 PRIVATE METHODS

=head2 _ensure_all_db_classes_are_loaded

Ensures that every resultset class related to the schema files are loaded

=cut

sub _ensure_all_db_classes_are_loaded {
    for my $module (Module::Find::findallmod(__PACKAGE__)) {

        ## Do not bother with the default resultset...it is already loaded
        next if $module->resultset_class eq 'DBIx::Class::ResultSet';

        __PACKAGE__->ensure_class_loaded($module->resultset_class);
    }
}

=head2 _clear_schema

Cleanup method, see L</CLEAR_PER_REQUEST> constant for a definition of
attributes cleared.

=cut

sub _clear_schema {
    my $self            = shift;

    for my $attr (@{ CLEAR_PER_REQUEST() }) {
        $self->$attr(undef);
    }
}

=head2 _setup_schema

Setup the schema for a specific InstanceConfig

=cut

sub _setup_schema {
    my $self = shift;
    my $config = shift;

    my $cache    = Zaaksysteem::Cache->new(storage => { });
    my $logger   = Log::Log4perl->get_logger(ref $self);

    my $betrokkene_model = Zaaksysteem::Betrokkene->new(
        dbic     => $self,
        stash    => {},
        config   => $self->catalyst_config,
        customer => $config
    );

    $self->customer_config($config);
    $self->cache($cache);

    my %attributes = (
        cache  => $cache,
        log    => $logger,
        config => $self->catalyst_config,
        betrokkene_model => $betrokkene_model
    );

    for my $key (keys %attributes) {
        $self->default_resultset_attributes->{ $key } = $attributes{ $key };
    }

    return $self;
}

# Install our own exception action, so when DBIx throws one it's an
# Zaaksysteem::Exceptio::Base instance
__PACKAGE__->exception_action(sub {
    my $err = shift;

    if(blessed $err) {
        $err->throw if $err->can('throw');
        throw('db', $err->as_string, $err) if $err->can('as_string');
        throw('db', '' . $err, $err);
    }
    throw('db', $err);
});

## Run on catalyst setup time
_ensure_all_db_classes_are_loaded();

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
