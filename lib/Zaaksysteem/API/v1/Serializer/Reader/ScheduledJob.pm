package Zaaksysteem::API::v1::Serializer::Reader::ScheduledJob;
use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ScheduledJob - A serializer for ScheduledJobs

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::ScheduledJob->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    my $reader_method = 'read_' . $object->object_class;

    if ($class->can($reader_method)) {
        return sub { $class->$reader_method(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }

=head2 read_scheduled_job

Reader for Scheduled Job

=cut

sig read_scheduled_job => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_scheduled_job {
    my ($self, $serializer, $object) = @_;

    return {
        type => 'scheduled_job',
        reference => $object->id,
        instance => {
            id                      => $object->id,
            $self->_get_object_attributes($serializer, $object, qw(
                job
                next_run
                interval_period
                data
                date_created
                date_modified
            )),
            $self->_get_object_attributes_as_int($serializer, $object, qw(
                runs_left
                interval_value
                interface_id
            )),
        }
    };
}

=head2 parse_value

=cut

sub _parse_value {
    my ($class, $serializer, $value) = @_;
    return $serializer->read($value) if blessed($value) && !JSON::is_bool($value);
    return $value;
}

sub _get_object_attributes_as_int {
    my ($self, $serializer, $object, @attribute_list) = @_;
    return map { my $v = $self->_parse_value($serializer, $object->get_object_attribute($_)->value); $_ => defined $v ? $v + 0 : undef } @attribute_list;
}

sub _get_object_attributes {
    my ($self, $serializer, $object, @attribute_list) = @_;
    return map { $_ => $self->_parse_value($serializer, $object->get_object_attribute($_)->value) } @attribute_list;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
