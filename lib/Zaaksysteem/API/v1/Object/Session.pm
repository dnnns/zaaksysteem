package Zaaksysteem::API::v1::Object::Session;
use Moose;

use List::MoreUtils qw[any];
use Zaaksysteem::API::v1::Object::Session::Account;
use Zaaksysteem::Constants::Interfaces qw(ALLOWED_INTERFACES);

=head1 NAME

Zaaksysteem::API::v1::Object::Session - Current session object

=head1 SYNOPSIS

    my $obj =  Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

=head1 DESCRIPTION

A custom object for C<api/v1> containing the session information for this zaaksysteem. It contains
information about the company, the user, etc.

=head1 Tests

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/API/v1/Object/Session.pm

=head1 ATTRIBUTES

=head2 logged_in_user

B<required> L<Zaaksysteem::Backend::Subject::Component>

=cut

has 'logged_in_user' => (
    is          => 'ro',
    isa         => 'Zaaksysteem::Backend::Subject::Component',
);

=head2 hostname

B<required> C<Str>

Hostname of this session

=cut

has 'hostname' => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 design_template

B<required> C<Str>

The design template of this zaaksysteem

=cut

has 'design_template' => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 active_interfaces

Shows which interfaces are active for this session.

=cut

has active_interfaces => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub { return [] },
    handles => {
        add_interface => 'push'
    }
);

=head2 account

B<required> C<Zaaksysteem::API::v1::Object::Session::Account>

The current account settings of this session. E.g.: the company info.

=cut

has 'account' => (
    is          => 'ro',
    isa         => 'Zaaksysteem::API::v1::Object::Session::Account',
    required    => 1,
);

=head2 capabilities

Aggregated capabilities the user associated with the session has, implied or
explicit.

=cut

has capabilities => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub { return [] },
    handles => {
        add_capability => 'push'
    }
);

=head2 configurable

Show items from the configuration table which are relevant for the user
session

=cut

has configurable => (
    is      => 'rw',
    isa     => 'ArrayRef',
    traits  => [qw(Array)],
    default => sub { [] },
    handles => {
        add_configurable => 'push',
    },
);

=head1 METHODS

=head2 new_from_catalyst

=over 4

=item Arguments: L<Catalyst>

=item Return value: L<Zaaksysteem::API::v1::Object::Session>

=back

    my $session = Zaaksysteem::API::v1::Object::Session->new_from_catalyst($c);

Constructs L<Zaaksysteem::API::v1::Object::Session> by inflating values from a Catalyst C<$c> object

=cut

sub new_from_catalyst {
    my $class   = shift;
    my $c       = shift;

    my $self = $class->new(
        account => Zaaksysteem::API::v1::Object::Session::Account->new_from_catalyst($c),
        hostname => $c->req->uri->host,
        design_template => $c->config->{ gemeente_id },
        ($c->user_exists ? (logged_in_user => $c->user) : ()),
    );

    if ($c->user_exists) {
        my $cap_skip_required = any {
            $c->user->has_legacy_permission($_)
        } qw[zaak_create_skip_required zaak_beheer];

        if ($cap_skip_required) {
            $self->add_capability('case_registration_allow_partial');
        }
    }

    $self->_get_active_interfaces_to_session($c);
    $self->_get_brk($c);
    return $self;
}

sub _get_active_interfaces_to_session {
    my $self = shift;
    my $c    = shift;

    my $model = $c->model('DB::Interface');

    my $active = $model->search_active(
        { module => [(ALLOWED_INTERFACES)] },
    );

    while (my $i = $active->next) {
        if ($i->module eq 'overheidio') {
            my $config = $i->get_interface_config;
            if ($config->{overheid_io_module_bag}) {
                $self->add_interface('overheidio_bag');
            }
            if ($config->{overheid_io_module_kvk} || $config->{overheid_io_module}) {
                $self->add_interface('overheidio_kvk');
            }
        }
        else {
            $self->add_interface($i->module);
        }
    }

}

sub _get_brk {
    my ($self, $c) = @_;
    my $brk = $c->model('DB::Config')->get_value('brk_demo');

    if ($brk) {
        $self->add_configurable("brk_demo");
    }
    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
