package Zaaksysteem::API::v1::CasetypeACL;
use Moose;

=head1 NAME

Zaaksysteem::API::v1::Casetype - Logic for our API v1 Controller and others

=head1 DESCRIPTION

This module exports casetype to user mappings for external partners that can then use
this information to map cases to users.

=head1 SYNOPSIS

=cut

with 'MooseX::Log::Log4perl';

use Zaaksysteem::Object::Types::CasetypeACL;
use BTTW::Tools;

=head1 ATTRIBUTES

=head2 schema

An L<Zaakstysteem::Schema> object.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);


=head2 get_casetype_users

Get a list of users that have access rights to the casetype object and the corresponding cases.

=cut

sig get_casetype_users => '?HashRef,?HashRef';

sub get_casetype_users {
    my ($self, $search, $opts) = @_;

    $search //= {};

    my $rs = $self->schema->resultset('CasetypeACL')->search_rs(
        $search,
        $opts,
    );

    my %rv;
    while (my $ct = $rs->next) {
        my $ct_acl = $rv{ $ct->casetype_uuid }
            // Zaaksysteem::Object::Types::CasetypeACL->new(
            casetype_uuid   => $ct->casetype_uuid,
            casetype_active => $ct->casetype_active
        );
        $ct_acl->add_user($ct);
        $rv{ $ct->casetype_uuid } = $ct_acl;
    }
    return [ values %rv ];
}

__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
