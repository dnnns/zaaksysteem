package Zaaksysteem::Model::Zaak;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Zaken::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Zaak - Catalyst model factory for
L<Zaaksysteem::Zaken::Model> instances.

=head1 SYNOPSIS

    my $zaak_model = $c->model('Zaak');

=head1 METHODS

=head2 prepare_arguments

Prepares the constructor arguments for L<Zaaksysteem::Zaken::Model>.

=cut

sub prepare_arguments {
    my ($self, $c, $args) = @_;

    my $queue_model = $args->{ queue_model } || $c->model('Queue', $args);
    my $subject = $args->{ subject } || $c->user;

    my $ret = {
        base_uri      => $c->uri_for('/'),
        schema        => $c->model('DB')->schema,
        queue_model   => $queue_model,
        subject_model => $c->model('BR::Subject'),
        object_model  => $c->model('Object'),
        queue         => $c->model('Queue'),
    };

    if (defined $subject) {
        $ret->{ user } = $subject;
    }

    return $ret;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
