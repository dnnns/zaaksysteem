package Zaaksysteem::Model::Plugins::Ogone;

use strict;
use warnings;

use BTTW::Tools;

use base 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Payment::Ogone',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $c) = @_;

    my $interface = $c->model('DB::Interface')->search_active({ module => 'ogone' })->first;

    unless (defined $interface) {
        throw(
            "plugin/ogone/not_configured",
            "Geen Ogone-configuratie gevonden, controleer de koppeling."
        );
    }

    my $prod = $interface->jpath('$.mode') eq 'production' && $c->config->{ otap } eq 'prod';

    return {
        c              => $c,
        log            => $c->log,
        session        => $c->session,
        baseurl        => $c->uri_for('/'),
        prod           => $prod,
        pspid          => $interface->jpath('$.ogone_id'),
        backurl        => $interface->jpath('$.return_url'),
        shapass        => $interface->jpath('$.shapass_in'),
        shapassout     => $interface->jpath('$.shapass_out'),
        hash_algorithm => $interface->jpath('$.hash_algorithm'),
        order_description_template => $interface->jpath('$.order_description_template'),
        offline_payment_name       => $interface->jpath('$.offline_payment_name') // 'Anders betalen',
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

