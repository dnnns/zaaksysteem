package Zaaksysteem::Model::Auth::Alternative;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Backend::Sysin::Auth::Alternative',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Auth::Alternative - Catalyst model factory for
L<Zaaksysteem::Backend::Sysin::Auth::Alternative>

=head1 SYNOPSIS

    my $model = $c->model('Auth::Alternative');

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Backend::Sysin::Auth::Alternative->new>>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    my $schema    = $c->model('DB')->schema;
    my $interface = $c->model('DB::Interface')->search_active({module => 'auth_twofactor'})->first;

    return {
        schema            => $c->model('DB')->schema,
        interface         => $interface,
        sms_sender        => $interface->jpath('$.sms_sender'),
        sms_endpoint      => $interface->jpath('$.sms_endpoint'),
        sms_product_token => $interface->jpath('$.sms_product_token'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
