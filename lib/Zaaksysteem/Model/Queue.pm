package Zaaksysteem::Model::Queue;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Object::Queue::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Queue - Catalyst model factory for
L<Zaaksysteem::Object::Queue::Model>.

=head1 SYNOPSIS

    my $queue_model = $c->model('Queue');

=cut

use Zaaksysteem::StatsD;

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Object::Queue::Model->new>>.

=cut

sub prepare_arguments {
    my ($self, $c, @args) = @_;

    my $olo_service_host = $c->config->{ olo_service_host } || $ENV{ OLO_SERVICE_HOST };
    my $virus_scan_service_host = $c->config->{ virus_scan_service_host } || $ENV{ VIRUS_SCAN_SERVICE_HOST };
    my $indexing_service_host = $c->config->{ indexing_service_host } || $ENV{ INDEXING_SERVICE_HOST };

    my %target_resolvers = (
        backend => sub {
            return $c->uri_for_action('/api/queue/run_item', [ shift->id ]);
        },

        olo => sub {
            return URI->new(sprintf('http://%s/sync', $olo_service_host));
        },

        virus_scanner => sub {
            return URI->new(sprintf('http://%s/filestore', $virus_scan_service_host));
        },

        index => sub {
            return URI->new(sprintf('http://%s/index_object', $indexing_service_host));
        }
    );

    return {
        table             => $c->model('DB::Queue'),
        base_uri          => $c->uri_for('/'),
        instance_hostname => $c->config->{ instance_hostname },
        betrokkene_model  => $c->model('Betrokkene', @args),
        subject_table     => $c->model('DB::Subject', @args),
        subject_model     => $c->model('BR::Subject', @args),
        statsd            => Zaaksysteem::StatsD->statsd,
        target_resolvers  => \%target_resolvers,
        geocoder          => $c->model('Geo', @args),

        $c->user_exists
            ? (object_model => $c->model('Object'))
            : (),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
