package Zaaksysteem::Zaken::ComponentZaakKenmerk;

use Moose;

use Data::Dumper;
use List::Util qw(any);
use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_CONSTANTS
/;

extends 'DBIx::Class';

has 'human_value'   => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self        = shift;
        my $definition  = $self->_get_veldoptie_definition($self->value_type);

        return $self->value unless $definition->{filter};

        my @values      = $self->value;

        for my $value (@values) {


        }

        if ($self->multiple) {



        }
    },
);


sub _get_veldoptie_definition {
    my $self    = shift;
    my $type    = shift;

    my $zaaksysteem_constants   = ZAAKSYSTEEM_CONSTANTS;

    return $zaaksysteem_constants->{veld_opties}->{$type};
}

sub _get_multiple {
    my $self    = shift;
    my $type    = shift;

    return $self->_get_veldoptie_definition(
        $self->value_type
    )->{multiple};
}

sub _kenmerk_find_or_create {
    my $self                = shift;
    my $bibliotheek_kenmerk = shift;


    ### Try to find existing kenmerk
    my $kenmerk;
    unless(ref($bibliotheek_kenmerk)) {
        $bibliotheek_kenmerk   = $self->result_source->schema
            ->resultset('BibliotheekKenmerken')->find(
                $bibliotheek_kenmerk
            );

        die(
            'Zaken::Kenmerken->_find_or_create: '
            . ' cannot find bibliotheek kenmerk by id'
        ) unless $bibliotheek_kenmerk;
    }


    my $kenmerken   = $self->result_source->schema
        ->resultset('ZaakKenmerken')->search(
            {
                bibliotheek_kenmerken_id    => $bibliotheek_kenmerk->id,
                zaak_id                     => $self->zaak_id->id,
            }
        );

    $kenmerk = $kenmerken->first if $kenmerken->count == 1;

    ### Found kenmerk, return it (this object);
    return $kenmerk if $kenmerk;

    ### Did not find existing kenmerk, finish this row
    $self->bibliotheek_kenmerken_id($bibliotheek_kenmerk->id);
    $self->naam($bibliotheek_kenmerk->naam);
    $self->value_type($bibliotheek_kenmerk->value_type);
    $self->multiple(
        (
            $self->_get_multiple ||
            $bibliotheek_kenmerk->type_multiple
        )
    );

    return $self;

}

1; #__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

=head2 insert

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

