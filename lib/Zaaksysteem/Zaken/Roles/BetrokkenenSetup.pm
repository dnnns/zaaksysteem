package Zaaksysteem::Zaken::Roles::BetrokkenenSetup;

use Moose::Role;
use Data::Dumper;

with 'Zaaksysteem::Zaken::Betrokkenen';

around '_create_zaak' => sub {
    my $orig            = shift;
    my $self            = shift;
    my ($opts)          = @_;

    $self->log->trace('Role [BetrokkenenSetup]: started');

    my %update = ();
    for my $betrokkene_target (qw/aanvrager behandelaar coordinator/) {
        my $betrokkene_target_p = $betrokkene_target . 's'; # PLURAL

        next unless $opts->{$betrokkene_target_p};

        unless (ref $opts->{$betrokkene_target_p} eq 'ARRAY') {
            $self->log->warn(
                'Geen aanvragers meegegeven of aanvragers != ARRAY'
            );
            return;
        }

        for my $betrokkene (@{ $opts->{ $betrokkene_target_p } }) {
            my $rv = $self->betrokkene_set($opts, $betrokkene_target, $betrokkene);

            if ($rv) {
                $update{$betrokkene_target} = {
                    id              => $rv,
                    verificatie     => $betrokkene->{verificatie}
                }

            }
        }
    }

    my $zaak = $self->$orig(@_);

    while (my ($betrokkene_target, $betrokkene_info) = each %update) {
        my $betrokkene = $self->result_source->schema->resultset('ZaakBetrokkenen')->find(
            $betrokkene_info->{id}
        );

        $betrokkene->zaak_id($zaak->id);
        $betrokkene->verificatie($betrokkene_info->{verificatie});
        $betrokkene->update;
    }

    unless ($zaak->aanvrager) {
        throw('Zaaksysteem/case/create', 'Geen aanvrager voor zaak, is toch echt verplicht');
    }

    $self->_betrokkene_setup_add_relaties(
        $zaak, $opts
    );

    $self->log->trace('Role [BetrokkenenSetup]: ended');
    return $zaak;
};

sub _betrokkene_setup_add_relaties {
    my ($self, $zaak, $opts) = @_;

    if ($opts->{betrokkene_relaties}) {
        for my $relatie (
            @{ $opts->{betrokkene_relaties} }
        ) {
            $zaak->betrokkene_relateren(
                $relatie
            );
        }
    }

    if ($opts->{ontvanger}) {
        $zaak->betrokkene_relateren(
            {
                betrokkene_identifier   => $opts->{ontvanger},
                rol                     => 'Ontvanger',
                magic_string_prefix     => 'ontvanger',
            }
        );
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
