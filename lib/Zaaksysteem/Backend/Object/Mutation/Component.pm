package Zaaksysteem::Backend::Object::Mutation::Component;

use Moose;

use Moose::Util::TypeConstraints qw[enum];

use BTTW::Tools;

extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::Backend::Object::Mutation::Component

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 read_only

=cut

has read_only => (
    is => 'rw',
    isa => 'Bool',
    default => 0
);

=head2 label

=cut

has label => (
    is => 'rw',
    isa => 'Str',
    default => ''
);

=head2 complete

=cut

has complete => (
    is => 'rw',
    isa => 'Bool',
    default => 1
);

=head2 state

=cut

has state => (
    is => 'rw',
    isa => enum([qw[pending executed failed]]),
    lazy => 1,
    default => sub {
        my $self = shift;

        if($self->get_column('lock_object_uuid')) {
            if($self->executed) {
                return 'failed';
            } else {
                return 'pending';
            }
        } else {
            if($self->executed) {
                return 'executed';
            } else {
                return 'unknown';
            }
        }
    }
);

=head2 messages

=cut

has messages => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub { [] },
    handles => {
        push_message => 'push',
        map_messages => 'map',
        all_messages => 'elements'
    }
);

=head1 METHODS

=head2 stringify

Returns a human readable(-ish) string with most identifying information of
the object mutation, formatted like C<type(id): label>.

=cut

sub stringify {
    my $self = shift;

    return sprintf(
        '%s(%s): %s',
        $self->type,
        $self->id || 'unsynched',
        $self->label
    );
}

=head2 TO_JSON

=cut

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        type => $self->type,
        label => $self->label,
        messages => $self->messages,
        read_only => $self->read_only ? \1 : \0,
        values => $self->values,
        state => $self->state,
        complete => $self->complete ? \1 : \0,
        object_uuid => $self->get_column('object_uuid')
    };
}

# Can't make_immutable, DBIx::Class::Row is incompatible...
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
