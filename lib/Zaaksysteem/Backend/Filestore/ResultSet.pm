package Zaaksysteem::Backend::Filestore::ResultSet;

use Moose;

use File::ArchivableFormats;
use File::Scan::ClamAV;
use Params::Profile;
use Path::Tiny;
use Try::Tiny;
use UUID::Tiny;

use BTTW::Tools;
use BTTW::Tools::File qw(get_file_extension);

use Zaaksysteem::Constants;

my $CHUNK_SIZE = 64*1024;

extends 'DBIx::Class::ResultSet';

with 'Zaaksysteem::Roles::FilestoreModel';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::FileType' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Filetype exception',
        alias       => 'throw_filetype_exception',
    },
);

=head1 METHODS

=head2 filestore_create

Creates a Filestore entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added to the file storage.

=item original_name [required]

The name the file originally has or had. This is always required to prevent
issues where a file storage (uuid-based) file name gets used.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=item force_mimetype [optional]

Force the given mimetype. Please use with care, the L<File::ArchivableFormats> module is smart enough to
find the mimetype for you.

=back

=head3 Returns

The newly created Filestore object.

=cut

define_profile 'filestore_create' => (
    required => [qw/
        original_name
        file_path
    /],
    optional => [qw/
        ignore_extension
        id
        force_mimetype
    /]
);

sub filestore_create {
    my $self = shift;
    my $opts = $_[0];

    my $args = assert_profile($opts)->valid;

    my $uuid = UUID::Tiny::create_uuid_as_string(UUID::Tiny::UUID_V4);

    my $db_params = {};

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    # only if it exists
    my $file_path = Path::Tiny->new($args->{file_path})
        ->assert( sub {
            $_->exists or throw(
                'filestore/no_readable_file',
                "File '$_' is does not exist or isn't readable"
            )
        }
    );
    # size check
    $db_params->{size}              = $file_path->stat->size or
        throw("filestore/create/empty", "Will not add an empty file");

    # archivable format mime-type
    my $af_info = _archivable_format( $file_path, $args->{force_mimetype} );
    $db_params->{is_archivable}     = $af_info->{DANS}{archivable};
    $db_params->{mimetype}          = $af_info->{mime_type};

    # filestore location
    my $filestore_engine = $self->filestore_model->get_default_engine();
    $db_params->{storage_location}  = [$filestore_engine->name];

    # others
    $db_params->{original_name}     = $args->{original_name};
    $db_params->{md5}               = $file_path->digest( { chunk_size => $CHUNK_SIZE }, "MD5" );
    $db_params->{uuid}              = $uuid;
    $db_params->{id}                = $args->{id} if $args->{id}; # XXX Create with duplicat ID's break
    $db_params->{virus_scan_status} = 'pending'; # let's be explicit, it's the default value too
    Zaaksysteem::StatsD->statsd->end('filestore.io.stat.time', $t0);

    # create entry in database
    $t0 = Zaaksysteem::StatsD->statsd->start;
    my $filestore_row = $self->create($db_params);
    Zaaksysteem::StatsD->statsd->end('filestore.db.create.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.create', 1);

    # write file to store
    $t0 = Zaaksysteem::StatsD->statsd->start;
    $filestore_engine->write( $uuid, $file_path->openr() );
    Zaaksysteem::StatsD->statsd->end('filestore.io.add.time', $t0);

    $self->_create_and_push_scan_queue_item($filestore_row);
    $self->_create_and_push_replicate_queue_item($filestore_row);

    return $filestore_row->discard_changes;
}

=head1 INTERNAL METHODS

=head2 _create_and_push_replicate_queue_item

=over

=item does a `create_item` on the 'Queue' resultset

=item pushes that result on the default_resultset_attributes->{queue_items}

=back

Returns the number of queued_items

=cut

sub _create_and_push_replicate_queue_item {
    my $self = shift;
    my $filestore_row = shift;

    my $qrs = $self->result_source->schema->resultset('Queue');
    my $item = $qrs->create_item('filestore_replicate',
        {
            label => 'Filestore-replicatie',
            data => {
                target => 'backend',
                filestore_id => $filestore_row->id
            },
        },
    );

    $qrs->queue_item($item);

    return;
}

sub _create_and_push_scan_queue_item {
	my $self = shift;
    my $filestore_row = shift;

    my $queue = $self->result_source->schema->resultset('Queue');

    my $item = $queue->create_item('scan_file', {
        label => 'Virus en malware scan',
        data => {
            target => 'virus_scanner',
            status => 'waiting',
            ignore_existing => 1,
            parameters => {
                file_id => $filestore_row->uuid
            }
        }
    });

    $queue->queue_item($item);
    $item->delete;

    return;
}

=head2 find_by_uuid

$result_source->find_by_uuid($uuid)

returns the firts result_row that search can find with given UUID

=cut

sub find_by_uuid {
    shift->search({ uuid => shift })->first;
}

=head2 _archivable_format($file_path, $force_mimetype)

Returns a L<File::ArchivableFormat> info object()

Unless C<$force_mimetype> is provided, it will try to take it from C<$file_path>

=cut

sub _archivable_format {
    my ($file_path, $force_mimetype) = @_;

    my $file_af = File::ArchivableFormats->new();
    my $af_info = $force_mimetype # XXX We no longer use $force_mimetype
        ? $file_af->identify_from_mimetype($force_mimetype)
        : $file_af->identify_from_path("$file_path");

    return $af_info;
}
#
# TODO: fix $force_mimetype here and all the way up and maybe tombstone it first

=head1 DBIX::Class::ResultSet METHODS

=head2 search_rs

restrict search and find methods commonly lnown in DBIx::Class to only return
results that have been scanned and found to be 'ok'.

limit results of any search or find to only return those ResultRows that have
been check and infected. Before the refactor of filestore_create() and Virus
Scan Service, only those files being uploaded that were not contaminated were
being inserted in the table. To be sure that we are not going to bubble up un-
wanted results, search_rs will only pass those that have been marked 'ok'

DBIx::Class internal search and/or find all call search_rs internally. Because
of the way DBIx::Class uses Class::C3, we can call next::method to actually
get the normal behaviour.

=cut

sub search_rs{
    my $self = shift;
    return $self
        ->next::method( @_ )
        ->next::method( { virus_scan_status => { -not_like => 'found%' } } )
}
#
# TODO: write search_any that does not have this build in filter.

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
