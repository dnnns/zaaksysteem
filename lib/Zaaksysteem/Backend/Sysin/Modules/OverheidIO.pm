package Zaaksysteem::Backend::Sysin::Modules::OverheidIO;
use Moose;

use Encode qw(encode_utf8);
use JSON;
use LWP::UserAgent;
use URI;

use BTTW::Tools;
use WebService::OverheidIO::KvK;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::OverheidIO - OverheidIO Koppelingen

=head1 DESCRIPTION

This koppeling will handle the different modules from OverheidIO. For now support is limited
to OpenKVK and BAG objects

=head1 SYNOPIS

    # Most of this module is used by third party modules, like L<Zaaksysteem::BR::Subject>

=head1 CONSTANTS

=head2 INTERFACE_ID

Contains the interface id: C<stufnps>

=cut

use constant INTERFACE_ID               => 'overheidio';

=head2 INTERFACE_CONFIG_FIELDS

Config fields for this interface, empty for now

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_overheid_io_key',
        type        => 'text',
        label       => 'Overheid.io API key',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_overheid_io_module_kvk',
        type        => 'checkbox',
        label       => 'Activeer Overheid IO voor KvK bevragingen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        when => 'interface_overheid_io_module_kvk === true',
        name  => 'interface_overheid_io_eherkenning_request',
        type  => 'checkbox',
        label => 'Activeren voor eHerkenning',
        description =>
            qq{Bevraag Overheid.io na het inloggen met eHerkenning zodat de meest recente gegevens in Zaaksysteem opgeslagen worden.},
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_overheid_io_module_bag',
        type        => 'checkbox',
        label       => 'Activeer Overheid IO voor BAG bevragingen',
    ),
];

=head2 MODULE_SETTINGS

Configuration for this module

=cut


use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Overheid IO Koppelingen',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => [],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 0,
    sensitive_config_fields         => [qw(overheid_io_key)],
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        search_companies      => {
            method  => 'search_companies',
        },
    },
    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },

    description => qq{
<p>
Deze koppeling is verantwoordelijke voor de integratie tussen de
verschillende API's bij OverheidIO en Zaaksysteem. Er dient een
abbonement aangevraagd te worden bij <a href="https://overheid.io/abonnementen">OverheidIO</a>
om gebruik te kunnen maken van deze interface.
</p>
<p>
    Voor meer informatie kunt u terecht op de
    <a href="http://wiki.zaaksysteem.nl/Redirect_koppelprofiel_Overheid_IO_koppelingen">
        Zaaksysteem Wiki
    </a>
</p>
},
};

=head2 _update_interface_config

Update the interface config to respect the new config items

=cut

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    my $legacy_config = delete $config->{overheid_io_module};
    return 0 unless $legacy_config;

    if ($legacy_config eq 'kvk') {
        $config->{overheid_io_module_kvk} = 1;
        $interface->update_interface_config($config);
        return 1;
    }
    return 0;
}


###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 METHODS

=head2 search_companies

    my $transaction = $interface->process_trigger('search_companies', { company => 'Mintlab' });

    my @result      = @{ $transaction->get_processor_params->{result} }

Searches for company entities matching the given params in remote systems

=cut

define_profile search_companies => (
    required => {},
    constraint_methods  => {
        'address_residence.zipcode' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[1-9][0-9]{3}[A-Z]{2}$/;
            return;
        },
        'address_residence.street_number' => sub {
            my $val     = pop;

            return 1 if $val =~ /^[0-9]+$/;
            return;
        }
    },
    field_filters   => {
        'address_residence.zipcode' => sub {
            my $val = shift;

            $val =~ s/\s//;

            return uc($val);
        }
    },
    require_some    => {
        coc_or_company_or_zipcode => [1, qw/coc_number coc_location_number address_residence.zipcode company/],
    },
    dependency_groups => {
        zipcode_group       => [qw/address_residence.zipcode address_residence.street_number/],
    }
);

sub search_companies {
    my $self            = shift;
    my $params          = shift;
    my $interface       = shift;

    ### Error when required params are missing, but do not override $params
    assert_profile($params);

    my $transaction     = $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_companies',
                request_params  => $params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

    unless (
        $transaction->processor_params &&
        $transaction->processor_params->{result}
    ) {
        throw(
            'sysin/modules/overheidio/search_companies/no_results',
            'Failure in getting results',
        );
    }

    return $transaction;
}

=head2 _process_search_companies

    $interface->process(
        {
            processor_params        => {
                processor       => '_process_search_companies',
                request_params  => { company => 'Mintlab' }
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data              => Data::Dumper::Dumper($params),
        }
    );

Actual processor for searching in other party for companies.

=cut

my $map = {
    coc_number                          => 'dossiernummer',
    coc_location_number                 => 'vestigingsnummer',
    company                             => 'handelsnaam',
    'address_residence.street'          => 'straat',
    'address_residence.zipcode'         => 'postcode',
    'address_residence.street_number'   => 'huisnummer',
};

sub _process_search_companies {
    my $self            = shift;
    my $record          = shift;

    ### Every error is by default fatal (do not retry)
    $self->process_stash->{error_fatal} = 1;

    my $transaction     = $self->process_stash->{transaction};
    my $params          = $transaction->get_processor_params()->{request_params};
    my $key             = $transaction->interface->get_interface_config->{overheid_io_key};

    my $config = $transaction->interface->get_interface_config;

    if (!$config->{overheid_io_module_kvk} && !$config->{overheid_io_module}) {
        throw('overheidio/module/kvk/inactive',
            "Unable to search for Kvk entries, module is not configured");
    }


    ### Map query company => handelsnaam, coc_number => dossiernummer etc
    my %query           = map {; $map->{ $_ } => $params->{ $_ } } grep({ $map->{$_} } keys %$params);

    $record->preview_string(
        "Zoek (kvk): " .
        join(', ', values %query)
    );


    my $search_term = delete $query{vestigingsnummer} ||
        delete $query{dossiernummer} ||
        delete $query{handelsnaam};

    if ($search_term =~ /^\d+$/) {
        $search_term = int($search_term);
    }

    return try {
        $transaction->input_data(dump_terse({ search_term => $search_term, filters => \%query }));

        my $model = WebService::OverheidIO::KvK->new(
            key  => $config->{overheid_io_key},
        );

        my $res = $model->search(
            $search_term,
            filter => \%query,
        );
        ### Map result to objects
        my $companies = $self->_map_to_companies($res);

        $record->output(dump_terse($companies, 5));

        $transaction->processor_params(
            {
                %{ $transaction->processor_params },
                result  => ($companies || []),
            }
        );
        return $record->output();

    }
    catch {
        $self->log->error($_);
        $transaction->error_fatal(1);
        $transaction->error_count(1);
        return $record->output($_);
    };

}

=head2 _map_to_companies

    $self->_map_to_companies(
        {
            '_embedded' => {
                'rechtspersoon' => [
                    {
                        '_links' => {
                            'self' => {
                                'href' => '/api/kvk/51902672/0000'
                            }
                        },
                        'dossiernummer' => '51902672',
                        'handelsnaam' => 'Mintlab B.V.',
                        'huisnummer' => '7',
                        'huisnummertoevoeging' => 'UNIT 521-522',
                        'plaats' => 'Amsterdam',
                        'postcode' => '1051JL',
                        'straat' => 'Donker Curtiusstraat',
                        'subdossiernummer' => '0000',
                        'vestigingsnummer' => '21881022'
                    },
                    {
                        '_links' => {
                            'self' => {
                                'href' => '/api/kvk/51902672/0000'
                            }
                        },
                        'dossiernummer' => '51902672',
                        'handelsnaam' => 'Mintlab B.V.',
                        'huisnummer' => '7',
                        'huisnummertoevoeging' => 'UNIT 521-522',
                        'plaats' => 'Amsterdam',
                        'postcode' => '1051JL',
                        'straat' => 'Donker Curtiusstraat',
                        'subdossiernummer' => '0000',
                        'vestigingsnummer' => '21881022'
                    }
                ]
            },
            '_links' => {
                'self' => {
                    'href' => '/api/kvk?filters%5Bdossiernummer%5D=51902672&ovio-api-key=59bc9ccb54a0125984b596fe192840ea960772e010d920a0f20b6d73c62d037c&fields[]=dossiernummer&fields[]=handelsnaam&fields[]=huisnummer&fields[]=huisnummertoevoeging&fields[]=plaats&fields[]=postcode&fields[]=straat&fields[]=vestigingsnummer'
                }
            },
            'pageCount' => 2,
            'size' => 100,
            'totalItemCount' => 2s
        };
    );

Will map the given Overheid.IO companies to objects for L<Zaaksysteem::BR::Subject>

=cut

sub _map_to_companies {
    my $self        = shift;
    my $json        = shift or return [];

    my $rechtspersonen = $json->{_embedded}->{rechtspersoon};

    return [] unless $rechtspersonen;

    my @rv;
    for my $r (@$rechtspersonen) {
        my $href    = $r->{_links}->{self}->{href};
        my ($ident) = $href =~ /(\d+\/\d+)$/;
        push(
            @rv,
            {
                subject_type    => 'company',
                subject         => {
                    company     => $r->{handelsnaam},
                    coc_number  => $r->{dossiernummer},
                    coc_location_number => $r->{vestigingsnummer},
                    address_residence => {
                        street => $r->{straat},
                        street_number => $r->{huisnummer},
                        street_number_suffix => $r->{huisnummertoevoeging},
                        city => $r->{plaats},
                        zipcode => $r->{postcode},
                        country => {
                            dutch_code => '6030',
                        }
                    }
                },
                external_subscription => {
                    external_identifier => $ident
                }
            }
        )
    }

    return \@rv;
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

