package Zaaksysteem::Backend::Sysin::Modules::Email;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use BTTW::Tools;

use Zaaksysteem::Backend::Email;
use Zaaksysteem::Backend::Mailer;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'email';

use constant INTERFACE_CONFIG_FIELDS    => [
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_how_hot',
    #     type        => 'text',
    #     label       => 'How hot is it in here',
    #     required    => 1,
    # )
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Ingeplande email',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'outgoing',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 0,
    retry_on_error                  => 1,
    trigger_definition  => {
        schedule_mail   => {
            method  => 'schedule_mail',
            #update  => 1,
        },
        delete_pending  => {
            method  => 'delete_pending',
            #update  => 1,
        },
        get_pending  => {
            method  => 'get_pending',
            #update  => 1,
        },
    },
};


has 'email_sender' => (
    is => 'rw',
    lazy => 1,
    default => sub { Zaaksysteem::Backend::Mailer->new }
);

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 schedule_mail

Queue a notification based email for later delivery.

=cut

define_profile schedule_mail => (
    required => [qw/case_id notification/],
    optional => [qw/scheduled_for zaaktype_notificatie_id/],
    typed => {
        scheduled_for => 'DateTime',
    }
);

sub schedule_mail {
    my $self                        = shift;
    my $params                      = assert_profile(shift || {})->valid;
    my $interface                   = shift;

    my $case                        = $interface
                                    ->result_source
                                    ->schema
                                    ->resultset('Zaak')
                                    ->find($params->{case_id});

    my $recipient                   = eval { $case->notification_recipient({
            recipient_type  => $params->{notification}->{recipient_type},
            behandelaar     => $params->{notification}->{behandelaar},
            email           => $params->{notification}->{email},
        });
    };

    ### Recipient only needed for "Ontvanger" below, so prevent any dies.
    if ($@) {
        print STDERR "Problem with retrieving tha ontvanger when scheduling mail: " . $@ . "\n";
    }


    my $notificatie                 = $interface
                                    ->result_source
                                    ->schema
                                    ->resultset('BibliotheekNotificaties')
                                    ->find(
                                        $params->{notification}->{id}
                                    );

    my $description                 =
        "\nZaak ID      : " . $params->{case_id} .
        "\nOntvanger    : " . ($recipient || '') .
        "\nBerichtlabel : " . $notificatie->label .
        "\n\n==============" .
        "\nSubject    : " . $notificatie->subject .
        "\nBericht:\n" . $notificatie->message;

    my $transaction = $interface->process(
        {
            processor_params        => {
                processor       => '_process_mail',
                notification    => $params->{notification},
                case_id         => $params->{case_id},
                label           => $notificatie->label,
                zaaktype_notificatie_id => $params->{zaaktype_notificatie_id}
            },
            external_transaction_id => 'mail-case-' . $case->id,
            input_data              => $description,
            direct                  => 0,
            schedule                => $params->{scheduled_for},
        }
    );
}

sub delete_pending {
    my $self                        = shift;

    my $transactions                = $self->get_pending(@_);

    while (my $transaction = $transactions->next) {
        $transaction->error_fatal(1);
        $transaction->input_data('Rescheduled');
        $transaction->date_next_retry(DateTime->now());
        $transaction->error_count(1);
        $transaction->update;
    }

    return 1;
}

Params::Profile->register_profile(
    method      => 'get_pending',
    profile     => {
        required        => [qw/case_id/],
    }
);

sub get_pending {
    my $self                        = shift;
    my $params                      = assert_profile(shift || {})->valid;
    my $interface                   = shift;

    my $transactions                = $interface->transactions->search(
        {
            date_next_retry         => { '>' => DateTime->now() },
            external_transaction_id => 'mail-case-' . $params->{case_id},
        }
    );

    return $transactions;
}

=head2 _process_mail

Send a queued email.

=cut

sub _process_mail {
    my $self                        = shift;
    my ($record, $params)           = @_;

    my $schema = $record->result_source->schema;
    my $case   = $schema->resultset('Zaak')->find($params->{case_id});

    my $notification = $schema->resultset('BibliotheekNotificaties')->find(
        $params->{notification}->{id}
    );

    my ($body, $recipient);
    eval {
        $recipient = $case->notification_recipient({
            recipient_type  => $params->{notification}->{recipient_type},
            behandelaar     => $params->{notification}->{behandelaar},
            email           => $params->{notification}->{email},
        }) or throw('interface/email/destination/unknown', 'no recipients');

        $body = $case->mailer->send_case_notification({
            recipient    => $recipient,
            notification => $notification,
            attachments  => $self->get_attachment_ids($schema, $params->{zaaktype_notificatie_id})
        });
    };

    if ($@ && $@ =~ /no recipients/) {
        throw(
            'sysin/modules/email/mailer_problem',
            "\n\nE-mail niet verstuurd: e-mailadres is niet opgegeven door aanvrager of behandelaar.",
            {
                fatal => 1
            }
        );
    } elsif ($@) {
        throw(
            'sysin/modules/email/mailer_problem',
            "Problem bij het mailen van ingeplande e-mail: $@",
            {
                fatal => 1
            }
        );
    }

    $record->output(
        "Zaak ID      : " . $params->{case_id} .
        "\nOntvanger    : " . ($recipient || '') .
        "\nBerichtlabel : " . $notification->label .
        "\n\n==============" .
        "\nSubject    : " . $notification->subject .
        "\nBericht:\n" . $notification->message
    );

}

=head2 get_attachment_ids

Get the zaaktype_kenmerken_ids for the attributes that are selected as
attachments and have files in them.

=cut

sub get_attachment_ids {
    my ($self, $schema, $zaaktype_notificatie_id) = @_;

    return [] unless $zaaktype_notificatie_id;

    my $zaaktype_notificatie = $schema->resultset('ZaaktypeNotificatie')->find(
        $zaaktype_notificatie_id
    ) or throw('sysin/email/zaaktype_notificatie_id_not_found',
        'Kon zaaktype email instellingen niet vinden');

    return [
        map { $_->{case_document_ids } }
        grep { $_->{selected} }
        @{ $zaaktype_notificatie->case_document_attachments }
    ];
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 delete_pending

TODO: Fix the POD

=cut

=head2 get_pending

TODO: Fix the POD

=cut

=head2 schedule_mail

TODO: Fix the POD

=cut

