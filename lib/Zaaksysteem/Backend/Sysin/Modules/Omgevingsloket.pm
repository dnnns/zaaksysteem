package Zaaksysteem::Backend::Sysin::Modules::Omgevingsloket;

use Moose;
use Net::FTPSSL;
use File::Spec::Functions qw/splitpath catfile/;
use List::MoreUtils qw/all/;

use DateTime::Format::Strptime qw(strptime);
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;
use BTTW::Tools;
use Zaaksysteem::XML::Compile;
use Zaaksysteem::Object::Model;

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head2 ATTRIBUTES

These are the relevant Omgevingsloket payload fields that need
to be stored in a newly created case for every request.

=cut

use constant ATTRIBUTES => qw/
    aanvraagdatum
    aanvraagNaam
    aanvraagnummer
    aanvraagProcedure
    aanvraagStatus
    bijlagen
    gedeeltelijkGoedkeurenGewenst
    gefaseerdIndienen
    Geslachtsaanduiding
    Geslachtsnaam
    Handelsnaam
    omschrijvingNietInTeDienenBijlagen
    omschrijvingUitgesteldeBijlagen
    persoonsgegevensVrijgeven
    plichtStatus
    projectKosten
    projectOmschrijving
    referentieCodeAanvrager
    sub.emailadres
    sub.telefoonnummer
    toelichtingBijIndiening
    uitgesteldeAanleveringBijlagen
    Verblijfsadres
    Vestigingsnummer
    Voorletters
    gor.straatnaam
    aoa.postcode
    aoa.huisnummer
    wpl.woonplaatsNaam
/;

=head2 interface

Convenience method

=cut

has interface => (is => 'rw');

=head2 supported_lvo

Convenience lookup attribute

=cut

has supported_lvo => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    default => sub {
        return {
            AanbiedenAanvraag  => '_process_aanvraag_row',
            IndienenAanvulling => '_process_aanvulling_row'
        };
    },
);

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_host',
        type        => 'text',
        label       => 'FTP host',
        required    => 1,
        description => 'FTP server voor bijlagen'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_username',
        type        => 'text',
        label       => 'Gebruikersnaam',
        required    => 1,
        description => 'Gebruikersnaam'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password',
        type        => 'password',
        label       => 'Wachtwoord',
        required    => 1,
        description => 'Wachtwoord'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_fallback_bedrijf',
        type        => 'spot-enlighter',
        label       => 'Fallback bedrijf',
        required    => 1,
        description => 'Wordt gebruikt als aanvrager als het bedrijf van de aanvraag niet gevonden kan worden.',
        data => {
            restrict => 'contact/bedrijf',
            placeholder => 'Type uw zoekterm',
            label => 'handelsnaam',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_fallback_natuurlijk_persoon',
        type        => 'spot-enlighter',
        label       => 'Fallback natuurlijk persoon',
        required    => 1,
        description => 'Wordt gebruikt als aanvrager als de natuurlijk persoon van de aanvraag niet gevonden kan worden.',
        data => {
            restrict => 'contact/natuurlijk_persoon',
            placeholder => 'Type uw zoekterm',
            label => 'geslachtsnaam',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_last_sync',
        label       => 'Laatste synchronisatie',
        description => 'Datum en tijd van de laatste synchronisatie',
        type        => 'display',
        data        => { template => "<[field.value | date:'dd-MM-yyyy HH:MM']>" }
    )
];

use constant INTERFACE_ID => 'omgevingsloket';
use constant MODULE_SETTINGS => {
    name                            => INTERFACE_ID,
    label                           => 'Omgevingsloket',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['file','text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 1,
    has_attributes                  => 1,
    active_state_callback => sub {
        return shift->install_scheduled_job(@_);
    },
    attribute_list                  => [
        map {{
            external_name   => $_,
            attribute_type  => 'magic_string'
        }} (ATTRIBUTES)
    ],
    trigger_definition  => {
        aanvraag => {
            method => 'aanvraag',
            update => 1
        },
        aanvulling => {
            method => 'aanvulling',
            update => 1
        },
        broadcast_poll_event => {
            method => 'broadcast_poll_event',
            update => 1
        }
    }
};


around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head2 broadcast_poll_event

Create and broadcast a 'poll_olo_messages' event to the listening services.

=cut

define_profile broadcast_poll_event => (
    required => {
        queue_model => 'Zaaksysteem::Object::Queue::Model',
        instance_hostname => 'Str'
    }
);

sub broadcast_poll_event {
    my $self = shift;
    my $params = assert_profile(shift)->valid;
    my $interface = shift;

    my $model = $params->{ queue_model };

    my $item = $model->create_item({
        label => 'Poll OmgevingsLoketOnline microservice',
        type => 'poll_olo_messages',
        status => 'waiting',
        # Duplicates are expected in current situation, cleanup may not have
        # deleted previous item(s).
        ignore_existing => 1,
        target => 'olo',
        data => {
            parameters => {
                instance_hostname => $params->{ instance_hostname },
                interface_id => $interface->uuid,
                last_sync => $interface->get_interface_config->{ last_sync }
            }
        }
    });

    $model->queue_item($item);

    $item->delete;

    return;
}

=head2 _process_get_next_row

Iterator function for this interface type. Because this
is an xml interface that will handle single requests, a mechanism
is followed to prevent multiple iterations. (return if once; once += 1).
Control is given to this iterator function.

=cut

sub _process_get_next_row {
    my $self        = shift;

    return if $self->process_stash->{once};

    $self->process_stash->{once} = 1;

    my $transaction = $self->process_stash->{transaction};
    my $input_file = $transaction->input_file;

    # normally use input data, if none given, see if a file uploas was done.
    my $xml = $transaction->input_data || $input_file && $input_file->content;

    if ($xml) {
        my $parsed = $self->parse_xml($xml);
        return ($parsed, $xml);
    }

    return;
}

=head2 _process_selector

Indicate wether this module is able to process the given xml.

=cut

sub _process_selector {
    my ($self, $perlobject) = @_;

    my $func = $self->supported_lvo->{$perlobject->{stuurgegevens}{functie}};
    return $self->can($func) if defined $func;
    return undef;
}

=head2 parse_xml

Omgevingsloket requests come as SOAP message. Parse this SOAP message according
to the defined structure and return and perl representation.

First the schema must be loaded. Using the compiled schema the xml will interpreted.

values are stored in a decorated format like this:

    'uitgesteldeAanleveringBijlagen' => {
        '_' => 'ja',
        'exact' => 1
    }

extract_scalar retrieves the underscore member
and handles potential Math::Bigfloat types.

=cut

sub _parse_xml {
    my ($self, $xml) = @_;

    my $parsed;
    foreach (qw(aanvraag aanvulling)) {
        my $method = $_ . "_omgevingsloket";
        if ($self->stuf0312->can($method)) {
            $parsed = eval { $self->stuf0312->$method('READER', $xml) };
            return $parsed if $parsed;
        }
        else {
            throw("ZS/Omgevingsloket/Parser", "Invalid StUF message type");
        }
    }
    throw("ZS/Omgevingsloket/StUF/Invalid", "Invalid StUF LVO message");
}

sub parse_xml {
    my ($self, $xml) = @_;
    my $parsed = $self->_parse_xml($xml);

    return {
        stuurgegevens => $parsed->{stuurgegevens},

        # English term has been used to avoid conflict with Dutch 'bijlagen',
        # which is used as the attribute name.
        attachments => $self->parse_attachments($parsed),

        # on purpose, make keys available for case fields
        $self->parse_aanvraagGegevens($parsed),
    };
}

=head2 install_scheduled_job

This method hooks into the
L<Zaaksysteem::Backend::Sysin::Modules/active_state_callback> infrastructure
and manages the (de)installation of a
L<Zaaksysteem::Object::Types::ScheduledJob> instance for the C<OLOSync> job
type.

=cut

sub install_scheduled_job {
    my $module = shift;
    my $interface = shift;
    my $model = shift;

    my $iter = $model->search('scheduled_job', {
        job => 'OLOSync',
        interface_id => $interface->id
    });

    $model->delete(object => $_) for $iter->all;

    return unless $interface->active;

    $module->log->debug(sprintf(
        'Installing OLOSync scheduled job for "%s"',
        $interface->name
    ));

    my $job = Zaaksysteem::Object::Types::ScheduledJob->new(
        job => 'OLOSync',
        interval_period => 'minutes',
        interval_value => 15,
        interface_id => $interface->id,
        next_run => DateTime->now(),
        data => {
            label => sprintf(
                'Omgevingsloket Online synchronisatie taak voor koppeling "%s"',
                $interface->name
            )
        }
    );

    $model->save_object(object => $job);

    return;
}

=head2 parse_aanvraagGegevens

Returns hash

=cut

sub parse_aanvraagGegevens {
    my ($self, $parsed) = @_;

    if ($parsed->{stuurgegevens}{functie} eq 'AanbiedenAanvraag') {
        return $self->_parse_aanvraag_gegevens($parsed);
    }
    elsif ($parsed->{stuurgegevens}{functie} eq 'IndienenAanvulling') {
        return $self->_parse_aanvulling_gegevens($parsed);
    }
}

sub _parse_aanvraag_gegevens {
    my ($self, $parsed) = @_;
    my $aanvraagGegevens  = $parsed->{update}{object}{aanvraagGegevens};
    my $isAangevraagdDoor = $parsed->{update}{object}{isAangevraagdDoor};
    my $isVoor            = $parsed->{update}{object}{isVoor}{gerelateerde};

    # special treatment since this can hang up other processes
    my $aanvraagdatum = $self->stuf0312->extract_scalar(
        delete $aanvraagGegevens->{aanvraagdatum}
    );

    my %location_attributes = $self->parse_isVoor($isVoor);

    my %rv = (
        isAangevraagdDoor => $self->parse_isAangevraagdDoor($isAangevraagdDoor),
        aanvraagdatum => $self->parse_aanvraagdatum($aanvraagdatum),
        #aanvrager_type => $parsed,
        stuurgegevens => $parsed->{stuurgegevens},

        %location_attributes,

        map {
            $_ => $self->stuf0312->extract_scalar($aanvraagGegevens->{ $_ })
        } keys %$aanvraagGegevens,
    );

    return %rv;
}

sub _parse_aanvulling_gegevens {
    my ($self, $parsed) = @_;
    my $aanvraagGegevens  = $parsed->{update}{object}{aanvraagGegevens};
    return
        map { $_ => $self->stuf0312->extract_scalar($aanvraagGegevens->{$_}) }
        keys %$aanvraagGegevens
}

=head2 parse_attachments

Return list with filenames

=cut

sub parse_attachments {
    my ($self, $parsed) = @_;

    my $heeftBijlage = $parsed->{update}{object}{heeftBijlage} // [];

    return [
        map {
            $self->stuf0312->extract_scalar($_->{gerelateerde}{bestandsnaam})
        } @$heeftBijlage
    ];
}

=head2 parse_isVoor

TODO: Document me

=cut

sub parse_isVoor {
    my ($self, $isVoor) = @_;

    # This is a deep data structure, and I don't want to risk dying later on because a
    # part is missing.
    return if not exists $isVoor->{seq_isKadastraleOnroerendeZaak};
    return if not @{ $isVoor->{seq_isKadastraleOnroerendeZaak} };
    return if not exists $isVoor->{seq_isKadastraleOnroerendeZaak}[0]{isAdres};
    return if not exists $isVoor->{seq_isKadastraleOnroerendeZaak}[0]{isAdres}{gerelateerde};
    return if not exists $isVoor->{seq_isKadastraleOnroerendeZaak}[0]{isAdres}{gerelateerde}{adresAanduidingGrp};

    my $adres_grp = $isVoor->{seq_isKadastraleOnroerendeZaak}[0]{isAdres}{gerelateerde}{adresAanduidingGrp};

    my %attributes;
    for my $key (grep { /^(gor|aoa|wpl)\./} ATTRIBUTES) {
        $attributes{$key} = $self->stuf0312->extract_scalar( $adres_grp->{$key} );
    }
    return %attributes
}


=head2 parse_isAangevraagdDoor

Determine the requestor. Returns a structure with the requestor type
and id, either:

    { type => 'natuurlijk_persoon', burgerservicenummer => '12345678' }

or
    { type => 'bedrijf' , vestigingsnummer => '23434455434' }

=cut

sub parse_isAangevraagdDoor {
    my ($self, $isAangevraagdDoor) = @_;

    my ($requestor) = grep { exists $_->{cho_natuurlijkPersoon} } @{$isAangevraagdDoor->{gerelateerde}{cho_kvkNummer}};
    $requestor = $requestor->{cho_natuurlijkPersoon}[0];
    if (!$requestor) {
        throw('sysin/omgevingsloket/could_not_parse_requestor', "Initial requestor fail");
    }

    if (exists $requestor->{vestiging}) {
        my $vestigingsNummer = $requestor->{vestiging}{vestigingsNummer};

        if ($vestigingsNummer) {
            return {
                type => 'bedrijf',
                vestigingsnummer => $self->stuf0312->extract_scalar($vestigingsNummer)
            };
        }
    } elsif (exists $requestor->{natuurlijkPersoon}) {
        my $burgerservicenummer = $requestor->{natuurlijkPersoon}{'inp.bsn'};

        if ($burgerservicenummer) {
            return {
                type => 'natuurlijk_persoon',
                burgerservicenummer => $burgerservicenummer
            };
        }
    }

    # fallthrough
    throw('sysin/omgevingsloket/could_not_parse_requestor');
}

=head2 stuf0312

Convenience reference to global schema instance singleton

=cut

has stuf0312 => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $xmlcompile =  Zaaksysteem::XML::Compile->xml_compile;
        $xmlcompile->add_class("Zaaksysteem::StUF::0312::Instance");
        return $xmlcompile->stuf0312;
    }
);

=head2 parse_aanvraagdatum

Convert from '20130415' to '15-04-2013' as Zaaksysteem likes it.

=cut

sub parse_aanvraagdatum {
    my ($self, $aanvraagDatum) = @_;

    # empty is OK
    return unless $aanvraagDatum;

    # strptime will bork
    throw('sysin/omgevingsloket/aanvraagDatum/parseError',
        "Systeemfout: Incorrect datumformaat ontvangen")
            unless $aanvraagDatum =~ m/^\d{8}$/;

    return strptime('%Y%m%d', $aanvraagDatum)->strftime('%d-%m-%Y');
}

=head2 _process_aanvraag_row

Given a transaction record with an request for a permit, parse
input xml, create case, fill in values, make preview string etc..

=cut

sub _process_aanvraag_row {
    my ($self, $record, $row) = @_;

    $self->interface($self->process_stash->{transaction}->interface);

    my $case = $self->interface->create_case({
        record => $record,
        values => $row,
        requestor => $self->determine_requestor($row->{isAangevraagdDoor})
    });

    my $model_stash = $self->get_process_stash('models');

    $model_stash->{ zaak }->execute_phase_actions($case, { phase => 1 });

    $self->store_remote_files($case, @{ $row->{attachments} });
    $record->preview_string('Nieuwe aanvraag geregistreerd onder zaak ' . $case->id);

    push @{ $self->process_stash->{row}->{mutations} },
        $self->interface->register_case_attributes_as_mutations($case);
}

=head2 _process_aanvulling_row

=cut

sub _process_aanvulling_row {
    my ($self, $record, $row) = @_;

    $self->interface($self->process_stash->{transaction}->interface);

    my $lvonummer = $row->{aanvraagnummer};

    my ($mapping) = grep { $_->{external_name} eq 'aanvraagnummer' }  @{ $self->interface->get_interface_config->{attribute_mapping} };

    my $schema = $self->interface->result_source->schema;
    my $model = Zaaksysteem::Object::Model->new(schema => $schema);
    my $rs = $model->search_rs('case', { 'attribute.' . $mapping->{internal_name}{searchable_object_id} => $lvonummer });
    my $count = $rs->count;

    if ($count == 1) {
        my $case = $rs->first->get_source_object;
        $self->store_remote_files($case, @{ $row->{attachments} });
        $record->preview_string('Nieuwe aanvraag geregistreerd onder zaak ' . $case->id);
        push @{ $self->process_stash->{row}->{mutations} }, $self->interface->register_case_attributes_as_mutations($case);
    }
    else {
        if ($count) {
            my @cases;
            while (my $r = $rs->next) {
                push(@cases, $r->get_source_object->id);
            }
            throw("Omgevingsloket/aanvulling/zaak", "Omgevingsloket heeft meerdere zaken gevonden met LVO id $lvonummer: " . join(", ", @cases));
        }
        else {
            throw("Omgevingsloket/aanvulling/zaak", "Omgevingsloket kon geen zaak vinden met LVO id $lvonummer");
        }
    }

}

=head2 attachment_case_document_ids

User can optionally allocate a case attribute where attachments
should show up. If not, they will be added without being linked
to a specific attribute.

=cut

sig attachment_case_document_ids => '?Zaaksysteem::Zaken::ComponentZaak';

sub attachment_case_document_ids {
    my ($self, $case) = @_;

    my $interface = $self->interface;
    my $attributes = $interface->get_interface_config->{attribute_mapping} || [];

    my ($bijlage_kenmerk) = grep {
        $_->{external_name} eq 'bijlagen' &&
        $_->{internal_name}{searchable_object_id}
    } @$attributes;

    return unless $bijlage_kenmerk; # not mapped, no problemo.

    my $magic_string = $bijlage_kenmerk->{internal_name}{searchable_object_id};

    my $lookup = $interface->case_type_id->attributes_by_magic_string;
    my $bibliotheek_kenmerk = $lookup->{$magic_string}
        or return;

    my $node = defined $case ? $case->zaaktype_node_id
                             : $interface->case_type_id->zaaktype_node_id;

    my $zaaktype_kenmerken = $node->zaaktype_kenmerkens->search({
        bibliotheek_kenmerken_id => $bibliotheek_kenmerk->id
    });

    return [ $zaaktype_kenmerken->get_column('id')->all ];
}

=head2 store_remote_files

The xml message may contain links to remote files. The files
need to be retrieved and stored locally as case_document.

Given a list of filenames, retrieve every one and store them in file objects
linked to the case.

Result: each of the given filename has been retrieved and stored locally.
If the interface has been configured to store the file as case_documents,
that has happened, otherwise they are still part of the case.

Returns a list of file objects.

Because the end-user will have a chance to manually add the files,
don't abort the process when downloading fails, instead generate a text file
with an error description.

=cut

sub store_remote_files {
    my ($self, $case, @remote_files) = @_;

    return map {
        $self->store_remote_file($case, $_)
    } grep {
        $_ # the truthy, the whole truthy and nothing but the truthy
           # aka, filter out undef, '', 0 --- anything not worth bothering
           # the file system with
    } @remote_files;
}

=head2 store_remote_file

Single version (store_remote_files would be the whole album)

=cut

sub store_remote_file {
    my ($self, $case, $remote_file) = @_;

    my ($volume, $directories, $filename) = splitpath($remote_file);

    my $schema = $self->interface->result_source->schema;

    my $local_file = File::Temp->new();

    try {
        $self->retrieve_ftp({
            local_file => "$local_file",
            remote_file => $remote_file
        });
    } catch {
        $self->log->error("Error while retrieving OLO file using FTP: $_");
        $filename = "FOUT - " . $filename . ".txt";

        open my $file, ">", "$local_file" or throw(
            "sysin/omgevingsloket/could_not_write_file",
            "Could not write to $local_file: ". $!);

        print $file "Kon bestand '$remote_file' niet ophalen.\nFoutmelding: $_";
        close $file;
    };

    return $schema->resultset('File')->file_create({
        file_path         => $local_file->filename,
        name              => $filename,
        case_document_ids => $self->attachment_case_document_ids($case) // [],
        db_params => {
            created_by      => $case->aanvrager_object->rt_setup_identifier,
            case_id         => $case->id,
        },
    });
}

=head2 retrieve_ftp

Perform the actual ftp transaction

After returning, the retrieved remote_file will be present at 'local_file'.

Will throw an exception if unsuccesful.

=cut

define_profile retrieve_ftp => (
    required => [qw/remote_file local_file/]
);
sub retrieve_ftp {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $config = $self->interface->get_interface_config;

    throw('sysin/omgevingsloket/missing_ftp_config',
        'Systeemfout: FTP host/username/password niet compleet')
        unless all { $config->{$_} } qw/host username password/;

    my $ftps = Net::FTPSSL->new($config->{host}, Port => 21, Encryption => 'E')
        or die "Can't open " . $config->{host};

    $ftps->login($config->{username}, $config->{password})
        or die "Can't login: ", $ftps->last_message;

    # Set binary mode, so no CR LF transformation is done, and PDF files and
    # images don't break
    $ftps->binary()
        or die "Can't set binary mode: ", $ftps->last_message;

    $ftps->get($params->{remote_file}, $params->{local_file})
        or die "Can't get file: ", $ftps->last_message;

    $ftps->quit;

    throw('omgevingsloket/ftp_error', 'Bestand kon niet opgehaald worden')
        unless -e $params->{local_file};
}

=head2 determine_requestor

From the incoming xml the requestor and requestor type will be determined.
If either of these fails, there are fallbacks:

- If requestor type and requestor can be determined from xml, these are looked
  up in the local database.
- If not found, the fallback requestor of the specified type will be used instead
- If type could not be determined, natuurlijk_persoon is used (arbitrarily)

=head3 ARGUMENTS

=over

=item isAangevraagdDoor

This corresponds with the parsed version of the input xml,
as supplied by the transaction wrapper.

=back

=cut

sub determine_requestor {
    my ($self, $isAangevraagdDoor) = @_;

    my $betrokkene_type = $isAangevraagdDoor->{type}
        or throw('sysin/omgevingsloket/requestor_type_missing',
            'Betrokkene type is niet correct herkend');

    return $betrokkene_type eq 'bedrijf' ?
        $self->determine_bedrijf($isAangevraagdDoor->{vestigingsnummer}) :
        $self->determine_natuurlijk_persoon($isAangevraagdDoor->{burgerservicenummer});
}

=head2 determine_bedrijf

Lookup a bedrijf by vestigingsnummer in the local db, if not found
return the fallback.

=cut

sub determine_bedrijf {
    my ($self, $vestigingsnummer) = @_;

    # first try to look up in database
    if ($vestigingsnummer) {
        my $schema = $self->interface->result_source->schema;

        my $bedrijf = $schema->resultset('Bedrijf')->search({
            vestigingsnummer => $vestigingsnummer
        })->first;

        return 'betrokkene-bedrijf-' . $bedrijf->id
            if $bedrijf;
    }

    return $self->get_fallback_requestor('bedrijf');
}

=head2 determine_natuurlijk_persoon

Lookup a natuurlijk_persoon by burgerservicenummer in the local db, if not found
return the fallback.

=cut

sub determine_natuurlijk_persoon {
    my ($self, $burgerservicenummer) = @_;

    # first try to look up in database
    if ($burgerservicenummer) {
        my $schema = $self->interface->result_source->schema;

        my $natuurlijk_persoon = $schema->resultset('NatuurlijkPersoon')->search({
            burgerservicenummer => $burgerservicenummer
        })->first;

        return 'betrokkene-natuurlijk_persoon-' . $natuurlijk_persoon->id
            if $natuurlijk_persoon;
    }

    return $self->get_fallback_requestor('natuurlijk_persoon');
}

=head2 get_fallback_requestor

If the given requestor can't be linked to a local record,
look for a fallback requestor in the interface or bork out.

=cut

sub get_fallback_requestor {
    my ($self, $type) = @_;

    throw('sysin/omgevingsloket/wrong_requestor_type',
        'Aanvrager mag alleen bedrijf of natuurlijk_persoon zijn')
        unless $type eq 'bedrijf' || $type eq 'natuurlijk_persoon';

    # otherwise use the fallback setting
    my $config = $self->interface->get_interface_config;

    my $fallback_id = $config->{ 'fallback_' . $type }{id}
        or throw('sysin/omgevingsloket/fallback_' . $type . '_missing',
            "Het veld 'Fallback $type' is niet ingevuld");

    return "betrokkene-$type-$fallback_id";
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

