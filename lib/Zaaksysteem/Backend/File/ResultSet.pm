package Zaaksysteem::Backend::File::ResultSet;

use Moose;

use File::Basename;
use Params::Profile;

use Zaaksysteem::BR::Subject;
use Zaaksysteem::Constants qw(MIMETYPES_ALLOWED DOCUMENT_STATUS);

use BTTW::Tools;
use BTTW::Tools::File qw(sanitize_filename);

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::File::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::File::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::File::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

with 'MooseX::Log::Log4perl';

=head2 generate_file_id

Retrieve a new, unused file id, without creating a file row (used by StUF-ZKN).

=cut

sub generate_file_id {
    my $self = shift;

    return $self->result_source->schema->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;

            my $sth = $dbh->prepare(q{
                SELECT nextval('file_id_seq');
            });
            $sth->execute();

            my @result = $sth->fetchrow_array;
            return $result[0];
        }
    );
}

=head2 file_create

Creates a File entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added.

=item name [required]

The file's name.

=item db_params [required]

Any parameter used in the file table. These are common/required:
 * created_by [required]   - the subject that is creating this entry
 * filestore_id [optional] - to not create a new filestore entry, use existing
 * case_id [optional]      - not setting this will let the file show up in the intake

=item metadata [optional]

Set any metadata at creation right away. See file_metadata table for columns.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=item publish_pip [optional]

Boolean, decides whether this file gets shown on the PIP or not.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=back

=head3 Returns

The newly created File object.

=cut

Params::Profile->register_profile(
    method  => 'file_create',
    profile => {
        optional => [qw/
            case_id
            created_by
            filestore_id
            publish_website
            publish_pip
            intake_owner
            document_status
            disable_message
        /],
        constraint_methods => {
            document_status => DOCUMENT_STATUS,
        },
    }
);

sub file_create {
    my $self = shift;
    my $opts = $_[0];

    my $db_params = $opts->{db_params};
    my $file_path = $opts->{file_path};
    my $name      = $opts->{name};

    # Check database parameters
    my $dv = Params::Profile->check(
        params  => $db_params,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw(
            '/file/file_create/invalid_parameters',
            "Invalid options given: @invalid",
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw(
            '/file/file_create/missing_parameters',
            "Missing options: @missing",
        );
    }

    my $schema = $self->result_source->schema;
    $schema->txn_do(sub {
        # Default to file created by currently logged in user (if there is one).
        unless ($db_params->{ created_by }) {
            $db_params->{ created_by } = $schema->current_user
                ? $schema->current_user->betrokkene_identifier
                : '';
        }

        # ignore_extension may only be used to ignore an invalid extension, File
        # entries -always- need to have an extension set.
        if ($opts->{ignore_extension} && $opts->{name} !~ /\./) {
            throw(
                '/file/file_create/ignore_extension/has_no_extension',
                sprintf(
                    "ignore_extension was set, but %s has no extension at all", $opts->{name}
                ),
            );
        }

        # case_document_ids is expected to always be a list
        if ($opts->{case_document_ids} && !UNIVERSAL::isa($opts->{case_document_ids}, 'ARRAY')) {
            throw(
                '/file/file_create/not_an_array',
                "case_document_ids can only be set as an array",
            );
        }

        # If the intake_owner isn't being set, attempt to extract a username from the filename.
        if (!$db_params->{case_id} && !$db_params->{intake_owner}) {
            my $extracted_data = $self->extract_username($opts->{name});

            if ($extracted_data) {
                $opts->{name}              = $extracted_data->{filename};
                $db_params->{intake_owner} = $extracted_data->{subject};
                $db_params->{created_by}   = $extracted_data->{subject} unless $db_params->{ created_by };
            }
        }

        my $file_properties     = get_file_properties(sanitize_filename($opts->{name}));
        $db_params->{name}      = $file_properties->{name};
        $db_params->{extension} = $file_properties->{ext};

        if (!$db_params->{filestore_id}) {
            my $fs = $schema->resultset('Filestore')->filestore_create({
                file_path        => $file_path,
                original_name    => sanitize_filename($opts->{name}),
            });
            $db_params->{filestore_id} = $fs->id;
        }

        my $case;
        if (defined $db_params->{case_id}) {
            $case = $schema->resultset('Zaak')->find($db_params->{case_id});
        }

        my $duplicate_version = $self->name_exists(
            $file_properties->{name}, $file_properties->{ext}, $db_params->{case_id}, $db_params->{directory_id}
        );
        if ($duplicate_version) {
            $self->log->trace(sprintf(
                "Duplicate name detected. Duplicate of: '%s'",
                $duplicate_version->root_file_id // $duplicate_version->id
            ));
            $db_params->{is_duplicate_name} = 1;
            $db_params->{is_duplicate_of}   = $duplicate_version->root_file_id // $duplicate_version->id;
        }

        if ($case && $case->behandelaar) {
            my $case_subject = sprintf "betrokkene-%s-%s", (
                $case->behandelaar->betrokkene_type,
                $case->behandelaar->betrokkene_id,
            );
            if ($db_params->{created_by} eq $case_subject) {
                $self->log->trace("created_by matches behandelaar ($case_subject) - set file to 'accepted'");
                $db_params->{accepted} = 1;
            }
        }

        $db_params->{active_version}    = 1;

        if ($db_params->{accepted}) {
            if ($db_params->{is_duplicate_name} && $case->is_in_phase('afhandel_fase')) {
                $db_params->{is_duplicate_name} = 0;
                $db_params->{is_duplicate_of}   = undef;

                $db_params->{name} = $self->get_valid_filename(
                    $file_properties->{name},
                    $file_properties->{ext},
                    $db_params->{case_id},
                );
            }
        }

        if ($db_params->{is_duplicate_name}) {
            $self->log->trace("Not accepting file, duplicate file name");
            $db_params->{accepted} = 0;
        }

        # Case type document handling
        if ($opts->{case_document_ids} && $opts->{case_document_clear_old}) {
            for my $case_document_id (@{$opts->{case_document_ids}}) {
                # Clear any existing files with this CTD/case if the flag for it is set.
                my $existing = $schema->resultset('FileCaseDocument')->search({
                    case_document_id => $case_document_id,
                })->delete;
            }
        }

        # If a document is being created without a case or subject_id set, it is safe
        # to assume it should be returned to the queue when rejected.
        if (!$db_params->{case_id} && !$db_params->{subject_id}) {
            $db_params->{reject_to_queue} = 1;
        }

        # Set modified
        $db_params->{date_modified} = DateTime->now;
        $db_params->{modified_by}   = $db_params->{created_by};

        # Set creation reason
        $db_params->{creation_reason} = sprintf("%s toegevoegd", $opts->{name});


        # Set search term
        $db_params->{search_term} = $db_params->{name};

        my $create = $self->create($db_params);

        $create->trigger('create',
            {
                reason => $db_params->{ creation_reason },
                disable_message => $opts->{disable_message},
            }
        );

        # Add case documents
        if (exists $opts->{case_document_ids}) {
            $create->set_or_replace_case_documents(@{$opts->{case_document_ids}});
        }

        # Add metadata
        if (exists $opts->{metadata}) {
            $create->update_metadata($opts->{metadata});
        }

        # Set case type document defaults
        if (exists $opts->{case_document_ids} && @{$opts->{case_document_ids}} == 1) {
            $create->apply_case_document_defaults;
        }

        my $baby = $create->discard_changes;

        if ($db_params->{accepted}) {
            my $bridge = Zaaksysteem::BR::Subject->new(
                schema => $schema,
            );

            $db_params->{subject} = try {
                $bridge->find_by_old_subject_identifier($db_params->{created_by});
            } catch {
                # Invalid identifier. Ignore.
                return "";
            };
            $baby->handle_accepted({properties => $db_params});
        }

        return $baby;
    });
}

=head2 $self->name_exists($name, $case_id)

Returns a valid filename to prevent duplicates.

Example: file.txt already exists for a given case, it then appends (1) to the
name. Every future occurence results in a +1 in this 'version'.

=cut

sub name_exists {
    my $self = shift;
    my ($name, $ext, $case_id, $directory_id) = @_;

    my $files = $self->search({
        name         => $name,
        extension    => $ext,
        case_id      => $case_id,
        date_deleted => undef,
        directory_id => $directory_id,
        accepted     => 1,
    });

    return unless $files->count;

    my $last_version = $files->first->get_last_version;

    return $last_version;
}

=head2 get_file_properties

Get the extension and the name (without extension) fromt the file

=cut

sub get_file_properties {
    my ($filename) = @_;

    my ($name, undef, $ext) = fileparse($filename, '\.[^\.]*');

    return  {
        name => $name || '',
        ext  => $ext
    };
}

=head2 $self->search_by_case_document_id($casetype_property_id)

Returns the resultset of files belonging to this property_id

=cut

sub search_by_case_document_id {
    my ($self, $case_type_property_id) = @_;

     return $self->search(
        {
            'case_documents.case_document_id'   => {
                -in => $case_type_property_id->get_column('id')->as_query
            },
            'me.destroyed'                      => 0,
            'me.active_version'                 => 1,
            'date_deleted'                      => undef,
        },
        {
            join => {case_documents =>'file_id'},
        }
    );
}


=head2 $self->get_valid_filename($name, $extension, $case_id)

Returns a valid filename to prevent duplicates.

Example: file.txt already exists for a given case, it then appends (1) to the
name. Every future occurence results in a +1 in this version suffix.

=cut

sub get_valid_filename {
    my $self = shift;
    my ($name, $extension, $case_id) = @_;

    my $files = $self->search({
        name      => { '~*' => $name.'(\ \(\d+\))?' },
        case_id   => $case_id,
        extension => $extension,
        accepted  => 1,
    });

    # No existing files found, return the name as is
    if ($files->count == 0) {
        return $name;
    }

    # Suffix in this context means the 'version' a file has.
    my $suffix;
    while (my $f = $files->next) {
        # Extract the suffix from the current file
        my ($f_suffix) = ($f->name =~ m/.*\((\d+)\)/);

        # If the current suffix + 1 is greater than the highest registered, it becomes leading.
        if ($f_suffix && $f_suffix+1 > $suffix) {
            $suffix = $f_suffix+1;
        }
        # First file with a suffix
        elsif (!$f_suffix && !$suffix) {
            $suffix = 1;
        }
    }
    return sprintf "%s (%d)", ($name, $suffix);
}

=head2 unaccepted

Shows an alert in the frontend that there are queued documents waiting.

=cut

sub unaccepted {
    my $self    = shift;

    return $self->search(
        {
            date_deleted => undef,
            accepted     => 0,
            destroyed    => { '!=' => 1 }
        }
    )->count;
}

=head2 extract_username

Attempts to find a username in the filename based on a default username+prefix symbol.

Returns the subject (betrokkene_identifier) and filename without the username in it.

=cut

sub extract_username {
    my ($self, $filename) = @_;
    my ($seperator) = $self->result_source->schema->resultset('Config')->get('file_username_seperator');
    $seperator = '-' if !length($seperator);

    my $usermatch_pattern = qr/(?<username>\w+)\Q$seperator\E/;
    my ($potential_user)  = ($filename =~ m/$usermatch_pattern/);
    if (!$potential_user) {
        $self->log->debug("No user $usermatch_pattern found in $filename");
        return;
    }

    my $rs = $self->result_source->schema->resultset('Subject')->search_active(
        { username => $potential_user },
    );
    my $subject = $rs->first;
    if (!$subject) {
        $self->log->debug("$potential_user is not active");
        return;
    }

    $filename =~ s/$usermatch_pattern//;

    return {
        username => $potential_user,
        subject  => $subject->betrokkene_identifier,
        filename => $filename,
    }
}



=head2 remove_unaccepted_case_documents

Given a zaaktype_kenmerken_id, delete all files that have been stored
in this kenmerk that have not been approved yet.
This implements 'Wis nieuwe bestanden' on a document kenmerk.

We have different naming conventions floating around. I'm gonna stick to
'zaaktype_kenmerken_id' instead of 'attribute_id',
because the referenced table is still called 'zaaktype_kenmerken', and there
is confusion between zaaktype_kenmerken_id and bibliotheek_kenmerken_id

=cut

define_profile remove_unaccepted_case_documents => (
    required => [qw[zaaktype_kenmerken_id]],
);
sub remove_unaccepted_case_documents {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $zaaktype_kenmerken_id = $arguments->{zaaktype_kenmerken_id}
        or die "need zaaktype_kenmerken_id";

    # first make a selection
    my $rs = $self->search({
        'case_documents.case_document_id' => $zaaktype_kenmerken_id,
        accepted => 0
    }, {
        join => { case_documents => 'file_id' }
    });

    my $file_ids = [map { $_->id } $rs->all];

    my $schema = $self->result_source->schema;

    # remove the case_document entries first
    $schema->resultset('FileCaseDocument')->search({
        file_id => $file_ids,
        case_document_id => $zaaktype_kenmerken_id
    })->delete_all;

    # now the references are cleared, remove the actual files
    $self->search({id => $file_ids})->update({
        date_deleted => 'now()',
        destroyed => 't'
    });
}


sub active {
    my ($self) = @_;

    return $self->search({
        destroyed => 0,
        date_deleted => undef,
    });
}

=head2 unaccepted_case_documents

When display a document kenmerk, we'd like to know if any of it's
files are still unaccepted.

=cut

define_profile unaccepted_case_documents => (
    required => [qw[zaaktype_kenmerken_id]],
);
sub unaccepted_case_documents {
    my $self = shift;
    my $arguments = assert_profile(shift)->valid;

    my $zaaktype_kenmerken_id = $arguments->{zaaktype_kenmerken_id}
        or die "need zaaktype_kenmerken_id";

    # first make a selection
    return $self->search({
        'case_documents.case_document_id' => $zaaktype_kenmerken_id,
        accepted => 0
    }, {
        join => { case_documents => 'file_id' }
    });
}


=head2 active_case_files

Select relevant files for a case. Default selection for display.

=cut

sub active_files {
    my $self = shift;

    my @search = $self->active_rs;

    return @search;
}

=head2 document_list

Arguments: none

Return value: L<DBIx::Class::ResultSet>

    my $rs = $file_rs->document_list();

Returns all documents that should be visible via the document tab in a case.

=cut

sub document_list {
    my $self    = shift;

    return $self->search(
        {
            'me.destroyed'      => 0,
            '-or'   => [
                {
                    'me.accepted'       => 1,
                    'me.active_version' => 1,
                },
                {
                    'me.accepted'       => 0,
                    'me.date_deleted'   => undef
                }
            ]
        },
    )->with_related_data;
}

=head2 active_rs

Arguments: none

Return value: L<DBIx::Class::ResultSet>

    my $rs = $file_rs->active_rs({ name => 'klaymen' });

Returns the active files, which means, return every file which is not destroyed and is not
an old version of another file

=cut

sub active_rs {
    my $self    = shift;

    return $self->search(
        {
            'me.destroyed'      => 0,
            'me.active_version' => 1,
            '-or'   => [
                {
                    'me.accepted'       => 1,
                },
                {
                    'me.accepted'       => 0,
                    'me.date_deleted'   => undef
                }
            ]
        },
    )->with_related_data;
}

=head2 with_related_data

Get all the correct properties of a file by one query.
The default is to retrieve all the data in case context.

=head3 SYNOPSIS

    $schema->resultset('File')->search({})->with_related_data(case_context => 0|1);

=cut

sub with_related_data {
    my ($self, %options) = @_;
    $options{case_context} //= 1;

    my $search_options;
    my $betrokkene_queries      = [
        \"CASE WHEN me.created_by ~ 'medewerker' THEN (select properties from subject where subject.id = regexp_replace(me.created_by, 'betrokkene-medewerker-','')::integer ) ELSE '' END",
        \"CASE WHEN me.modified_by ~ 'medewerker' THEN (select properties from subject where subject.id = regexp_replace(me.modified_by, 'betrokkene-medewerker-','')::integer ) ELSE '' END",
        \"CASE WHEN me.deleted_by ~ 'medewerker' THEN (select properties from subject where subject.id = regexp_replace(me.deleted_by, 'betrokkene-medewerker-','')::integer ) ELSE '' END",
    ];

    if ($options{case_context}) {
        my $user_id = $self->result_source->schema->current_user;
        my $case_documents_query    = '
            array(
                select case_document_id from
                    file_case_document
                where file_case_document.file_id = me.id
            )';

        my $annotation_count_query  = '
            (
                select count(id) from file_annotation where file_id = me.id' .
                    (
                        $self->result_source->schema->resultset('Config')->get('pdf_annotations_public')
                            ? ''
                            : ($user_id ? ' and subject = \'betrokkene-medewerker-' . $user_id->uidnumber . '\'' : '')
                    ) .
            ')';

        $search_options = {
                prefetch    => [
                    { case_id => "zaaktype_node_id" },
                    { case_id => "zaaktype_id" },
                    { case_id => "aanvrager" },
                    { case_id => "coordinator" },
                    { case_id => "behandelaar" },
                    'filestore_id',
                    'root_file_id',
                    'metadata_id',
                    'directory_id'
                ],
                '+select' => [
                    \$case_documents_query,
                    \$annotation_count_query,
                    @$betrokkene_queries
                ],
                '+as' => [
                    'case_document_id_list',
                    'annotation_count',
                    'created_by_properties',
                    'modified_by_properties',
                    'deleted_by_properties'
                ],
        };
    }
    else {
        $search_options = {
                prefetch    => [
                    'filestore_id',
                    'root_file_id',
                    'metadata_id',
                    'directory_id'
                ],
                '+select' => $betrokkene_queries,
                '+as' => [
                    'created_by_properties',
                    'modified_by_properties',
                    'deleted_by_properties'
                ],
        };
    }

    return $self->search({}, $search_options);
}

=head2 search_active

Search active files, optional with the file ID.

=cut

sub search_active {
    my ($self, $ids) = @_;

    my $args = {
        'me.destroyed'      => 0,
        'me.active_version' => 1,
        '-or'               => [
            { 'me.accepted' => 1, },
            {
                'me.accepted'     => 0,
                'me.date_deleted' => undef
            }
        ]
    };
    if (ref $ids eq 'ARRAY' && @$ids) {
        $args = {
            'me.id' => $ids,
            '-and'  => { %$args },
        };
    }

    return $self->search_rs($args);
}

=head2 cleanup_file_locks

Perform garbage collection on stale file locks, cleanup every lock which has
expired in bulk.

=cut

sub cleanup_file_locks {
    my $self = shift;

    my $stale_locks = $self->search({
        lock_timestamp => { '<' => \"NOW() AT TIME ZONE 'UTC'" }
    });

    return $stale_locks->update({
        lock_timestamp => undef,
        lock_subject_id => undef,
        lock_subject_name => undef
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 active

TODO: Fix the POD

=cut

=head2 active_files

TODO: Fix the POD

=cut

=head2 get_file_properties

TODO: Fix the POD

=cut


