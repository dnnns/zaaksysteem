package Zaaksysteem::Backend::Session::Invitation::ResultSet;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Session::Invitation::ResultSet - Session::Invitation
DBIx::Class::ResultSet behaviors

=head1 DESCRIPTION

=cut

use Zaaksysteem::Search::ObjectQueryResultSetShim;
use Zaaksysteem::Object::Types::Session::Invitation;
use Zaaksysteem::Object::Syntax;

=head1 METHODS

=head2 store

Instantiates a new L<Zaaksysteem::Object::Query> shim.

=cut

sub store {
    my $self = shift;

    return Zaaksysteem::Search::ObjectQueryResultSetShim->new(
        resultset => $self,
        type => 'session_invitation',
        object_type => 'Zaaksysteem::Object::Types::Session::Invitation',
        inflator => sub {
            my $entry = shift;

            my $subject = Zaaksysteem::Object::Reference::Instance->new(
                type => 'subject',
                id => $entry->get_column('subject_id')
            );

            my %args = (
                id => $entry->id,
                token => $entry->token,
                subject => $subject,
                date_expires => $entry->date_expires->clone,
                date_created => $entry->date_expires->clone,
            );

            my $object_id = $entry->get_column('object_id');
            my $object_type = $entry->get_column('object_type');

            if (defined $object_id && defined $object_type) {
                $args{ object } = Zaaksysteem::Object::Reference::Instance->new(
                    type => $object_type,
                    id => $object_id
                );
            }

            my $action_path = $entry->action_path;

            if (defined $action_path) {
                $args{ action_path } = $action_path;
            }

            return Zaaksysteem::Object::Types::Session::Invitation->new(%args);
        },
        deflator => sub {
            my $object = shift;

            my $entry = {
                subject_id => $object->subject->id,
                token => $object->token,
                action_path => $object->action_path,
                date_expires => $object->date_expires->clone->set_time_zone('UTC')->iso8601,
                date_created => $object->date_created->clone->set_time_zone('UTC')->iso8601
            };

            if ($object->has_object) {
                $entry->{ object_id } = $object->object->id;
                $entry->{ object_type } = $object->object->type;
            }

            return $entry;
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
