use utf8;
package Zaaksysteem::Schema::ZaakOnafgerond;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaakOnafgerond

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaak_onafgerond>

=cut

__PACKAGE__->table("zaak_onafgerond");

=head1 ACCESSORS

=head2 zaaktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 betrokkene

  data_type: 'char'
  is_nullable: 0
  size: 50

=head2 json_string

  data_type: 'text'
  is_nullable: 0

=head2 afronden

  data_type: 'boolean'
  is_nullable: 1

=head2 create_unixtime

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "zaaktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "betrokkene",
  { data_type => "char", is_nullable => 0, size => 50 },
  "json_string",
  { data_type => "text", is_nullable => 0 },
  "afronden",
  { data_type => "boolean", is_nullable => 1 },
  "create_unixtime",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</zaaktype_id>

=item * L</betrokkene>

=back

=cut

__PACKAGE__->set_primary_key("zaaktype_id", "betrokkene");

=head1 RELATIONS

=head2 zaaktype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "zaaktype_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:45
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2b42Rrpk2q3vA3sz7FFfDg





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

