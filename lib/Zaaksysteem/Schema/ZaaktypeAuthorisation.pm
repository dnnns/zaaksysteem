use utf8;
package Zaaksysteem::Schema::ZaaktypeAuthorisation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeAuthorisation

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_authorisation>

=cut

__PACKAGE__->table("zaaktype_authorisation");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_authorisation_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 recht

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 role_id

  data_type: 'integer'
  is_nullable: 1

=head2 ou_id

  data_type: 'integer'
  is_nullable: 1

=head2 zaaktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 confidential

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_authorisation_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "recht",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "role_id",
  { data_type => "integer", is_nullable => 1 },
  "ou_id",
  { data_type => "integer", is_nullable => 1 },
  "zaaktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "confidential",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 zaaktype_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaaktype>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_id",
  "Zaaksysteem::Schema::Zaaktype",
  { id => "zaaktype_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wsJIhpNdWj9NPNojoomW4A

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeAuthorisation');

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::ZaaktypeAuthorisation',
    __PACKAGE__->load_components()
);




# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

