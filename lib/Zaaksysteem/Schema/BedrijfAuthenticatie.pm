use utf8;
package Zaaksysteem::Schema::BedrijfAuthenticatie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BedrijfAuthenticatie

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bedrijf_authenticatie>

=cut

__PACKAGE__->table("bedrijf_authenticatie");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'bedrijf_authenticatie_id_seq'

=head2 gegevens_magazijn_id

  data_type: 'integer'
  is_nullable: 1

=head2 login

  data_type: 'integer'
  is_nullable: 1

=head2 password

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "bedrijf_authenticatie_id_seq",
  },
  "gegevens_magazijn_id",
  { data_type => "integer", is_nullable => 1 },
  "login",
  { data_type => "integer", is_nullable => 1 },
  "password",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jXuoHyJjTkh7wDR8gFZiJQ

__PACKAGE__->add_columns('last_modified',
    { %{ __PACKAGE__->column_info('last_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});



# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

