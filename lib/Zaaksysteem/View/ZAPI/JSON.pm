package Zaaksysteem::View::ZAPI::JSON;

use Moose;
use JSON::XS (); # Empty list to prevent import of JSON::XS::encode_json

use Zaaksysteem::ZAPI::Response;
use Zaaksysteem::ZAPI::Error;

use Data::Dumper;

BEGIN { extends 'Catalyst::View::JSON'; }

__PACKAGE__->config(
    expose_stash    => 'zapi',
);


# after http://www.gossamer-threads.com/lists/catalyst/users/29706
after 'process' => sub {
    my ($self, $c) = @_;

    if (my $content_type = $c->stash->{ json_content_type }) {
        $c->res->content_type($content_type);
    }
};


sub encode_json {
    my($self, $c, $data) = @_;

    my $encoder = JSON::XS->new->utf8->canonical->pretty->allow_nonref->allow_blessed->convert_blessed;

    ### We do NOT want to keep these things in memory, we never call resultset->all,
    ### but use a memory efficient NEXT method
    $data->{result} = $self->_prepare_results($data);

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub { shift->iso8601 . 'Z' };

    return $encoder->encode($data);
}

sub _prepare_results {
    my $self = shift;
    my $data = shift;

    if (   ref $data eq 'HASH'
        && exists $data->{result}
        && (   eval { $data->{result}->isa('DBIx::Class::ResultSet') }
            || eval { $data->{result}->isa('Zaaksysteem::Object::Iterator') }
           )
        &&
        !$data->{result}->can('TO_JSON')
    ) {
        ### Let's generate one...
        my $result = $data->{result};
        $result->reset;

        my @rows;
        while (defined(my $entry = $result->next)) {
            push(@rows, $entry->TO_JSON);
        }

        return \@rows;
    }

    return $data->{result};
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 encode_json

TODO: Fix the POD

=cut

