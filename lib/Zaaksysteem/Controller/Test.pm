package Zaaksysteem::Controller::Test;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub base : Chained('/') : PathPart('test') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    unless($c->debug) {
        throw('test/mode', 'Unable to enter testmode in non-test environment');
    }
}

sub upload_global : Chained('base') : PathPart('upload_global') : Args(0) {
    my ($self, $c) = @_;

    unless (lc($c->req->method) eq 'post') {
        $c->stash->{ template } = 'test/upload_global.tt';
        $c->detach;
    }

    my $file = $c->req->upload('file');

    unless ($file) {
        throw('test/upload_global', 'Failed to get file, did you upload?');
    }

    $c->model('DB::File')->file_create({
        db_params => {
        },
        name => $file->filename,
        file_path => $file->tempname
    });

    $c->res->body('Done. <a href="">Reload</a>?');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 upload_global

TODO: Fix the POD

=cut
