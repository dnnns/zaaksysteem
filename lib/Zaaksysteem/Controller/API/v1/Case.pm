package Zaaksysteem::Controller::API::v1::Case;
use Moose;

use DateTime::Format::DateParse;
use DateTime;
use List::Util qw[first uniq];
use Moose::Util::TypeConstraints qw[enum union];

use BTTW::Tools;
use BTTW::Tools::HashMapper;

use Zaaksysteem::Types qw[
    Boolean
    UUID
    NonEmptyStr
    TelephoneNumber
    MobileNumber
    EmailAddress
];

use Zaaksysteem::API::v1::PreparedFileBag;
use Zaaksysteem::API::v1::Set;
use Zaaksysteem::Constants qw[
    ZAAKSYSTEEM_CONSTANTS
];
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::Object::Types::ScheduledJob;
use Zaaksysteem::Object::Types::Serial;
use Zaaksysteem::Search::ESQuery;
use Zaaksysteem::Search::ZQL;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern/] }
);

has 'api_control_module_types' => (
    is          => 'rw',
    default     => sub { ['api', 'app'] },
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Case - APIv1 controller for case objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/base') : PathPart('case') : CaptureArgs(0) : Scope('case') {
    my ($self, $c) = @_;
    my $additional_search_params;

    if ($c->req->params->{ es_query }) {
        my %query = map { $_ => $c->req->params->{ $_ } } grep {
            $_ =~ m[^query\:]
        } keys %{ $c->req->params };

        unless (scalar keys %query) {
            throw('api/v1/case/es_query_fault', sprintf(
                'Please specify a ElasticSearch query using URL mapped JSON with the "query:" root'
            ));
        }

        $c->stash->{ es_query } = Zaaksysteem::Search::ESQuery->new(
            query => inflate(\%query, nesting_separator => ':'),
            object_type => 'case'
        );
    } elsif ($c->req->params->{ zql }) {
        $c->stash->{ zql } = Zaaksysteem::Search::ZQL->new(
            $c->req->params->{ zql }
        );
    } else {
        $c->stash->{ zql } = Zaaksysteem::Search::ZQL->new(
            'SELECT {} FROM case'
        );
    }

    if ($c->stash->{ zql } && $c->stash->{zql}->cmd->from->value ne 'case') {
        throw('api/v1/case/query_fault', sprintf(
            'Cannot parse ZQL, only objecttype "case" is allowed.'
        ));
    }

    my $interface = $c->stash->{ interface };
    my $model = $c->model('Object');
    my $base_rs = $c->stash->{ case_base_rs } || $model->rs;

    my $query_constraint = eval { $interface->jpath('$.query_constraint') };

    # Applies a base query constraint to the base_rs, if one is configured.
    if ($query_constraint) {
        # Ignore user permissions for finding the query constraint
        my $query = $model->inflate_from_row(
            $model->new_resultset->find($query_constraint->{ id })
        );

        unless (defined $query) {
            throw('api/v1/case/query_constraint_not_found', sprintf(
                'This API is configured with a constraint query, but the query object could not be found'
            ));
        }

        $base_rs = $query->zql->apply_to_resultset($base_rs);
    }

    my $set = try {
        my $iterator;

        if ($c->stash->{ zql }) {
            my $rs = $c->controller('API::Object')->_search_intake(
                $c,
                $c->stash->{ zql },
                $c->stash->{ zql }->apply_to_resultset($base_rs)
            );

            $iterator = Zaaksysteem::Object::Iterator->new(
                rs => $rs,
                inflator => sub { $model->inflate_from_row(shift) }
            );
        } elsif ($c->stash->{ es_query }) {
            $iterator = Zaaksysteem::Object::Iterator->new(
                rs => $c->stash->{ es_query }->apply_to_resultset($base_rs),
                inflator => sub { $model->inflate_from_row(shift) },
            );
        }

        unless ($iterator) {
            throw('api/v1/case/query_fault', sprintf(
                'Could not build search query from request.'
            ));
        }

        return Zaaksysteem::API::v1::Set->new(
            iterator => $iterator,
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        $_->throw if blessed $_ && $_->can('throw');

        throw(
            'api/v1/case',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ set } = $set;

    $c->stash->{ cases } = $set->build_iterator->rs;

}

=head2 instance_base

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ case } = try {
        return $c->stash->{ cases }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    unless (defined $c->stash->{ case }) {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    unless ($c->stash->{ case }->object_class eq 'case') {
        throw('api/v1/case/not_found', sprintf(
            "The case object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }

    $c->stash->{zaak} = try {
        $c->stash->{case}->get_source_object;
    }
    catch {
        $c->log->warn($_);
        throw('api/v1/case/retrieval_fault',
            sprintf('Case retrieval failed, unable to continue.'));
    };
}

=head2 list

=head3 URL Path

C</api/v1/case>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{ cases }
    )->init_paging($c->req);

    ### Load relations
    my $object_uuids_query = $c->stash->{ result }->build_iterator->get_column('uuid')->as_query;
    my $relations = $c->model('DB::ObjectRelationships')->search(
        {
            -or => [
                { object1_uuid => { -in => $object_uuids_query } },
                { object2_uuid => { -in => $object_uuids_query } },
            ]
        }
    );

    $c->stash->{ serializer_opts }{ object_relationships } = [ $relations->all ];


    ### Load documents
    ### This is uncommented, why? Because we run it on every request but
    ### $documents is never used. When we do? Uncomment the below code here and
    ### in the reader: ObjectData routine: _load_documents
    # $self->_load_documents_in_serializer($c);
}

sub _load_documents_in_serializer {
    my ($self, $c) = @_;

    # WARNING: The contents of this subrouting is also used in our normal version
    # in Zaaksysteem::Backend::Object::Data::Roles::Case. Where case_documents are
    # on the object. When you change something below, please change it also in
    # Zaaksysteem::Backend::Object::Data::Roles::Case.
    my @case_ids = $c->stash->{ result }->build_iterator->get_column('object_id')->all;
    my $documents = $c->model('DB::File')->search(
        {
            case_id         => { -in => [ uniq @case_ids ] },
            accepted        => 1,
            date_deleted    => undef,
            destroyed       => 0,
        },
        { prefetch => [qw[filestore_id metadata_id]] }
    );

    $c->stash->{ serializer_opts }{ case_documents } = [ $documents->all ];
}

=head2 get

=head3 URL Path

C</api/v1/case/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ case };

    if ($c->stash->{ result }) {
        my $zaak = $c->stash->{ zaak };

        $zaak->log_view();

        my $uuid = $c->stash->{ result }->get_column('uuid');
        my $relations = $c->model('DB::ObjectRelationships')->search(
            {
                -or => [
                    { object1_uuid => $uuid },
                    { object2_uuid => $uuid },
                ]
            }
        );
        $c->stash->{ serializer_opts }{ object_relationships } = [ $relations->all ];
    }

}

=head2 casetype

=head3 URL Path

C</api/v1/case/[UUID]/casetype>

=cut

sub casetype : Chained('instance_base') : PathPart('casetype') : Args(0) : RO {
    my ($self, $c) = @_;

    # Prevent new query, use prefetched results
    my @relations = $c->stash->{ case }->object_relation_object_ids->all;

    my $relation = first { $_->object_type eq 'casetype' } @relations;

    # Safeguard for untouched case object_data rows since the introduction
    # of casetype embedded relations.
    unless (defined $relation) {
        my $zaak = $c->stash->{ zaak };

        $relation = $zaak->_rewrite_object_casetype($c->stash->{ case });
    }

    $c->stash->{ result } = try {
        $c->model('Object')->inflate_from_relation($relation);
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/casetype/retrieval_error', 'No casetype')
    };
}

=head2 create

=head3 URL Path

C</api/v1/case/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    my $model = $c->model('Zaak');
    my $create_arguments = $model->prepare_case_arguments($c->req->params);

    my $case = $model->create_case($create_arguments);
    $self->_reload_case_and_get($c, $case);
}

=head2 reserve_casenumber

=head3 URL Path

C</api/v1/case/reserve_casenumber>

=cut

sub reserve_casenumber : Chained('base') : PathPart('reserve_casenumber') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $id     = $c->model('Zaak')->generate_case_id();
    my $object = Zaaksysteem::Object::Types::Serial->new(
        name         => "number",
        serial       => $id,
        object_class => 'case',
    );

    $c->stash->{result}  = $object;
}


=head2 create_delayed

=head3 URL Path

C</api/v1/case/create_delayed>

=cut

sub create_delayed : Chained('base') : PathPart('create_delayed') : Args(0) : RW {
    my ($self, $c) = @_;

    # Unfortunatly I cannot save the output of this because it contains
    # all kinds of DB things and soforth, so need to run this twice,
    # also on case creation time
    my $model  = $c->model('Zaak');
    my $params = $c->req->params;

    $model->prepare_case_arguments($params);
    my $qitem = $model->create_delayed_case($params);

    $c->forward('/execute_post_request_actions');

    $c->stash->{result}  = $qitem;

}

sub reserve_filenumber : Chained('base') : PathPart('reserve_filenumber') : Args(0) : RW {
    my ($self, $c) = @_;

    my $id = $c->model('DB')->schema->resultset('File')->generate_file_id;

    my $object = Zaaksysteem::Object::Types::Serial->new(
        object_class => 'file',
        serial       => $id,
        name         => 'number',
    );

    $c->stash->{result} = $object;
}

=head2 prepare_file

=head3 URL

C</api/v1/case/prepare_file>

=cut

sub prepare_file : Chained('base') : PathPart('prepare_file') : Args(0) : RW {
    my ($self, $c) = @_;

    my @uploads = map { ref $_ eq 'ARRAY' ? @$_ : $_ } values %{ $c->req->uploads };

    unless (scalar @uploads) {
        throw('api/v1/case/upload', sprintf(
            'Upload(s) missing.'
        ), { http_code => 400 });
    }

    my $filestore = $c->model('DB::Filestore');

    $c->stash->{ result } = Zaaksysteem::API::v1::PreparedFileBag->new;

    for my $upload (@uploads) {
        my $file = try {
            return $filestore->filestore_create({
                original_name => $upload->filename,
                file_path     => $upload->tempname,
            });
        } catch {
            $c->log->warn($_);

            throw('api/v1/case/upload_validation', sprintf(
                'File creation failed, unable to continue.'
            ));
        };

        $c->stash->{ result }->add($file);

        my $clean_job = Zaaksysteem::Object::Types::ScheduledJob->new(
            job => 'CleanTmp',
            interval_period => 'once',
            next_run => DateTime->now->add(minutes => 15),
            data => $file->uuid
        );

        try { $c->model('Object')->save_object(object => $clean_job) } catch {
            $c->log->warn("Non-fatal; failed to schedule temporary file cleaner job. Original error follows:", $_);
        };
    }
}

=head2 transition

=head3 URL

C</api/v1/case/[UUID]/transition>

=cut

define_profile transition => (
    optional => {
        result_id   => 'Num',
        result      => 'Str',
    }
);

sub transition : Chained('instance_base') : PathPart('transition') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $opts = assert_profile($c->req->params)->valid;

    my $zaak = $c->stash->{zaak};
    unless ($zaak->zaaktype_node_id->properties->{ api_can_transition }) {
        throw('api/v1/case/transition', sprintf(
            'Case transitioning is not enabled for this casetype.'
        ));
    }

    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/transition/closed',
            "Unable to transition case, case is closed ",
            { http_code => 409 }
            )
    }

    try {
        if(exists $opts->{ result_id } || exists $opts->{ result }) {
            if($zaak->afhandel_fase->status eq ($zaak->milestone + 1)) {
                if ($opts->{result_id}) {
                    $zaak->set_result_by_id({ id => $opts->{ result_id } });
                } else {
                    $zaak->set_resultaat($opts->{result});
                }
            }
            else {
                throw('case/set_result', "Unable to set result: case is not in final state");
            }
        }

        $zaak->advance(
            object_model     => $c->model('Object'),
            betrokkene_model => $c->model('Betrokkene'),
            current_user     => $c->user,
        );
    }
    catch {
        my $errormsg = "$_";
        $c->log->info($errormsg);

        if (blessed($_) && $_->isa('BTTW::Exception::Base')) {
            ### TODO: We really need information about the "why", owner not complete? Missing result?
            my $object = $_->object;

            if ($object) {
                $errormsg
                    = 'No transition possible for this case, missing '
                    . join(
                    ', ',
                    map({
                            $_ =~ s/_complete//;
                                $_;
                        }
                        grep ({ !$object->{transition_states}->{$_} }
                            keys %{ $object->{transition_states} }))
                    );

            }
        }

        throw('api/v1/case/transition', $errormsg);
    };

    $self->_reload_case_and_get($c, $zaak);
}

=head2 upload_files

=head3 URL Path

C</api/v1/case/[UUID]/upload_files>


    );

Upload files to a case

=cut

sub upload_files : Chained('instance_base') : PathPart('upload_files') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    my $zaak = $self->_get_case($c);

    my %uploads = %{$c->req->uploads};
    my @fields = keys %uploads;
    unless (@fields) {
        throw('api/v1/case/upload_files', sprintf(
            'Upload(s) missing.'
        ), { http_code => 400 });
    }


    my $kenmerken = $self->_get_case_attributes($c, $zaak, \@fields);

    $kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    my %document_values;
    while (my $k = $kenmerken->next) {
       $document_values{ $k->bibliotheek_kenmerken_id->magic_string } = $k->id,
    }

    $c->stash->{ result } = Zaaksysteem::API::v1::PreparedFileBag->new;

    try {
        my $counter = 0;
        my $filestore = $c->model('DB::Filestore');
        my $file_rs   = $c->model('DB::File');

        foreach my $magic_string (keys %uploads) {
            if (!$document_values{$magic_string}) {
                throw(
                    "api/v1/case/file_upload/attribute/unknown",
                    "Unknown document attribute $magic_string",
                    { http_code => 400 }
                );
            }

            my $upload = $uploads{$magic_string};

            $c->log->debug(
                sprintf(
                    "Would create file %s with path %s as attribute %s",
                    $upload->filename, $upload->tempname, $magic_string
                )
            );


            my $filestore_obj = $filestore->filestore_create({
                original_name => $upload->filename,
                file_path     => $upload->tempname,
            });

            $c->stash->{ result }->add($filestore_obj);

            $file_rs->file_create(
                {
                    disable_message   => 1,
                    case_document_ids => [ $document_values{$magic_string} ],
                    name      => $filestore_obj->original_name,
                    db_params => {
                        filestore_id => $filestore_obj->id,
                        created_by   => $c->user->betrokkene_identifier,
                        case_id      => $zaak->id
                    }
                }
            );

            $counter++;

        }

        if ($counter) {
            $zaak->create_message_for_behandelaar(
                message => sprintf(
                    "%d document%s toegevoegd door %s",
                    $counter, $counter > 1 ? 'en' : '', $c->user->display_name,
                ),
                event_type => 'api/v1/update/documents',
            );
        }

    }
    catch {
        $c->log->fatal($_);
        throw('api/v1/case/upload_validation', sprintf(
            'File creation failed, unable to continue.'
        ));
    };

}

=head2 update

=head3 URL Path

C</api/v1/case/[UUID]/update>
=cut

define_profile update => (
    optional => {
        values => 'HashRef',
        payment_info => 'HashRef',
    },
);

sub update : Chained('instance_base') : PathPart('update') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);

    my $opts = assert_profile($c->req->params)->valid;

    $c->stash->{zaak} = $self->_get_case(
        $c,
        ($opts->{payment_info} ? 1 : 0),
    );

    if ($opts->{values}) {
        $self->_update_case_values($c, $opts->{values});
    }

    if ($opts->{payment_info}) {
        $c->model('Zaak')->update_payment_status(
            zaak           => $c->stash->{zaak},
            payment_status => $opts->{payment_info}{payment_status},
        );
    }

    $self->_reload_case_and_get($c, $c->stash->{zaak});
}

sub _update_case_values {
    my $self = shift;
    my $c = shift;
    my $values = shift;

    my $zaak = $c->stash->{zaak};

    my @fields = keys %$values;
    my $kenmerken = $self->_get_case_attributes($c, $zaak, \@fields);

    my $normal_kenmerken = $kenmerken->search(
        {
            'bibliotheek_kenmerken_id' => { '!=' => undef },
            'bibliotheek_kenmerken_id.value_type' => { '!=' => 'file' }
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    );

    my $document_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'file'
    });

    my $geolatlon_kenmerken = $kenmerken->search({
        'bibliotheek_kenmerken_id.value_type' => 'geolatlon'
    });

    my %new_values;
    my %document_values;

    # Iterate over all found attributes, ignoring the ones we could not
    # resolve, and build the %new_values var.
    for my $key (keys %$values) {
        unless (ref $values->{ $key } eq 'ARRAY') {
            throw('api/v1/case/update', sprintf(
                'Every attribute value must be wrapped in an array. Validation for attribute "%s" failed.',
                $key
            ), { http_code => 400 });
        }
    }

    while (my $dbkenmerk = $normal_kenmerken->next) {
        my $kenmerk = $dbkenmerk->bibliotheek_kenmerken_id;
        my $value   = $values->{ $kenmerk->magic_string };

        ### Checkboxes, we normalize it to like it are "opplusbare" kenmerken. It is the same
        ### in our database.
        if ($kenmerk->can_have_multiple_values_per_value) {
            unless (ref $value eq 'ARRAY' && ref $value->[0] eq 'ARRAY') {
                throw(
                    'api/v1/case/incorrect_values',
                    'Value: "' . $kenmerk->magic_string . '" is a "multiple option" value (like checkbox)'
                    .', which needs to be wrapped in an array. E.g: "values": { "'
                    . $kenmerk->magic_string . '": [  [ "Value1", "Value2" ]  ], "another_attribute": [ "55" ] }'
                );
            }

            $value = [ @{ $value->[0] } ];
        }

        $new_values{$kenmerk->id} = $value;
    }

    while (my $type_kenmerk = $document_kenmerken->next) {
        my $kenmerk = $type_kenmerk->bibliotheek_kenmerken_id;

        $document_values{ $type_kenmerk->id } = $values->{ $kenmerk->magic_string };
    }

    try {
        $zaak->zaak_kenmerken->update_fields({
            new_values => \%new_values,
            zaak => $zaak
        });
        if (my $counter = keys %new_values) {
            $zaak->create_message_for_behandelaar(
                message => sprintf(
                    "%d kenmerk%s toegevoegd door een extern proces",
                    $counter, $counter > 1 ? 'en' : ''
                ),
                event_type => 'api/v1/update/attributes',
            );
        }
    } catch {
        $c->log->warn($_);

        throw('api/v1/case/update_fault', sprintf(
            'Case update failed, unable to continue'
        ));
    };

    try {
        my $counter =0;
        for my $zaaktype_kenmerk_id (keys %document_values) {
            my $refs = $document_values{ $zaaktype_kenmerk_id };

            for my $ref (ref $refs eq 'ARRAY' ? @$refs : $refs) {
                my $filestore_obj = $c->model('DB::Filestore')->find({
                    uuid => $ref
                });

                next unless defined $filestore_obj;

                $c->model('DB::File')->file_create(
                    {
                        disable_message   => 1,
                        case_document_ids => [$zaaktype_kenmerk_id],
                        name      => $filestore_obj->original_name,
                        db_params => {
                            accepted     => 1,
                            filestore_id => $filestore_obj->id,
                            created_by =>
                                $c->user->betrokkene_identifier,
                            case_id => $zaak->id
                        }
                    }
                );

                $counter++;
            }
        }
        if ($counter) {
            $zaak->create_message_for_behandelaar(
                message => sprintf(
                    "%d document%s toegevoegd door een extern proces",
                    $counter, $counter > 1 ? 'en' : ''
                ),
                event_type => 'api/v1/update/documents',
            );
        }

    } catch {
        $c->log->error($_);

        throw('api/v1/case/update_document_fault', sprintf(
            'Case document update failed, unable to continue.'
        ));
    };

    try {
        while (my $ztk = $geolatlon_kenmerken->next) {
            my $value = $values->{ $ztk->bibliotheek_kenmerken_id->magic_string };

            next unless $value;
            next unless $ztk->properties->{ map_case_location };

            # Case location cannot be defined through multiple values, just
            # pick the first one.
            my ($lat, $lon) = split m[,], $value->[0];

            my $model = $c->model('Queue');

            $model->queue_item($model->create_item('update_case_location', {
                object_id => $c->stash->{ case }->id,
                label => 'Zaaklocatie instellen',
                data => {
                    latitude => $lat,
                    longitude => $lon
                }
            }));
        }
    } catch {
        # Not being able to set a case location is not a case update failure
        # condition, just warn the update has failed in log.
        $c->log->warn($_);
    };

    return;
}

=head2 take

=head3 URI

    C</api/v1/case/[UUID]/take>

=cut

sub take : Chained('instance_base') : PathPart('take') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_user;

    my $zaak = $c->stash->{ zaak };
    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/take/closed',
            "Unable to 'take' case, case is closed ",
            { http_code => 409 }
            )
    }

    $zaak->open_zaak();
    $self->_reload_case_and_get($c, $zaak);
}

=head2 reject

=head3 URI

    C</api/v1/case/[UUID]/reject>

=cut

sub reject : Chained('instance_base') : PathPart('reject') : Args(0) : RW {
    my ($self, $c) = @_;

    $self->assert_post($c);
    $c->assert_user;

    my $zaak = $c->stash->{ zaak };
    if ($zaak->is_afgehandeld) {
        throw(
            'api/v1/case/reject/closed',
            "Unable to 'reject' case, case is closed ",
            { http_code => 409 }
        );
    }

    if ($zaak->status ne 'new') {
        throw(
            'api/v1/case/reject/not_new',
            "Unable to 'reject' case, case is not in 'intake' state",
            { http_code => 409 }
        );
    }

    $zaak->reject_zaak();
    $self->_reload_case_and_get($c, $zaak);
}


sub _get_case_attributes {
    my ($self, $c, $case, $fields) = @_;

    my $kenmerken = $case->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            # Only get those kenmerken we need.
            'bibliotheek_kenmerken_id.magic_string' => { -in => $fields },


            # 'specifieke behandelrechten' always off-limits for API users
            required_permissions => [ undef, '{}' ]
        },
        {
            prefetch => [qw[bibliotheek_kenmerken_id zaak_status_id]]
        }
    );

    try {
        $c->assert_user();
    }
    catch {
        $kenmerken = $kenmerken->search({
            # And only those in the current phase.
            'zaak_status_id.status' => $case->milestone + 1,
        });
    };

    unless ($kenmerken->count) {
        # Note that this error may occur when a valid attribute name was
        # provided, but it isn't bound to the current phase.
        throw('api/v1/case/nop', sprintf(
            'Refusing to update because no supplied value resolved to an (authorized) attribute'
        ), { http_code => 409 });
    }

    return $kenmerken;
}


sub _get_case {
    my $self = shift;
    my $c = shift;
    my $allow_closed = shift;

    my $zaak = $c->stash->{zaak};

    if ($zaak->is_afgehandeld && !$allow_closed) {
        throw(
            'api/v1/case/update/closed',
            "Unable to update case, case is closed ",
            { http_code => 409 }
        );
    }

    return $zaak;
}

sub _reload_case_and_get {
    my ($self, $c, $zaak) = @_;

    $zaak->discard_changes;
    $zaak->_touch;

    $c->stash->{case} = $zaak->object_data;
    $c->stash->{zaak} = $zaak;
    $c->detach('get');
}



__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
