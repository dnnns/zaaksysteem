package Zaaksysteem::Controller::API::v1::Case::Queue;

use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::Queue - APIv1 controller for case object queue items

=head1 DESCRIPTION

=head1 ACTIONS

=head2 queue_base

Reserves the C</api/v1/case/[UUID]/queue> namespace.

=cut

sub queue_base : Chained('/api/v1/case/instance_base') : PathPart('queue') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{ queue } = $c->stash->{ case }->queues->search(undef, {
        order_by => { -desc => 'date_created' }
    })
}

=head2 instance_base

Reserves the C</api/v1/case/[UUID]/queue/[UUID]> namespace.

=cut

sub instance_base : Chained('queue_base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $item = $c->stash->{ queue }->find($uuid);

    unless (defined $item) {
        throw('api/v1/case/queue/item/not_found', sprintf(
            'No queue item with id "%s" could be found',
            $uuid
        ));
    }

    $c->stash->{ item } = $item;
}

=head2 list

Returns the serialized set of queued items.

=head3 URL path

C</api/v1/case/[UUID]/queue>

=cut

sub list : Chained('queue_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->stash->{ queue },
    )->init_paging($c->request);
}

=head2 get

=head3 URL path

C</api/v1/case/[UUID]/queue/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ item };
}

=head2 handle

=head3 URL path

C</api/v1/case/[UUID]/queue/[UUID]/run>

=cut

sub handle : Chained('instance_base') : PathPart('run') : Args(0) : RW {
    my ($self, $c) = @_;

    # Sync instance to DB row state
    $c->stash->{ result } = $c->stash->{ item }->discard_changes;
}

=head1 ATTRIBUTES

=head2 api_capabilities

Defines the API capability for this controller, which is set to C<intern>.

=cut

has api_capabilities => (
    is => 'ro',
    default => sub { return [qw[intern]] }
);

=head2 api_control_module_types

=cut

has api_control_module_types => (
    is => 'rw',
    default => sub { return [qw[api app]] }
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
