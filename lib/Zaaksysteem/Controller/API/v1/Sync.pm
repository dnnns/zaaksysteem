package Zaaksysteem::Controller::API::v1::Sync;

use Moose;

use BTTW::Tools;

use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::API::v1::Message::Ack;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Sync - Synchronization access for objects

=head1 DESCRIPTION

This controller contains a collection actions that synchronize parts of the
older Zaaksysteem infrastructure to new objects.

=head1 ATTRIBUTES

=head2 api_capabilities

=cut

has api_capabilities => (
    is => 'ro',
    default => sub { return [qw[intern]] }
);

=head1 ACTIONS

=head2 base

This base action reserves the C</api/v1/sync> namespace and performs minimal
security checks (requires admin permissions).

=cut

define_profile base => (
    optional => {
        object_id => 'Int',
        immediate => 'Bool'
    }
);

sub base : Chained('/api/v1/base') : PathPart('sync') : CaptureArgs(0) {
    my ($self, $c) = @_;

    # Hard require admin permissions.
    $c->assert_any_user_permission('admin');

    $c->stash->{ opts } = assert_profile($c->req->params)->valid;
}

=head2 attributes

Synchronizes L<Zaaksysteem::DB::Component::BibliotheekKenmerken> objects using
the L<Zaaksysteem::Object::Queue> infrastructure.

=cut

sub attributes : Chained('base') : PathPart('attributes') : Args(0) {
    my ($self, $c) = @_;

    my %search_args;

    if (exists $c->stash->{ opts }{ object_id }) {
        $search_args{ id } = $c->stash->{ opts }{ object_id };
    }

    $self->create_items($c, {
        table => 'BibliotheekKenmerken',
        label => 'Kenmerk synchronisatie',
        search_args => \%search_args
    });
}

=head2 documents

Synchronizes L<Zaaksysteem::DB::Component::File> objects using the
L<Zaaksysteem::Object::Queue> infrastructure.

=cut

sub documents : Chained('base') : PathPart('documents') : Args(0) {
    my ($self, $c) = @_;

    my %search_args;

    if (exists $c->stash->{ opts }{ object_id }) {
        $search_args{ id } = $c->stash->{ opts }{ object_id };
    }

    $self->create_items($c, {
        table => 'File',
        label => 'Document synchronisatie',
        search_args => \%search_args
    });
}

=head2 cases

Synchronizes L<Zaaksysteem::Zaken::ComponentZaak> objects using the
L<Zaaksysteem::Object::Queue> infrastructure.

=cut

sub cases : Chained('base') : PathPart('cases') : Args(0) {
    my ($self, $c) = @_;

    my %search_args;

    if (exists $c->stash->{ opts }{ object_id }) {
        $search_args{ id } = $c->stash->{ opts }{ object_id };
    }

    $self->create_items($c, {
        table => 'Zaak',
        label => 'Zaak synchronisatie',
        search_args => \%search_args
    });
}

=head2 casetypes

Synchronizes L<Zaaksysteem::DB::Component::Zaaktype> objects using the
L<Zaaksysteem::Object::Queue> infrastructure.

=cut

sub casetypes : Chained('base') : PathPart('casetypes') : Args(0) {
    my ($self, $c) = @_;

    my %search_args;

    if (exists $c->stash->{ opts }{ object_id }) {
        $search_args{ id } = $c->stash->{ opts }{ object_id };
    }

    $self->create_items($c, {
        table => 'Zaaktype',
        label => 'Zaaktype synchronisatie',
        search_args => \%search_args
    });
}

=head1 METHODS

=head2 create_items

Creates L<Zaaksysteem::Backend::Object::Queue::Component> instances and populates the
queue table with the created items.

=cut

sub create_items {
    my ($self, $c, $item_spec) = @_;

    my $rs = $c->model(sprintf('DB::%s', $item_spec->{ table }))->search(
        $item_spec->{ search_args }
    );

    my @jobs;

    for my $id ($rs->get_column('id')->all) {
        push @jobs, {
            label => $item_spec->{ label },
            data => {
                table => $item_spec->{ table },
                primary_key => $id
            }
        }
    }

    my $model = $c->model('Queue');

    if ($c->stash->{ opts }{ immediate }) {
        if (scalar @jobs > 50) {
            throw('api/v1/sync/too_many_items', sprintf(
                'Too many jobs to execute immediately (max 50, count %d)',
                scalar @jobs
            ));
        }

        my @results = map {
            $model->run($model->create_item('sync_object', $_))
        } @jobs;

        $c->stash->{ result } = Zaaksysteem::API::v1::ArraySet->new(
            content => \@results
        );
    } else {
        $model->create_items('sync_object', @jobs);

        $c->stash->{ result } = Zaaksysteem::API::v1::Message::Ack->new;
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
