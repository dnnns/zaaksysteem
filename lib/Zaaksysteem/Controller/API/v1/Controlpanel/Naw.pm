package Zaaksysteem::Controller::API::v1::Controlpanel::Naw;
use Moose;

use BTTW::Tools;
use Zaaksysteem::Object::Types::Naw;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern allow_pip/] }
);

has 'api_control_module_types' => (
    is          => 'rw',
    default     => sub { ['controlpanel'] },
);


=head1 NAME

Zaaksysteem::Controller::API::v1::Controlpanel::naw - APIv1 controller for Controlpanel objects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/controlpanel/ID/naw>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Controlpanel::naw>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Controlpanel::naw>

=head1 ACTIONS

=head2 base

=cut

sub base : Chained('/api/v1/controlpanel/instance_base') : PathPart('naw') : CaptureArgs(0) : Scope('controlpanel') {
    my ($self, $c)      = @_;

    my $zql             = 'SELECT {} FROM naw where owner="' . $c->stash->{controlpanel}->get_object_attribute('owner')->value . '"';

    $c->stash->{zql}    = Zaaksysteem::Search::ZQL->new($zql);

    my $set = try {
        return Zaaksysteem::API::v1::Set->new(
            iterator => $c->model('Object')->zql_search($zql)
        )->init_paging($c->request);
    } catch {
        $c->log->warn($_);

        throw(
            'api/v1/controlpanel/naw',
            'API configuration error, unable to continue.'
        );
    };

    $c->stash->{ naw_set }     = $set;
    $c->stash->{ naws }        = $set->build_iterator->rs;
}

=head2 list

=head3 URL Path

C</api/v1/controlpanel/ID/naw>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ naw_set };
}

=head2 naw_base

=cut

sub naw_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    # Retrieve case via Object model so we can benefit from builtin ACL stuff
    $c->stash->{ naw } = try {
        return $c->stash->{ naws }->find($uuid);
    } catch {
        $c->log->warn($_);

        throw('api/v1/controlpanel/naw/not_found', sprintf(
            "The controlpanel naw object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    };

    unless (defined $c->stash->{ naw } && $c->stash->{ naw }->object_class eq 'naw') {
        throw('api/v1/controlpanel/naw/not_found', sprintf(
            "The controlpanel naw object with UUID '%s' could not be found.",
            $uuid
        ), { http_code => 404 });
    }
}

=head2 get

=head3 URL Path

C</api/v1/controlpanel/[cpuuid]/naw/[UUID]>

=cut

sub get : Chained('naw_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    $c->stash->{ result } = $c->stash->{ naw };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
