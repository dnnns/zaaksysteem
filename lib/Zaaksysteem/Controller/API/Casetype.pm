package Zaaksysteem::Controller::API::Casetype;

use Moose;

use JSON qw[decode_json];
use Data::Visitor::Callback;

use BTTW::Tools;
use Zaaksysteem::ZTT::MagicDirective;
use Zaaksysteem::Attributes qw[ZAAKSYSTEEM_SYSTEM_ATTRIBUTES];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

# Reserves the URI namepace

=head2 casetype_root

Empty function as a base for this api

=cut

sub casetype_root : Chained('/api/base') : PathPart('casetype') : CaptureArgs(0) {
}

sub index : Chained('casetype_root') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $casetype_id) = @_;

    $c->stash->{casetype} = $c->load_session_casetype($casetype_id);
}


sub save_child_casetypes: Chained('index') : PathPart('save_child_casetypes') {
    my ($self, $c) = @_;

    my $child_casetypes = JSON::decode_json($c->req->params->{child_casetypes});

    # this effectively updates the casetype in the session
    $c->stash->{casetype}->{node}->{properties}->{child_casetypes} = $child_casetypes;
    $c->stash->{zapi} = [ $child_casetypes ];
}


sub is_casetype_mother : Chained('index') : PathPart('is_casetype_mother') {
    my ($self, $c) = @_;

    # this effectively updates the casetype in the session
    $c->stash->{casetype}->{node}->{properties}->{is_casetype_mother} =
        $c->req->params->{is_casetype_mother};

    $c->stash->{zapi} = [];
}


sub publish : Chained('index') : PathPart('publish') : ZAPI {
    my ($self, $c) = @_;

    try {
        $c->model('DB')->schema->txn_do(sub {
            my @children = $c->model('Zaaktypen')->list_child_casetypes({
                mother => $c->stash->{casetype}
            });

            my %queue = map {$_ => 0} (
                $c->stash->{casetype}->{zaaktype}->{id},
                map { $_->{casetype}{id} } @children
            );

            $self->initialize_queue($c, \%queue);

            $c->forward('publish_mother');

            # there will only be children if this has been configured
            foreach my $child_settings (@children) {
                $c->forward('publish_child', [$child_settings]);
            }

            $c->stash->{zapi} = [];

        });
    } catch {
        $self->log->error("Publishing error captured: " . $_);
        $c->stash->{zapi} = [];
    };
}


=head2 validate_rule_action_formula

TODO Validates the given formula for this rule?

=cut

sub validate_rule_action_formula : Chained('casetype_root') : PathPart('rule/action/formula/validate') : Args(0) {
    my ($self, $c) = @_;

    my $bibliotheek_kenmerken = $c->model('DB::BibliotheekKenmerken');

    my @valid_attribute_types = qw[
        text
        select
        option
        numeric
        valuta
        valutain
        valutaex
        valutaex6
        valutain6
        valutaex21
        valutain21
    ];

    # Deref the underlying Parse::RecDescent object so we can call a
    # subparser (binary_expr is not usually the root production of a magic
    # directive)
    my $parser = Zaaksysteem::ZTT::MagicDirective->new->parser;
    my $visitor = Data::Visitor::Callback->new(
        scalar => sub {
            my $attribute_name = ${ $_ };

            if ($attribute_name =~ m[^case\.]) {
                # Yeah... great. The case.price attribute is defined as 'text'
                # in the Attributes package. Changing the type will probably
                # cause all kinds of havoc, so imma let it slide.
                if ($attribute_name eq 'case.price') {
                    return;
                }

                my ($attribute) = grep {
                    $_->name eq $attribute_name
                } Zaaksysteem::Attributes::predefined_case_attributes;

                if (defined $attribute) {
                    return if grep {
                        $_ eq $attribute->attribute_type
                    } @valid_attribute_types;

                    throw('api/casetype/validation/invalid_attribute_type', sprintf(
                        'Het systeemkenmerk "%s" is geen tekst, numeriek, valuta, enkelvoudige keuze, of keuzelijst kenmerk',
                        $attribute_name
                    ));
                }
            }

            if ($attribute_name =~ m/\w[\w\.]+/) {
                # Normalize name reference to magic string
                my @components = split m[\.], $attribute_name;
                shift @components if $components[0] eq 'attribute';
                my $magic_string = join '.', map { lc } @components;

                my $attribute = $bibliotheek_kenmerken->search({
                    magic_string => $magic_string
                })->first;

                if (defined $attribute) {
                    # If the value type for the attribute is valid, we're
                    # cool and we can return safely.
                    unless (grep { $_ eq $attribute->value_type } @valid_attribute_types) {
                        throw('api/casetype/validation/invalid_attribute_type', sprintf(
                            'Het kenmerk "%s" is geen tekst, numeriek, valuta, enkelvoudige keuze, of keuzelijst kenmerk',
                            $attribute_name
                        ));
                    }

                    if ($attribute->type_multiple) {
                        throw('api/casetype/validation/invalid_attribute_type', sprintf(
                            'Het kenmerk "%s" is opplusbaar, en wordt niet ondersteund in regelformules',
                            $attribute_name
                        ));
                    }

                    return;
                }
            }

            throw('api/casetype/validation/invalid_attribute_reference', sprintf(
                'Het kenmerk "%s" kan niet gevonden worden.',
                $attribute_name
            ));
        }
    );

    for my $expression (values %{ $c->req->params }) {
        my $ast = $parser->binary_expr($expression);

        unless (defined $ast) {
            throw('api/casetype/validation/invalid_formula', sprintf(
                'De formule "%s" is geen geldige invoer.',
                $expression
            ));
        }

        $visitor->visit($ast);
    }

    $c->stash->{ zapi } = {
        success => \1
    }
}

sub publish_mother : Private {
    my ($self, $c) = @_;

    my $commit_message    = $c->req->params->{commit_message};
    my $commit_components = $c->req->params->{commit_components};

    my $zaaktype_node = $c->model('Zaaktypen')->commit_session(
        session           => $c->stash->{casetype},
        commit_message    => $commit_message,
        commit_components => $commit_components,
    );
    my $zaaktype_id = $zaaktype_node->get_column('zaaktype_id');

    $self->update_queue($c, $zaaktype_id);
    $self->flush($c, $zaaktype_id);
}


sub publish_child : Private {
    my ($self, $c, $child_settings) = @_;

    my $commit_message    = $c->req->params->{commit_message};
    my $commit_components = $c->req->params->{commit_components};

    $c->model('Zaaktypen')->update_child_casetype({
        child_settings    => $child_settings,
        mother            => $c->stash->{casetype},
        commit_message    => $commit_message,
        commit_components => $commit_components,
    });

    $self->update_queue($c, $child_settings->{casetype}{id});

    # child casetypes may be open. when refreshing, the user
    # wants to see it updated with the applied changes
    $self->flush($c, $child_settings->{casetype}{id});
}


sub initialize_queue {
    my ($self, $c, $queue) = @_;

    $c->session->{casetype_mother} = $queue;
    $c->finalize_session; # save session so the publish_status request will see this
}


sub update_queue {
    my ($self, $c, $casetype_id) = @_;

    $c->session->{casetype_mother}->{$casetype_id} = 1;
    $c->finalize_session; # save session so the publish_status request will see this
}

sub publish_status : Chained('index') : PathPart('publish_status') : ZAPI {
    my ($self, $c) = @_;

    $c->stash->{zapi} = [$c->session->{casetype_mother}];
}

=head2 flush

casetypes are edited in the session - the pages manipulated the
casetype info in the session. after publishing the casetype is flushed
from the session.

=cut

sub flush {
    my ($self, $c, $casetype_id) = @_;

    die "need casetype_id" unless $casetype_id;

    delete $c->session->{zaaktypen}->{$casetype_id};
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 index

TODO: Fix the POD

=cut

=head2 initialize_queue

TODO: Fix the POD

=cut

=head2 is_casetype_mother

TODO: Fix the POD

=cut

=head2 publish

TODO: Fix the POD

=cut

=head2 publish_child

TODO: Fix the POD

=cut

=head2 publish_mother

TODO: Fix the POD

=cut

=head2 publish_status

TODO: Fix the POD

=cut

=head2 save_child_casetypes

TODO: Fix the POD

=cut

=head2 update_queue

TODO: Fix the POD

=cut

