package Zaaksysteem::Controller::API::Poc;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller::API' }

sub foo : Local : ZAPI {
    my ($self, $c) = @_;

    $self->digest_auth($c);

    #$self->always_auth($c);

    return 1;
}

sub digest_auth : Private {
    my ($self, $c) = @_;

    my $realm = $c->get_auth_realm('zs_test');
    #barf($realm);
    $realm->authenticate($c, $realm);
}

sub always_auth : Private {
    my ($self, $c) = @_;

    my $realm = $c->get_auth_realm('zs_test');
    $realm->credential->authorization_required_response($c, $realm);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Controller::API::Poc - Proof of Concept for HTTP digest authentication

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 always_auth

TODO: Fix the POD

=cut

=head2 digest_auth

TODO: Fix the POD

=cut

=head2 foo

TODO: Fix the POD

=cut

