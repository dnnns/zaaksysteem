package Zaaksysteem::SAML2::IdP;

use Moose;

BEGIN { extends 'Net::SAML2::IdP'; }

use BTTW::Tools;

use LWP::UserAgent;

use XML::XPath;
use XML::XPath::Node::Element;
use XML::XPath::Node::Text;

use URI;

has 'interface'         => (
    is  => 'rw',
    isa => 'Zaaksysteem::Model::DB::Interface',
);

### Metadata without SLO-urls, think eHerkenning
has '+slo_urls'         => (isa => 'Maybe[HashRef[Str]]', is => 'ro', required => 0);
has '+art_urls'         => (isa => 'Maybe[HashRef[Str]]', is => 'ro', required => 0);
# has '+formats'          => (isa => 'Maybe[HashRef[Str]]', is => 'ro', required => 0);
# has '+default_format'   => (isa => 'Maybe[Str]', is => 'ro', required => 0);

# When using a Swift storage backend, we do get a filehandle which we cannot allow
# to go out of scope during this session. We place cert_path in _cert_fh for persistence.
has _cacert_fh => ( is => 'rw' );

=head1 SAML2::IdP

This class wraps L<Net::SAML2::IdP> for a bit nicer integration with
L<Zaaksysteem>. In here you can do some better/fancier validation of the
object's initialization, and extend the default constructors with subs
that hook into Zaaksysteem's behavior.

=head1 Example usage

In the context of Zaaksysteem, we are the SP, not the IdP in the SAML process.
This means that we merely use this IdP package to model the authenticating side
(So DigID, E-Herkenning, Google, whoever). This class retrieves the metadata
and allows you to retrieve a base URL for doing useful things with the IdP.

The following snippet gets the base URL for single-sign-on.

    my $idp = Zaaksysteem::SAML2::IdP->new_from_url(
        url => 'https://url.to/idp/metadata',
        cacert => '/path/to/ssl/certificate/that/signs/idp/messages'
    );

    my $sso_base_url = $idp->sso_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect');

You can then use this base URL to generate a full HTTP Redirect url that can
be used to redirect the user to the IdP servers for authentication.

=head2 Common pitfalls

This object requires you supply a PEM certificate(-chain) that is being used to
sign the messages coming from the IdP. B<That doesn't need to be the same
CA-certificate your SP certificate and key were signed under!>

=head1 CONSTRUCTORS

=head2 new_from_interface

This convenience constructor makes it easy to generate an IdP interface object
from a L<Zaaksysteem::Backend::Sysin::Interface::Component> object (or deriving
modules).

=head3 Parameters

=over 4

=item interface

Required argument, assumed and validated to be an instance of
L<Zaaksysteem::Model::DB::Interface>

=back

=cut

define_profile new_from_interface => (
    required => [qw[interface]],
    typed => {
        interface => 'Zaaksysteem::Model::DB::Interface'
    }
);

sub new_from_interface {
    my ($class, %args) = @_;

    my $interface = assert_profile(\%args)->valid->{ interface };

    my $schema = $interface->result_source->schema;
    my $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.idp_ca[0].id'));

    unless($ca_file) {
        throw('saml2/idp/invalid_ca_certificate', 'CA Certificate configured for IdP not found in filestore.');
    }

    my $url         = $interface->jpath('$.idp_metadata');

    my $ca_path     = $ca_file->get_path;

    if ($url) {
        return $class->new_from_url(
            url => URI->new($url),
            _cacert_fh => $ca_path,
            cacert => "$ca_path",
            interface => $interface
        );
    }

    my $metadata_filestore_id = $interface->jpath('$.idp_metadata_filename[0].id');

    if ($metadata_filestore_id) {
        my $metadata = $schema->resultset('Filestore')->find($metadata_filestore_id);

        unless (defined $metadata) {
            throw('saml2/idp/cached_metadata_not_found', sprintf(
                'Attempted to load SAML IdP Metadata from filestore (id: %d), none found',
                $metadata_filestore_id
            ));
        }

        # new_from_xml comes from the parent class, inject interface later
        my $self = $class->new_from_xml(
            xml        => $metadata->content,
            _cacert_fh => $ca_path,
            cacert     => "$ca_path",
        );

        $self->interface($interface);
        # done

        return $self;
    }

    throw('saml2/idp/no_metadata_given', 'No metadata URL given or metadata filename uploaded');
}

=head2 new_from_url

Overriding constructor that does the same as C<new_from_url> from
L<Net::SAML2::IdP> but doesn't die, and throws proper exceptions on failure.

This method also verifies the existence of a C<NameIDFormat> element in the
metadata XML being retrieved from the IdP, and injects a default value with
L</patch_missing_nameid_format>.

=head3 Parameters

=over 4

=item url

String value of the URL where the IdP's metadata can be found.

=item cacert

A file containing the certificate that was used to sign the metadata returned
by the IdP. This can be a different CA certificate than the one used to sign
the SP's certificates.

=back

=cut

define_profile new_from_url => (
    required => [qw[url cacert interface]],
    typed => {
        url => 'URI',
        interface => 'Zaaksysteem::Model::DB::Interface'
    },
    constraint_methods => {
        cacert => sub { -e pop }
    }
);

sub new_from_url {
    my ($class, %args) = @_;

    my $opts = assert_profile(\%args)->valid;

    my $ua = LWP::UserAgent->new();
    $ua->ssl_opts(
        verify_hostname => 1,
        SSL_ca_file     => $opts->{cacert},
    );

    my $response = $ua->get($opts->{ url });

    unless($response->is_success) {
        throw('saml2/idp/metadata', sprintf(
            'Could not retrieve IdP metadata from URL "%s" (status: %s)',
            $opts->{ url },
            $response->code
        ));
    }

    my $self = $class->new_from_xml(
        xml         => $response->content,
        cacert      => $opts->{ cacert },
    );

    $self->_cacert_fh($opts->{ _cacert_fh }),

    $self->interface($opts->{ interface });

    return $self;
}

around BUILDARGS => sub {
    my $orig = shift;
    my $self = shift;

    my %args = @_;

    unless($args{ default_format }) {
        $args{ default_format } = 'entity';
        $args{ formats } = {
            entity => 'urn:oasis:names:tc:SAML:2.0:nameid-format-entity'
        };
    }

    return $self->$orig(%args);
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

