=head1 SAML - Logius

=head2 Registering a new customer

Logius provides an intake form that can be used to register new customers for a
SAML DigiD integration. Before filling out this form, several data must be
available and ready.

The first step to every full intake, is an intake for the B<accept>
environment. When it is complete, and Logius has verified the functionality of
the system, they can flip a switch and activate the production environment.

The intake for production requires no actions from our side, but the
production instance of Zaaksysteem for the customer must also have SAML
interfaces configured.

=head3 Prerequisites

=over 4

=item PKIoverheid Certificates

Every SAML Service Provider (in this case, Zaaksysteem) must own a PKIoverheid
certificate for interactiving with the Logius SAML servers. These certificates
can be ordered from any intermediary certificate authority such as
L<KPN|https://certificaat.kpn.com/pkioverheidcertificaten/servercertificaat/>,
L<Digidentity|https://www.digidentity.eu/static/nl/digidentity-ssl/pkioverheid-ssl.html>,
and a few others. These certificates are usually already present when starting
the DigiD intake process, and can be found on the NTN config repository (see
L<Zaaksysteem::Manual::Servers>).

=item Intake form

The intake form is usually provided by Logius when the customer initiates the
intake process. In case it is missing, ring up Logius for a copy. The form
is a PDF interactive form, so usage of Adobe Reader is required.

=item Metadata

One of the requirements for Logius is that we provide the full XML of the
customers Service Provider metadata. To acquire this document, configure the
Identity Provider interface as described below, and save the contents of
L<this|http://zaaksysteem-accept.GEMEENTE.nl/auth/saml/metadata> URL.

The URL needs to be changed according to the customer's Zaaksysteem instance.

=back

=head3 Filling out the form

=head4 Technical contact person

Enter the credentials to the local project manager for current customer.

=head4 "Gegevens van de aansluiting DigiD - koppelvlak SAML"

=over 4

=item URL

This is the main URL to the customers homepage. Something along the lines of
B<http://www.gemeente.nl/>

=item URL aansluiting

This is the URL that will be used by the end-user to login to their PIP or
C</form> sections. You can copy any URL from the C</form> Zaaksysteem page for
'Burger' case registrations.
B<https://sprint.zaaksysteem.nl/aanvraag/captcha/natuurlijk_persoon>, for
example.

=item URL metadata

This will be the same URL mentioned in L</Prerequisites>.

=item IP adres Service Provider

Fill in the customer-specific IP on which their Zaaksysteem instance is hosted.

=item Uitgaande IP Service Provider

Since we only map incoming traffic over the customer-specific IP, the outgoing
address will be the main IP of the server their Zaaksysteem instance is hosted
on.

=back

=head4 "Gegevens PKIoverheid-certificaat"

=over 4

=item Land (C)

Always B<NL>.

=item Common name (CN)

Always the hostname used by the customer to access their Zaaksysteem instance.
For example, B<zaaksysteem-accept.mintlab.nl>. This hostname must match the CN
used to register the PKIoverheid certificate.

=item Organisatienaam (O)

Always the full name of the customer. This field must also match the O specified
when requesting the PKIoverheid certificate, and can be found in the subject
line of the certificate.

=item "Eerste 10 cijfers van SSL fingerprint"

This is a secondary verification for Logius to make sure they've received the
correct certificates. It can be acquired by running the following command on
the certificate

    openssl x509 -noout -fingerprint -in /path/to/certificate.crt

OpenSSL returns a hexadecimal representation of the C<SHA1> fingerprint for the
specific certificate, of which you will only need to copy the first 5
digitgroups.

=item "Eenmalig inloggen"

Always tick "Nee", we don't support federated / single-signon SAML as of yet.

=back

=head2 Identity Provider Interface

=head3 Create

Create the interface by navigating to interface management interface and
clicking on the '+' to add it. B<The name you use here will be used in the
eventual UI to describe the authenticator. Make sure it's something the
customer expects>.

=head4 Example

=for html <img src="/images/manual/saml/create-idp-logius.png" />

=head3 Configure

=head4 Betrouwbaarheidsniveau

Select the I<security level> appropriate for this integration. This setting
depends on whatever deal was cut with the provider. Level 3 selects a
two-factor authentication level, where the user must login with user/password
credentials as well as a transaction code provided by a second path (usually
SMS).

=head4 SSO Binding

This option allows the SSO binding method to be changed. Always set it to
B<HTTP Redirect>.

=head4 SAML Implementatie

Set this to the name of the provider of the IdP we're talking to. Set it to
B<Logius>, since this manpage describes SAML for them.

=head4 SAML Metadata URL

Set this to one of the links found L</Metadata>. For the forseeable future this
value will always be B<Pre-prod Koppelvlak>.

=head4 Entity ID

In the case for Logius, this field should remain empty, the Entity ID is
automatically derived from other settings in this interface.

=head4 CA Certificaat van de IdP

This item expects a CA Certificate that was used to sign the exchanges and
metadata provided by the provider. Plain-old ASCII (Base64) armored PEM format
expected.

At the time of writing, the certificate is available for download
L<here|https://www.logius.nl/fileadmin/logius/product/pkioverheid/certificaten/QuoVadis_CSP_-_PKI_Overheid_CA_-_G2.crt>

The Subject for this certificate is
C<C=NL, O=QuoVadis Trustlink BV, OU=Issuing Certification Authority, CN=QuoVadis CSP - PKI Overheid CA - G2>
(Serial number C<20001554>), issued by
C<C=NL, O=Staat der Nederlanden, CN=Staat der Nederlanden Organisatie CA - G2>.

Let's hope we need not change this contant before the expiration date of the
certificate on B<23rd of March, 2020>.

=head4 Endpoints

Tick the boxes where you would like this authentication profile to be available
for the end-user.

=head4 Example

=for html <img src="/images/manual/saml/config-idp-logius.png" />

=head2 Metadata

Metadata for every version of Gemnet's SAML implementation.

=over 4

=item Pre-prod koppelvlak

L<https://was-preprod1.digid.nl/saml/idp/metadata>

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
