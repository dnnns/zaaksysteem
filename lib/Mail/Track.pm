package Mail::Track;
use Moose;

use Data::FormValidator;
use Mail::Track::Message;

=head1 NAME

Mail::Track - Mail handling for zaaksysteem.

=head1 SYNOPSIS

    my $mailer = Mail::Track->new(
        'subject_prefix_name'    => 'Mintlab'
    );

    $mailer->message($MIME_MESSAGE);

    my $message = $mailer->parse;

=head1 DESCRIPTION

Mailer for L<Zaaksysteem>, handling incoming and outgoing mail properly, by
taking care of subject prefixes, finding the proper identifier, etc.

=head1 ATTRIBUTES

=head2 subject_prefix_name

    $mailer->subject_prefix_name('Mintlab');

Name of the prefix when matching this mail from a mailstore, the prefix is in the
form of C<[PREFIX IDENTIFIER_REGEX]>, where IDENTIFIER_REGEX normally is something
like a case number, and defaults to something like #1234-4892-4390A.

Example: [Mintlab #1234-4892-4390A] Thank you for registering

In this case, C<Mintlab> is the C<subject_prefix_name>

=cut

has subject_prefix_name => (
    is      => 'rw',
    isa     => 'Str',
    default => sub { return '' },
    trigger => sub { shift->_clear_subject_prefix; },
);

=head2 identifier

Returns the identifier found in the subject.

=cut

has identifier => (
    is      => 'rw',
    isa     => 'Str',
    default => sub { return '' },
);

=head2 identifier_regex

The regex object (RegexpRef) for the identifier part of the subject string. It defaults to C<#(\d+\-\d+\-\d+[A-Z])>,
which enables subject parsing like '#1234-4892-4390A';

=cut

has identifier_regex => (
    is      => 'rw',
    isa     => 'RegexpRef',
    default => sub { qr/#(\d+\-\d+\-\d+[A-Z])/ },
    trigger => sub { shift->_clear_subject_prefix; }
);

=head2 subject_prefix_regex

Will return the regex object (RegexpRef) of the subject prefix, matching the identifier_regex
and the subject string.

When C<subject_prefix_name> is set to something like 'Mintlab', then the subject_prefix_regex
will default to:

C<\[Mintlab #(\d+\-\d+\-\d+[A-Z])\] ?(.*)$>

=cut

has 'subject_prefix_regex' => (
    'is'      => 'ro',
    'lazy'    => 1,
    'isa'     => 'RegexpRef',
    'builder' => '_build_subject_prefix',
    'clearer' => '_clear_subject_prefix',
);

sub _build_subject_prefix {
    my $self = shift;

    my $prefix = '\[';
    $prefix .= length($self->subject_prefix_name) ?
        $self->subject_prefix_name . ' ' : '';
    $prefix .= $self->identifier_regex . '';
    $prefix .= '\] ?(.*)$';

    return qr/$prefix/;
}

=head2 message

isa: Str

MIME Message to parse

=cut

has 'message' => (
    'is'  => 'rw',
    'isa' => 'Str',
);

has encoding => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return 'UTF-8';
    },
);

=head1 METHODS

=head2 $mailer->parse([$OPTIONAL_MESSAGE])

Return value: L<Zaaksysteem::Mail::Message>

    my $message = $mailer->parse($MIME_MESSAGE)

    # Or

    $mailer->message($MIME_MESSAGE);
    my $message = $mailer->parse;

Will return an object containing the mail message.

An optional

=cut

sub parse {
    my $self = shift;
    my $message = $_[0] ? $self->message(shift) : $self->message;

    return $self->_construct_message_object();
}

sub _construct_message_object {
    my $self = shift;

    return Mail::Track::Message->new(
        'message'              => $self->message,
        'subject_prefix_regex' => $self->subject_prefix_regex,
    );
}

use constant PREPARE_PROFILE => {
    required => [qw/to from subject/],
    optional => [qw/identifier cc bcc dkim transport extra_headers/],
};

=head2 prepare

TODO: Fix the POD

=cut

sub prepare {
    my $self = shift;

    my $dv = Data::FormValidator->check(shift, PREPARE_PROFILE);
    unless ($dv->success) {
        my @missing = $dv->missing;
        my @invalid = $dv->invalid;

        my @msg;
        if (@missing) {
            push(@msg, "Missing parameters: " . join(",", @missing));
        }
        if (@invalid) {
            push(@msg, "Invalid parameters: " . join(",", @invalid));
        }

        die join(". ", @msg);
    }
    my $params = $dv->valid;

    my $msg = Mail::Track::Message->new(
        subject_prefix_name  => $self->subject_prefix_name,
        subject_prefix_regex => $self->subject_prefix_regex,
        identifier_regex     => $self->identifier_regex,
        identifier           => $self->identifier,
        encoding             => $self->encoding,
        %{$params}
    );

    return $msg;
}

1;

__END__

=head1 EXAMPLES

See L<#SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
