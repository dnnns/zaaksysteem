#! /bin/bash

if [ ! -e etc/zaaksysteem.conf ]; then
    cp -v etc/zaaksysteem.conf.dist etc/zaaksysteem.conf
else
    echo "Not creating etc/zaaksysteem.conf - file exists."
fi

if [ ! -e etc/customer.d/default.conf ]; then
    cp -v etc/default_customer.conf.dist etc/customer.d/default.conf
else
    echo "Not creating etc/customer.d/default.conf - file exists."
fi

if [ ! -e etc/log4perl.conf ]; then
    cp -v etc/log4perl.conf.dist etc/log4perl.conf
else
    echo "Not creating etc/log4perl.conf - file exists"
fi

echo "Done!"
