[% USE Dumper %]

[% BLOCK display_required_permissions %]
    <ul>
        [% FOREACH required = kenmerk.required_permissions_decoded %]
            [% ou_id = required.org_unit_id %]
            <li>Afdeling: <strong>[% kenmerk.ou_names.$ou_id %]</strong>
            [% role_id = required.role_id %]<br/>
            Rol: <strong>[% kenmerk.role_names.$role_id %]</strong></li>
        [% END %]
    </ul>
[% END %]

[% BLOCK warn_required_kenmerk %]
    <div class="field-required-permissions">
        <i class="icon-font-awesome icon-lock add-tooltip" title="
        <em>Dit kenmerk kan alleen gewijzigd worden door de volgende rollen:</em>
        [% PROCESS display_required_permissions %]
        "></i>
    </div>
[% END %]

[% BLOCK confirm_privileges_for_required_kenmerk %]
    <div class="field-required-permissions">
        <i class="icon-font-awesome icon-unlock add-tooltip" title="
        <em>U heeft rechten om dit kenmerk te wijzigen, omdat u in &eacute;&eacute;n of meer van de volgende rollen zit:</em>
        [% PROCESS display_required_permissions %]
        "></i>
    </div>
[% END %]

[% bibliotheek_id = kenmerk.bibliotheek_kenmerken_id.id %]
[% kenmerkvalue = values.$bibliotheek_id %]


[% kenmerkrow_table_classes = [] %]
[% IF regels_result.active_fields.$bibliotheek_id; kenmerkrow_table_classes.push('regels_enabled_kenmerk'); END %]



[% document_value_zaak = zaak %]
[% IF zaak.pid && kenmerk.referential %]
    [% IF kenmerk.type == 'file' %]
        [% document_value_zaak = zaak.pid %]
    [% ELSE %]
        [% bibliotheek_kenmerken_id = kenmerk.bibliotheek_kenmerken_id.id %]
        [% referred_field_values = zaak.pid.scalar.field_values({
            bibliotheek_kenmerken_id => bibliotheek_kenmerken_id
        }) %]

        [% kenmerkvalue = referred_field_values.$bibliotheek_kenmerken_id %]
    [% END %]
[% END %]

[% IF kenmerk.type == 'file' %]

    [% case_document_id = kenmerk.id %]

    [% kenmerkvalue = document_value_zaak.file_field_documents(bibliotheek_id) %]

    [% # Temporary fix to make referential documents appear.
       # the current structure makes this is a bit difficult
       # the different naming conventions yield confusion,
       # next step might be to unify these.
       # Then the search for case values should happen in the model layer.
       #
       # These all refer to a zaaktype_kenmerken.id:
       # kenmerk.id => zaaktype_kenmerken.id
       # case_document_id: file_case_document.case_document_id
       #
       # Approach: Dig up the case_document_id of the parent case field value. We have parent case id, and bibliotheek_kenmerken_id
       # We need it's zaaktype_kenmerken_id
       #
       # jw@mintlab.nl Sep 7 2013
    %]
    [% IF kenmerk.referential %]

        [% parent_zaaktype_kenmerk = c.model('DB::ZaaktypeKenmerken').find({
            zaaktype_node_id => document_value_zaak.zaaktype_node_id.id,
            bibliotheek_kenmerken_id => kenmerk.bibliotheek_kenmerken_id.id
        }) %]

        [% IF parent_zaaktype_kenmerk %]
            [% case_document_id = parent_zaaktype_kenmerk.id %]
        [% END %]

        [% unaccepted_file_field_documents = 0 %]
    [% ELSE %]
        [% unaccepted_file_field_documents = document_value_zaak.unaccepted_file_field_documents(kenmerkvalue) %]
    [% END %]

[% END %]

[% BLOCK view_kenmerk %]
    [% previous_update_request = c.model('DB::ScheduledJobs').search_update_field_tasks({case_id => zaak.id, kenmerken => [bibliotheek_id]}).first  %]

    [% IF previous_update_request && check_queue_coworker_changes %]
        [% kenmerkvalue = previous_update_request.parameters.value %]
    [% END %]

    [% PROCESS widgets/general/veldoptie_view.tt
        veldoptie_multiple       = kenmerk.bibliotheek_kenmerken_id.type_multiple
        veldoptie_label_multiple = kenmerk.label_multiple
        veldoptie_type           = kenmerk.type
        veldoptie_value          = kenmerkvalue
    %]

    [% IF check_queue_coworker_changes && previous_update_request && kenmerk.type != 'file' %]
        <span class="change-pending-message">In beoordeling</span>
    [% END %]

[% END %]

[% kenmerk_help = c.user_exists? kenmerk.help : kenmerk.help_extern %]

[% attribute_name = kenmerk.type != 'objecttype' ? ('attribute.' _ kenmerk.bibliotheek_kenmerken_id.magic_string) : ('object.' _ kenmerk.object_id.get_object_attribute('prefix').value) %]
[% attribute_id = kenmerk.type != 'objecttype' ? ('kenmerk_id_' _ bibliotheek_id) : kenmerk.get_column('object_id') %]

<div class="kenmerk-veld [% kenmerkrow_table_classes.join(' ') %] clearfix[% IF kenmerk_help %] has-kenmerk-help[% END %] ezra_field_wrapper row ng-cloak kenmerk-veld-type-[% kenmerk.type %]"
    data-ng-show="caseWebformField.isVisible()"
    data-zs-case-webform-field
    data-zs-case-webform-field-type="[% kenmerk.type %]"
    data-field-group-id="[% kenmerk_last_group_id %]"

    [% SWITCH kenmerk.type %]
    
    [%   CASE 'text_block' %]
    data-zs-case-webform-field-name="text_block_[% kenmerk.id %]"
    data-zs-case-webform-field-id="text_block_[% kenmerk.id %]"

    [%   CASE DEFAULT %]
    data-zs-case-webform-field-name="[% attribute_name %]"
    data-zs-case-webform-field-id="[% attribute_id %]"

    [% END %]
>

    <!-- Display text-block -->
    [% IF kenmerk.type == 'text_block' %]
    <div class="column large-12">
        [% kenmerk.properties.text_content %]
    </div>
    [% END %]

    [% IF kenmerk.type != 'text_block' %]
    <div class="column large-4 has-icon">
        <div class="field-label[% IF kenmerk_help %] field-has-help[% END %]">
            [% IF kenmerk_help %]
            <div class="field-help">
                [% dropdown_content = BLOCK %]
                    [% kenmerk_help %]
                [% END %]
                [% PROCESS widgets/general/dropdown.tt
                    icon = 'icon-question-sign',
                    icon_type = 'icon-font-awesome',
                    content_overflow = 'dropdown-content-overflow',
                    dropdown_content = dropdown_content,
                    allow_click = 1
                %]
            </div>
            [% END %]
            [% kenmerkvalue_present = kenmerkvalue || (kenmerkvalue.size > 0 && kenmerkvalue.first) %]

            [% veldoptie_id = 'field_' _ bibliotheek_id _ '_' _ count %]

            <label class="titel" for="[% veldoptie_id %]">[% kenmerk.label || kenmerk.naam %][% IF !pip && kenmerk.value_mandatory && !zaak.is_afgehandeld %]<i class="ezra_required_field kenmerk-label-verplicht" title="Dit veld is verplicht"> * </i>[% END %][% UNLESS pip %]<i class="add-tooltip icon icon-font-awesome icon-book" title="[% kenmerk.naam %]"></i>[% END %]
            </label>
        </div>
    </div>
    [% END %]

    [% IF kenmerk.type != 'text_block' %]
    <div class="column large-8">
        [% # special privileges are needed to edit this field %]
        [% kenmerk_has_required_permissions = kenmerk.required_permissions_decoded.size %]
        
        <div class="field regels_fixed_value" data-ng-show="caseWebformField.isFixedValue()" data-ng-bind-html="caseWebformField.getFixedValueHtml()">
            
        </div>

        <div class="field
        [% IF kenmerk_has_required_permissions %][% UNLESS pip %] field-has-required-permissions[% END %][% END %]
         " data-ng-show="!caseWebformField.isFixedValue()">
            [% IF !kenmerk.is_group %]
                [% check_queue_coworker_changes = c.check_queue_coworker_changes %]
                [% show_attribute_update_request = check_queue_coworker_changes || (pip && kenmerk.pip_can_change && !zaak.is_afgehandeld) %]

                [% IF zaak.get_column('pid') && kenmerk.referential %]
                    <div class="referred_field_value">
                    [% IF kenmerk.type == 'file' %]
                        [% IF kenmerkvalue.size %]
                            [% FOREACH file IN kenmerkvalue %]

                            [%# ZS-3239 - Do not show referential attribute
                                file entries that are not yet accepted in
                                the main case
                            %]
                            [% NEXT UNLESS file.accepted %]

                            <div class="fileuploadthumbnail application_msword">
                                <a href="[% c.uri_for('/zaak/' _ zaak.get_column('pid') _ '/document/' _ file.file_id _ '/download') %]">[% file.filename %]</a>
                            </div>
                            [% END %]
                        [% ELSE %]
                            -
                        [% END %]
                    [% ELSE %]
                        [% PROCESS view_kenmerk %]
                    [% END %]
                    </div>
                    
                [% ELSIF c.can_change && !registration_phase_locked && !c.check_queue_coworker_changes %]

                    [% # this user has these privileges %]
                    [% user_has_required_privileges = c.check_field_permission(kenmerk) %]

                    [% IF user_has_required_privileges %]
                        [% PROCESS widgets/general/veldoptie.tt
                        veldoptie_multiple  = kenmerk.bibliotheek_kenmerken_id.type_multiple
                        veldoptie_name      = 'kenmerk_id_' _ bibliotheek_id
                        veldoptie_type      = kenmerk.type
                        veldoptie_opties    = kenmerk.options
                        veldoptie_value     = kenmerkvalue
                        veldoptie_id        = veldoptie_id
                        veldoptie_label_multiple = kenmerk.label_multiple
                        %]
                    [% ELSE %]
                        [% PROCESS view_kenmerk %]
                        [% PROCESS warn_required_kenmerk %]
                    [% END %]
                    [% IF kenmerk_has_required_permissions && user_has_required_privileges %]
                        [% PROCESS confirm_privileges_for_required_kenmerk %]
                    [% END %]
                [% ELSIF show_attribute_update_request && !registration_phase_locked %]
                    [% PROCESS zaak/update_attribute_form.tt %]
                [% ELSE %]
                    [% PROCESS view_kenmerk %]
                [% END %]
            [% END %]
        </div>
    </div>
    [% END %]

    [% IF !show_attribute_update_request && !pip %]
        [% PROCESS zaak/show_attribute_update_request.tt %]
    [% END %]
    
</div>
