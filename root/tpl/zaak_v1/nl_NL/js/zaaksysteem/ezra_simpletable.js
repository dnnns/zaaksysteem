$(document).ready( function() {

    $(document).on('click', '.ezra_simple_table-remove', function() {
        var obj = $(this);
        var id = obj.closest('tr').attr('id');


        var simpletable = obj.closest('.ezra_simple_table');
        var options = getOptions(simpletable.find('.ezra_simple_table-add').attr('rel'));
        var data_source = options.data_source;

        simpletable.find('.ezra_simple_table-table_container').load(
            data_source + '?action=remove&remove_id=' + id + ' .ezra_simple_table-table_container',
            function() {
                initializeEverything();
            }
        );

        return false;
    });


    $(document).on('click', '.ezra_simple_table-add', function() {
        var clicked_obj = $(this);

        var simpletable = clicked_obj.closest('.ezra_simple_table');
        var options = getOptions(simpletable.find('.ezra_simple_table-add').attr('rel'));
        var which = options['dialog'] || '#dialog';
        var data_source = options.data_source;

        var update_href = clicked_obj.attr('href');
        ezra_dialog({
            url     : clicked_obj.attr('href'),
            title   : clicked_obj.attr('title'),
            which   : which,
            layout  : options['ezra_dialog_layout']
        }, function() {
            var clickhandler = function(clicked_obj) {
                var item_id = clicked_obj.attr('id');

                var current_content = simpletable.closest('form').serialize();
                //console.log('current_content: ' + current_content);
                simpletable.find('.ezra_simple_table-table_container').load(
                    data_source + "?" + current_content + "&action=add&simpletable_new_item_id=" + 
                    item_id + ' .ezra_simple_table-table_container'
                );

                $(which).dialog('close');
            };
            
            // TODO make generic
            ezra_select_kenmerk(clickhandler);
            initializeEverything();

            $('#create_relatie form.zvalidate').unbind('submit').submit(function() {
                var obj = $(this);
                
                zvalidate(obj, {
                    callback: function() {
                        var current_content = obj.serializeArray();
    
                        simpletable.find('.ezra_simple_table-table_container').load(
                            data_source + ' .ezra_simple_table-table_container',
                            current_content
                        );                
    
                        $(which).dialog('close');
                    }
                });
                return false;
            });
        });
        return false;
    });
   
});
